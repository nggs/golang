package log

import (
	"fmt"
	"path/filepath"

	blog "github.com/astaxie/beego/logs"

	nutil "nggs/util"
)

const (
	LevelError         = blog.LevelError
	LevelWarning       = blog.LevelWarning
	LevelInformational = blog.LevelInformational
	LevelDebug         = blog.LevelDebug
)

type ILogger interface {
	Debug(format string, args ...interface{})
	Info(format string, args ...interface{})
	Warn(format string, args ...interface{})
	Error(format string, args ...interface{})
	SetLevel(int)
	Close()
}

func New(logDir string, logFileBaseName string) ILogger {
	nutil.MustMkdirIfNotExist(logDir)

	logger := blog.NewLogger()
	logger.Async()
	//logger.EnableFuncCallDepth(true)
	//logger.SetLogFuncCallDepth(2)

	logFilePath := filepath.Join(logDir, logFileBaseName)
	logFilePath = filepath.ToSlash(logFilePath)

	config := fmt.Sprintf(`{
		"filename": "%s.log",
		"level": %d,
		"maxlines": 50000,
		"maxsize":2097152,
		"separate": [ "error" ]
	}`, logFilePath, LevelDebug)

	if err := logger.SetLogger("multifile", config); err != nil {
		panic(err)
	}

	return logger
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var logger = NewConsoleLogger()

func SetLogger(l ILogger) {
	logger = l
}

func SetLevel(level int) {
	logger.SetLevel(level)
}

func Debug(format string, args ...interface{}) {
	logger.Debug("%s", fmt.Sprintf(format, args...))
}

func Info(format string, args ...interface{}) {
	logger.Info("%s", fmt.Sprintf(format, args...))
}

func Warn(format string, args ...interface{}) {
	logger.Warn("%s", fmt.Sprintf(format, args...))
}

func Error(format string, args ...interface{}) {
	logger.Error("%s", fmt.Sprintf(format, args...))
}
