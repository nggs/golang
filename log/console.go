package log

import (
	"fmt"
	"log"
	"os"
)

type ConsoleLogger struct {
	level  int
	logger *log.Logger
}

func NewConsoleLogger() (logger ILogger) {
	logger = &ConsoleLogger{
		level:  LevelDebug,
		logger: log.New(os.Stderr, "", log.LstdFlags),
	}
	return
}

func (cl ConsoleLogger) Debug(format string, args ...interface{}) {
	if LevelDebug > cl.level {
		return
	}
	cl.logger.Printf("[D] %s\n", fmt.Sprintf(format, args...))
}

func (cl ConsoleLogger) Info(format string, args ...interface{}) {
	if LevelInformational > cl.level {
		return
	}
	cl.logger.Printf("[I] %s\n", fmt.Sprintf(format, args...))
}

func (cl ConsoleLogger) Warn(format string, args ...interface{}) {
	if LevelWarning > cl.level {
		return
	}
	cl.logger.Printf("[W] %s\n", fmt.Sprintf(format, args...))
}

func (cl ConsoleLogger) Error(format string, args ...interface{}) {
	if LevelError > cl.level {
		return
	}
	cl.logger.Printf("[E] %s\n", fmt.Sprintf(format, args...))
}

func (cl *ConsoleLogger) SetLevel(level int) {
	cl.level = level
}

func (ConsoleLogger) Close() {

}
