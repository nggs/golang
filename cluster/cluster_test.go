package cluster

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"testing"
	"time"

	"github.com/asynkron/protoactor-go/actor"
	"go.etcd.io/etcd/client/v3"

	nexport "nggs/export"
	nservice "nggs/service"
)

const (
	testEtcdConfigDir      = "../tests/config"
	testEtcdConfigFilePath = testEtcdConfigDir + "/etcd.json"
)

func TestGenPareKey(t *testing.T) {
	args := []interface{}{
		"root", "test", "config", 1,
	}

	except := "root:test:config:1"
	key := GenKey(args...)
	if key != except {
		t.Errorf("GenKey fail, except=[%s], key=[%s]", except, key)
		return
	}

	etcdRoot, section, serviceName, strServiceID, err := ParseKeyS(key)
	if err != nil {
		t.Errorf("ParseKey fail, %v", err)
		return
	}
	if etcdRoot != args[0].(string) {
		t.Errorf("ParseKey fail, etcdRoot=[%v], excepte=[%v]", etcdRoot, args[0])
		return
	}
	if section != args[1].(string) {
		t.Errorf("ParseKey fail, section=[%v], excepte=[%v]", section, args[1])
		return
	}
	if serviceName != args[2].(string) {
		t.Errorf("ParseKey fail, serviceName=[%v], excepte=[%v]", serviceName, args[2])
		return
	}
	serviceID, _ := strconv.Atoi(strServiceID)
	if serviceID != args[3].(int) {
		t.Errorf("ParseKey fail, serviceID=[%v], excepte=[%v]", strServiceID, args[3])
		return
	}
}

type global struct {
	Gateway gatewayGlobal
}

func (c global) TidyAndCheck() error {
	if err := c.Gateway.TidyAndCheck(); err != nil {
		return fmt.Errorf("invalid Gateway, %w", err)
	}
	return nil
}

func (c global) Clone() nexport.IClusterGlobalConfig {
	n := &global{}
	*n = c
	return n
}

type gatewayGlobal struct {
	MaxConnNum     int
	RecvTimeoutSec int
	SendTimeoutSec int
}

func (c *gatewayGlobal) TidyAndCheck() error {
	return nil
}

func (c gatewayGlobal) Clone() nexport.IServiceGlobalConfig {
	n := &gatewayGlobal{}
	*n = c
	return n
}

type gateway struct {
	nservice.Config
	IP   string
	Port int
}

func (c *gateway) TidyAndCheck() error {
	if err := c.Config.TidyAndCheck(); err != nil {
		return err
	}
	if c.IP == "" {
		return fmt.Errorf("gateway[%d].IP must not empty", c.GetID())
	}
	if c.Port < 0 {
		return fmt.Errorf("gateway[%d].Port must >= 0", c.GetID())
	}
	return nil
}

func (c gateway) Clone() nexport.IServiceConfig {
	n := &gateway{}
	*n = c
	return n
}

type base struct {
	nservice.Config
	IP   string
	Port int
}

func (c *base) TidyAndCheck() error {
	if err := c.Config.TidyAndCheck(); err != nil {
		return err
	}
	if c.IP == "" {
		return fmt.Errorf("base[%d].IP must not empty", c.GetID())
	}
	if c.Port <= 0 {
		return fmt.Errorf("base[%d].Port must > 0", c.GetID())
	}
	return nil
}

func (c base) Clone() nexport.IServiceConfig {
	n := &base{}
	*n = c
	return n
}

func Test(t *testing.T) {
	err := C.Init(testEtcdConfigFilePath,
		(*global)(nil),
		ServiceGroupConfigMap{
			"gateway": {
				GlobalConfig: (*gatewayGlobal)(nil),
				Config:       (*gateway)(nil),
			},
			"base": {
				Config: (*base)(nil),
			},
		})
	if err != nil {
		t.Error(err)
		return
	}
	defer C.Close()

	err = C.AddWatchCallback(func(err error, section string, serviceName string, serviceID int, ev *clientv3.Event) {
		log.Printf("[%v][%v][%v][%v][%v]", err, section, serviceName, serviceID, ev)
		if err != nil {
			t.Error(err)
		}
	})

	time.Sleep(1 * time.Second)

	globalConfig, err := C.GetGlobalConfig()
	if err != nil {
		t.Error(err)
		return
	}

	log.Printf("%#v", globalConfig)

	gatewayGlobalConfig, err := C.GetServiceGlobalConfig("gateway")
	if err != nil {
		t.Error(err)
		return
	}

	log.Printf("%#v", gatewayGlobalConfig)

	gatewayConfig, err := C.GetServiceInfo("gateway", 1)
	if err != nil {
		t.Error(err)
		return
	}

	log.Printf("%#v", gatewayConfig.Config)

	gatewayInfos, err := C.GetServiceGroupAllInfo("gateway")
	if err != nil {
		t.Error(err)
		return
	}
	for _, info := range gatewayInfos {
		log.Printf("%#v", info.Config)
	}

	baseInfos, err := C.GetServiceGroupAllInfo("base")
	if err != nil {
		t.Error(err)
		return
	}
	for _, info := range baseInfos {
		log.Printf("%#v", info.Config)
	}

	err = C.PutServiceInstance("base", 1, &nservice.Instance{
		PID:          actor.NewPID("127.0.0.1:8080", "base-1"),
		PprofAddress: "127.0.0.1:8081",
		Extra: &nservice.InstanceExtra{
			"extra1": 1,
			"extra2": "2",
		},
	})
	if err != nil {
		t.Error(err)
		return
	}

	time.Sleep(1 * time.Second)

	i, err := C.GetServiceInstance("base", 1)
	if err != nil {
		t.Error(err)
		return
	}
	log.Printf("%#v", i)
}

func TestPutConfig(t *testing.T) {
	c := NewCluster(nil, nil)
	err := c.Init(testEtcdConfigFilePath, nil,
		ServiceGroupConfigMap{
			"gateway": {
				GlobalConfig: (*gatewayGlobal)(nil),
				Config:       (*gateway)(nil),
			},
		})
	if err != nil {
		t.Error(err)
		return
	}
	defer c.Close()

	key := c.GenConfigKey("gateway", 3)
	_, err = c.etcdClient.Delete(key)

	time.Sleep(100 * time.Millisecond)

	gatewayInfos, err := c.GetServiceGroupAllInfo("gateway")
	if err != nil {
		t.Error(err)
		return
	}
	var exist bool
	for _, info := range gatewayInfos {
		if info.Config.GetID() == 3 {
			exist = true
			break
		}
	}
	if exist {
		t.Errorf("gateway[3] config must not exist")
		return
	}

	data, err := ioutil.ReadFile(testEtcdConfigDir + "/gateway/3.json")
	if err != nil {
		t.Errorf("读取gateway[3]配置文件失败, %s\n", err)
		return
	}

	if err := c.etcdClient.Put(key, string(data)); err != nil {
		t.Errorf("将gateway[3]配置文件写入etcd失败, %s\n", err)
		return
	}

	time.Sleep(100 * time.Millisecond)

	gatewayInfos, err = c.GetServiceGroupAllInfo("gateway")
	if err != nil {
		t.Error(err)
		return
	}
	exist = false
	for _, info := range gatewayInfos {
		if info.Config.GetID() == 3 {
			exist = true
			break
		}
	}
	if !exist {
		t.Errorf("gateway[3] config must exist")
		return
	}
}

func TestGenParseConfigKey(t *testing.T) {
	c := NewCluster(nil, nil)
	err := c.Init(testEtcdConfigFilePath, nil, nil)
	if err != nil {
		t.Error(err)
		return
	}
	defer c.Close()

	k := c.GenConfigKey("base", 1)
	fmt.Println(k)
	n, i, err := c.ParseConfigKeyS(k)
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("n=%v\n", n)
	t.Logf("i=%v\n", i)
}

func TestGenParseInstanceKey(t *testing.T) {
	c := NewCluster(nil, nil)
	err := c.Init(testEtcdConfigFilePath, nil, nil)
	if err != nil {
		t.Error(err)
		return
	}
	defer c.Close()

	k := c.GenInstanceKey("base", 1)
	n, i, err := c.ParseInstanceKey(k)
	if err != nil {
		t.Error(err)
		return
	}
	if n != "base" {
		t.Errorf("parse Group fail")
		return
	}
	if i != 1 {
		t.Errorf("parse id fail")
		return
	}
}

func TestPutGetLockInstance(t *testing.T) {
	c := NewCluster(nil, nil)
	err := c.Init(testEtcdConfigFilePath,
		nil,
		ServiceGroupConfigMap{
			"base": {
				Config: (*base)(nil),
			},
		})
	if err != nil {
		t.Error(err)
		return
	}
	defer c.Close()

	err = c.AddWatchCallback(func(err error, section string, serviceName string, serviceID int, ev *clientv3.Event) {
		if err != nil {
			t.Error(err)
			return
		}
		t.Logf("[%v][%v][%v][%v][%v]", err, section, serviceName, serviceID, ev)
	})

	err = c.PutServiceInstance("base", 1, &nservice.Instance{
		PID:          actor.NewPID("127.0.0.1:8080", "base-1"),
		PprofAddress: "127.0.0.1:8081",
		Extra: &nservice.InstanceExtra{
			"extra1": 1,
			"extra2": "2",
		},
	})
	if err != nil {
		t.Error(err)
		return
	}

	i, err := c.GetServiceInstance("base", 1)
	if err != nil {
		t.Error(err)
		return
	}

	if i.PID.Address != "127.0.0.1:8080" {
		t.Errorf("pid addr not match, [%v]", i.PID.Address)
		return
	}
	if i.PID.Id != "base-1" {
		t.Errorf("pid id not match, [%v]", i.PID.Id)
		return
	}
	if i.PprofAddress != "127.0.0.1:8081" {
		t.Errorf("pprof addr not match, [%v]", i.PprofAddress)
		return
	}
	if (*i.Extra)["extra1"].(float64) != 1 {
		t.Errorf("extra 1 not match, [%v]", (*i.Extra)["extra1"])
		return
	}
	if (*i.Extra)["extra2"].(string) != "2" {
		t.Errorf("extra 2 not match, [%v]", (*i.Extra)["extra2"])
		return
	}

	err = c.TryLockServicePosition("base", 1, nservice.NewInstance())
	if err == nil {
		t.Errorf("must fail")
	} else {
		t.Log(err)
	}
}

func TestGenParsePayloadKey(t *testing.T) {
	c := NewCluster(nil, nil)
	err := c.Init(testEtcdConfigFilePath, nil, nil)
	if err != nil {
		t.Error(err)
		return
	}
	defer c.Close()

	k := c.GenPayloadKey("base", 1)
	n, i, err := c.ParsePayloadKey(k)
	if err != nil {
		t.Error(err)
		return
	}
	if n != "base" {
		t.Errorf("parse Group fail")
		return
	}
	if i != 1 {
		t.Errorf("parse id fail")
		return
	}
}

func TestPutGetPayload(t *testing.T) {
	c := NewCluster(nil, nil)
	err := c.Init(testEtcdConfigFilePath,
		nil,
		ServiceGroupConfigMap{
			"base": {
				Config: (*base)(nil),
			},
		})
	if err != nil {
		t.Error(err)
		return
	}
	defer c.Close()

	err = c.AddWatchCallback(func(err error, section string, serviceName string, serviceID int, ev *clientv3.Event) {
		t.Logf("[%v][%v][%v][%v][%v]", err, section, serviceName, serviceID, ev)
		if err != nil {
			t.Error(err)
			return
		}
	})

	err = c.TryLockServicePosition("base", 2, &nservice.Instance{
		PID:          actor.NewPID("127.0.0.1:8080", "base-2"),
		PprofAddress: "127.0.0.1:8081",
		Extra: &nservice.InstanceExtra{
			"extra1": 1,
			"extra2": "2",
		},
	})
	if err != nil {
		t.Error(err)
		return
	}

	err = c.PutServicePayload("base", 2, 1)
	if err != nil {
		t.Error(err)
		return
	}

	var p int
	p, err = c.GetServicePayload("base", 2)
	if err != nil {
		t.Error(err)
		return
	}

	if p != 1 {
		t.Errorf("payload not match, [%v]", p)
		return
	}
}

func TestSetWatchCallbackWithNoRegister(t *testing.T) {
	c := NewCluster(nil, nil)
	err := c.Init(testEtcdConfigFilePath, nil, nil)
	if err != nil {
		t.Error(err)
		return
	}
	defer c.Close()

	err = c.AddWatchCallback(func(err error, section string, serviceName string, serviceID int, ev *clientv3.Event) {
		t.Logf("[%v][%v][%v][%v][%v]", err, section, serviceName, serviceID, ev)
		if err == nil {
			t.Error("must not found the Info unit")
			return
		}
	})
	if err != nil {
		t.Errorf("add watch callback fail, %s", err)
		return
	}

	time.Sleep(500 * time.Millisecond)

	err = c.directPutInstance("base", 1, *nservice.NewInstance())
	if err != nil {
		t.Error(err)
	}

	time.Sleep(500 * time.Millisecond)
}

func TestWatchCallback(t *testing.T) {
	c := NewCluster(nil, nil)
	err := c.Init(testEtcdConfigFilePath,
		nil,
		ServiceGroupConfigMap{
			"base": {
				Config: (*base)(nil),
			},
		})
	if err != nil {
		t.Error(err)
		return
	}
	defer c.Close()

	err = c.AddWatchCallback(func(err error, section string, serviceName string, serviceID int, ev *clientv3.Event) {
		t.Logf("[%v][%v][%v][%v][%v]", err, section, serviceName, serviceID, ev)
	})

	time.Sleep(100 * time.Millisecond)

	err = c.directPutInstance("base", 10, *&nservice.Instance{
		PID:          actor.NewPID("127.0.0.1:8080", "base-10"),
		PprofAddress: "127.0.0.1:8081",
		Extra: &nservice.InstanceExtra{
			"extra1": 1,
			"extra2": "2",
		},
	})
	if err != nil {
		t.Error(err)
	}

	time.Sleep(100 * time.Millisecond)
}

func TestCluster_RoundRobinPickServiceInstance(t *testing.T) {
	c := NewCluster(nil, nil)
	err := c.Init(testEtcdConfigFilePath,
		nil,
		ServiceGroupConfigMap{
			"gateway": {
				GlobalConfig: (*gatewayGlobal)(nil),
				Config:       (*gateway)(nil),
			},
			"base": {
				Config: (*base)(nil),
			},
		})
	if err != nil {
		t.Error(err)
		return
	}
	defer c.Close()

	err = c.AddWatchCallback(func(err error, section string, serviceName string, serviceID int, ev *clientv3.Event) {
		t.Logf("[%v][%v][%v][%v][%v]", err, section, serviceName, serviceID, ev)
		if err != nil {
			t.Error(err)
		}
	})

	err = c.PutServiceInstance("gateway", 1, &nservice.Instance{
		PID:          actor.NewPID("127.0.0.1:8080", "gateway-1"),
		PprofAddress: "127.0.0.1:8081",
		Extra: &nservice.InstanceExtra{
			"extra1": 1,
			"extra2": "2",
		},
	})
	if err != nil {
		t.Error(err)
		return
	}

	for i := 0; i < 10; i++ {
		info, err := c.RoundRobinPickService("gateway")
		if err != nil {
			t.Error(err)
			return
		}
		t.Logf("%s\n", info.Instance.PID.String())
	}

	key := c.GenInstanceKey("gateway", 1)
	_, err = c.etcdClient.Delete(key)

	time.Sleep(100 * time.Millisecond)

	for i := 0; i < 10; i++ {
		info, err := c.RoundRobinPickService("gateway")
		if err != nil {
			t.Error(err)
			return
		}
		if info != nil {
			t.Errorf("info must be nil")
			return
		} else {
			t.Logf("pick nil gateway instance")
		}
	}

	err = c.PutServiceInstance("gateway", 1, &nservice.Instance{
		PID:          actor.NewPID("127.0.0.1:8080", "gateway-1"),
		PprofAddress: "127.0.0.1:8081",
		Extra: &nservice.InstanceExtra{
			"extra1": 1,
			"extra2": "2",
		},
	})
	if err != nil {
		t.Error(err)
		return
	}
	err = c.PutServiceInstance("gateway", 3, &nservice.Instance{
		PID:          actor.NewPID("127.0.0.1:8080", "gateway-3"),
		PprofAddress: "127.0.0.1:8082",
		Extra: &nservice.InstanceExtra{
			"extra1": 5,
			"extra2": "6",
		},
	})
	if err != nil {
		t.Error(err)
		return
	}

	//infos, err := c.GetServiceGroupAllInfo("gateway")
	//if err != nil {
	//	t.Error(err)
	//	return
	//}
	//for _, info := range infos {
	//	if info.Instance != nil {
	//		t.Logf("%#v\n", info.Instance)
	//	} else {
	//		t.Logf("%#v\n", info)
	//	}
	//}

	for i := 0; i < 10; i++ {
		info, err := c.RoundRobinPickService("gateway")
		if err != nil {
			t.Error(err)
			return
		}
		t.Logf("%s\n", info.Instance.PID.String())
	}

	err = c.PutServiceInstance("gateway", 2, &nservice.Instance{
		PID:          actor.NewPID("127.0.0.1:8080", "gateway-2"),
		PprofAddress: "127.0.0.1:8081",
		Extra: &nservice.InstanceExtra{
			"extra1": 3,
			"extra2": "4",
		},
	})
	if err != nil {
		t.Error(err)
		return
	}

	for i := 0; i < 10; i++ {
		info, err := c.RoundRobinPickService("gateway")
		if err != nil {
			t.Error(err)
			return
		}
		t.Logf("%s\n", info.Instance.PID.String())
	}

	key = c.GenInstanceKey("gateway", 3)
	_, err = c.etcdClient.Delete(key)

	time.Sleep(100 * time.Millisecond)

	for i := 0; i < 10; i++ {
		info, err := c.RoundRobinPickService("gateway")
		if err != nil {
			t.Error(err)
			return
		}
		t.Logf("%s\n", info.Instance.PID.String())
	}
}
