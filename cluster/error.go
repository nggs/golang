package cluster

import "errors"

var ErrServiceGroupNotExist = errors.New("service group not exist")

var ErrServiceInfoNotExist = errors.New("service info not exist")

var ErrServiceInstanceAlreadyExist = errors.New("service instance already exist")

var ErrCallbackIsNil = errors.New("callback is nil")
