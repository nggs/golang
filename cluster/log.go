package cluster

import "nggs/log"

var gLogger = log.NewConsoleLogger()

func Logger() log.ILogger {
	return gLogger
}

func SetLogger(l log.ILogger) {
	gLogger = l
}
