package export

import (
	"sync"

	"github.com/asynkron/protoactor-go/actor"
)

type ServiceID = int

type IServiceGlobalConfig interface {
	IGlobalConfig
	Clone() IServiceGlobalConfig
}

type IServiceConfig interface {
	IConfig
	Clone() IServiceConfig
}

type IService interface {
	Init(appConfig IAppConfig, serviceID ServiceID, startedWg *sync.WaitGroup, stoppedWg *sync.WaitGroup, args ...interface{}) error
	Run(ctx actor.Context, pprofAddress string, args ...interface{}) error
	Stop() error
}

type IServiceInstanceExtra interface {
	Clone() IServiceInstanceExtra
}
