package export

import "github.com/asynkron/protoactor-go/actor"

type LogicID = int

type ILogic interface {
	ID() LogicID
	Init() (err error)
	Run()
	Finish()
	OnActorTerminated(who *actor.PID, ctx actor.Context)
	OnPulse(ctx actor.Context)
}
