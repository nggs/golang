package export

type Payload = int

type IClusterGlobalConfig interface {
	IGlobalConfig
	Clone() IClusterGlobalConfig
}
