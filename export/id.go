package export

type ActorTimerID = uint64

type ActorTimerTag = uint64

type ActorHttpTaskID = uint64

type ActorHttpTaskTag = uint64
