package export

type IGlobalConfig interface {
	TidyAndCheck() error
}

type IConfig interface {
	TidyAndCheck() error
	GetID() int
	SetID(id int)
}
