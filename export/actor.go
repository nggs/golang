package export

import (
	nevent "nggs/event"
	npb "nggs/network/protocol/protobuf/v3"
	nrpc "nggs/rpc"

	"github.com/asynkron/protoactor-go/actor"
	"github.com/gin-gonic/gin"
)

type IActorImplement interface {
	OnStarted(ctx actor.Context)
	OnStopping(ctx actor.Context)
	OnStopped(ctx actor.Context)
	OnReceiveMessage(ctx actor.Context)
	OnActorTerminated(who *actor.PID, ctx actor.Context)
	OnRestarting(actor.Context)
	OnRestarted(ctx actor.Context)
	BeforeTriggerTimer(id ActorTimerID, tag ActorTimerTag, ctx actor.Context) (ok bool, err error)
	AfterTriggerTimer(err error, id ActorTimerID, tag ActorTimerTag, ctx actor.Context)
	OnPulse(ctx actor.Context)
	BeforeDispatchEvent(inEvent nevent.IEvent, ctx actor.Context) (args []interface{}, ok bool, err error)
	AfterDispatchEvent(err error, iEvent nevent.IEvent, ctx actor.Context, args ...interface{})
	BeforeDispatchRPC(sender *actor.PID, iRequestMessage nrpc.IMessage, ctx actor.Context) (args []interface{}, ok bool, err error)
	AfterDispatchRPC(err error, sender *actor.PID, iRequestMessage nrpc.IMessage, iResponseMessage nrpc.IMessage, ctx actor.Context, args ...interface{})
	BeforeCompleteHttpTask(id ActorHttpTaskID, tag ActorHttpTaskTag, ctx actor.Context) (ok bool, err error)
	AfterCompleteHttpTask(err error, id ActorHttpTaskID, tag ActorHttpTaskTag, ctx actor.Context)
	BeforeDispatchHttpRequest(ctx *gin.Context) (ok bool, err error)
	AfterDispatchHttpRequest(err error, ctx *gin.Context)
	BeforeDispatchMessage(iProtocol npb.IProtocol, iRecv npb.IMessage, ctx actor.Context) (iSend npb.IMessage, args []interface{}, ok bool, err error)
	AfterDispatchMessage(err error, iRecv npb.IMessage, iSend npb.IMessage, ctx actor.Context, args ...interface{})
}
