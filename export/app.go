package export

import "time"

type AppID = int

type IServiceConfigInApp interface {
	GetName() string
	GetID() ServiceID
	Clone() IServiceConfigInApp
}

type IAppConfig interface {
	IConfig

	GetPprofIP() string
	GetPprofPort() int
	GetEndpointIP() string
	GetEndpointPort() int
	EachServiceConfig(func(index int, iServiceConfig IServiceConfigInApp) (continued bool))
	GetServiceConfigByIndex(index int) (IServiceConfigInApp, bool)
	AddServiceConfig(iServiceConfig IServiceConfigInApp)
	GetLogDir() string
	GetStartServiceIntervalMSec() time.Duration
	GetStopServiceIntervalMSec() time.Duration
	GetExitWaitSec() time.Duration
	Clone() IServiceConfig
}
