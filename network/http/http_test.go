package http

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net"
	"net/http"
	"testing"
	"time"

	"nggs/random"
)

//type example struct {
//	*nactor.Actor
//
//	startedWg sync.WaitGroup
//	stoppedWg sync.WaitGroup
//
//	httpServer *Server
//}
//
//func newExample(listenAddr string) (e *example, err error) {
//	e = &example{}
//
//	e.Actor = nactor.New(
//		nactor.WithOnReceiveMessage(e.onReceiveMessage),
//		nactor.WithOnStarted(e.onStarted),
//		nactor.WithOnStopping(e.onStopping),
//		nactor.WithOnStopped(e.onStopped),
//		nactor.WithOnRestarting(e.onRestarting),
//		nactor.WithOnRestarted(e.onRestarted),
//		nactor.WithStartedWaitGroup(&e.startedWg),
//		nactor.WithStoppedWaitGroup(&e.stoppedWg),
//		nactor.WithMailBoxSize(10),
//	)
//
//	e.httpServer = NewServer()
//
//	err = e.httpServer.Listen(listenAddr)
//	if err != nil {
//		err = fmt.Errorf("listen fail, %w", err)
//		return
//	}
//
//	go e.httpServer.Serve()
//
//	err = e.Start(nil, "example")
//	if err != nil {
//		err = fmt.Errorf("start fail, %w", err)
//		return
//	}
//
//	e.WaitForStarted()
//
//	return
//}
//
//func (e *example) onReceiveMessage(ctx actor.Context) {
//	iMsg := ctx.Message()
//	sender := ctx.Sender()
//	switch msg := iMsg.(type) {
//	case nactor.IHttpRequest:
//		e.httpServer.Dispatch(e.Actor, sender, ctx, msg)
//
//	default:
//		log.Printf("recv %#v", msg)
//	}
//}
//
//func (e *example) onStarted(ctx actor.Context) {
//	log.Println("onStarted")
//}
//
//func (e *example) onStopping(ctx actor.Context) {
//	log.Println("onStopping")
//}
//
//func (e *example) onStopped(ctx actor.Context) {
//	log.Println("onStopped")
//}
//
//func (e *example) onRestarting(ctx actor.Context) {
//	log.Println("onRestarting")
//}
//
//func (e *example) onRestarted(ctx actor.Context) {
//	log.Println("onRestarted")
//}

//func Test_HttpServer(t *testing.T) {
//	nactor.Init(alog.DebugLevel, nil)
//	defer nactor.Close()
//
//	example, err := newExample("localhost:8080")
//	if err != nil {
//		t.Errorf("newExample fail, %s", err)
//		return
//	}
//
//	echoHandler := func(writer http.ResponseWriter, r *http.Request) {
//		if recvData, err := ioutil.ReadAll(r.Body); err != nil {
//			t.Errorf("get request body data fail, %s", err)
//		} else {
//			if len(recvData) > 0 {
//				_, err = writer.Write(recvData)
//				if err != nil {
//					t.Errorf("write data fail, %s", err)
//				}
//			}
//		}
//	}
//
//	err = example.httpServer.Register(example.Actor, "/echo1", echoHandler, nil)
//	if err != nil {
//		t.Errorf("Register /echo1 fail, %s", err)
//		return
//	}
//
//	err = example.httpServer.Register(example.Actor, "/echo2", echoHandler, NewHandlerOption(WithHandlerQueued(false)))
//	if err != nil {
//		t.Errorf("Register /echo2 fail, %s", err)
//		return
//	}
//
//	_, err = example.NewHttpTask(3*time.Second, func() (request *http.Request, err error) {
//		request, err = http.NewRequest("GET", "http://localhost:8080/echo1", bytes.NewReader([]byte("echo1")))
//		if err != nil {
//			return
//		}
//		return
//	}, func(err error, task *nactor.HttpTask) {
//		if err != nil {
//			t.Errorf("NewHttpTask fail, %s", err)
//			return
//		}
//
//		body, err := task.GetResponseBodyData()
//		if err != nil {
//			t.Errorf("GetResponseBodyData fail, %s", err)
//			return
//		}
//
//		if string(body) != "echo1" {
//			t.Errorf("recv data not match")
//			return
//		}
//	})
//	if err != nil {
//		t.Errorf("NewHttpTask fail, %s", err)
//		return
//	}
//
//	_, err = example.NewHttpTask(3*time.Second, func() (request *http.Request, err error) {
//		request, err = http.NewRequest("GET", "http://localhost:8080/echo2", bytes.NewReader([]byte("echo2")))
//		if err != nil {
//			return
//		}
//		return
//	}, func(err error, task *nactor.HttpTask) {
//		if err != nil {
//			t.Errorf("NewHttpTask fail, %s", err)
//			return
//		}
//
//		body, err := task.GetResponseBodyData()
//		if err != nil {
//			t.Errorf("GetResponseBodyData fail, %s", err)
//			return
//		}
//
//		if string(body) != "echo2" {
//			t.Errorf("recv data not match")
//			return
//		}
//	})
//	if err != nil {
//		t.Errorf("NewHttpTask fail, %s", err)
//		return
//	}
//
//	time.Sleep(3 * time.Second)
//}

func TestGetJson(t *testing.T) {
	type param struct {
		I32  int32
		I64  int64
		F32  float32
		F64  float64
		Str  string
		Time time.Time
	}

	const sleepTime = 1 * time.Second
	const waitTime = 3 * time.Second

	addr := "localhost:8080"
	ln, err := net.Listen("tcp", addr)
	if err != nil {
		t.Errorf("listen [%s] fail, %s", addr, err)
		return
	}

	pattern := "/test"

	mux := http.NewServeMux()
	mux.HandleFunc(pattern, func(w http.ResponseWriter, r *http.Request) {
		t.Logf("token=[%s]\n", r.Header.Get("Authorization"))

		recv := param{
			I32:  random.Int32(0, 10000),
			I64:  random.Int64(0, 10000),
			F32:  rand.Float32(),
			F64:  rand.Float64(),
			Str:  random.String(32),
			Time: time.Now().Add(random.TimeDuration(5*time.Second, 10000*time.Second)),
		}
		//t.Logf("before decode, recv=%v\n", recv)
		json.NewDecoder(r.Body).Decode(&recv)
		t.Logf("after decode, recv=%v\n", recv)

		time.Sleep(sleepTime)

		send := param{
			I32:  random.Int32(0, 10000),
			I64:  random.Int64(0, 10000),
			F32:  rand.Float32(),
			F64:  rand.Float64(),
			Str:  random.String(32),
			Time: time.Now().Add(random.TimeDuration(1*time.Second, 10000*time.Second)),
		}
		t.Logf("send=%v\n", send)
		if err := json.NewEncoder(w).Encode(send); err != nil {
			t.Errorf("encode send fail, %s", err)
			return
		}
	})

	svr := &http.Server{
		Addr:    addr,
		Handler: mux,
	}

	go svr.Serve(ln)

	defer svr.Close()

	send := param{
		I32:  random.Int32(0, 10000),
		I64:  random.Int64(0, 10000),
		F32:  rand.Float32(),
		F64:  rand.Float64(),
		Str:  random.String(32),
		Time: time.Now().Add(random.TimeDuration(1*time.Second, 10000*time.Second)),
	}

	t.Logf("send=%v\n", send)

	recv := param{}

	if err := GetJson(fmt.Sprintf("http://%s%s", addr, pattern), waitTime, "123", send, &recv); err != nil {
		t.Error(err)
		return
	}

	t.Logf("recv=%v\n", recv)

	time.Sleep(sleepTime)
}
