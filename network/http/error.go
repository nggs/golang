package http

import "fmt"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type ErrParseFormFail struct {
	err error
}

func NewParseFormFail(err error) *ErrParseFormFail {
	return &ErrParseFormFail{err: err}
}

func (e ErrParseFormFail) Error() string {
	return fmt.Sprintf("parse form fail, %s", e.err)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type ErrInvalidSign struct {
	invalidSign string
	validSign   string
}

func NewErrInvalidSign(invalidSign string, validSign string) *ErrInvalidSign {
	return &ErrInvalidSign{invalidSign: invalidSign, validSign: validSign}
}

func (e ErrInvalidSign) Error() string {
	return fmt.Sprintf("invalid sign=[%s], validSign=[%s]", e.invalidSign, e.validSign)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type ErrInvalidParam struct {
	name  string
	value string
}

func NewErrInvalidParam(name string, value string) *ErrInvalidParam {
	return &ErrInvalidParam{name: name, value: value}
}

func (e ErrInvalidParam) Error() string {
	return fmt.Sprintf("invalid param, name=[%s], value=[%s]", e.name, e.value)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type ErrConvert2NumFail struct {
	str string
	err error
}

func NewErrConvert2NumFail(str string, err error) *ErrConvert2NumFail {
	return &ErrConvert2NumFail{str: str, err: err}
}

func (e ErrConvert2NumFail) Error() string {
	return fmt.Sprintf("converto %s to num fail, %s", e.str, e.err)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type ErrHandlerAlreadyRegister struct {
	pattern string
}

func NewErrHandlerAlreadyRegister(pattern string) *ErrHandlerAlreadyRegister {
	return &ErrHandlerAlreadyRegister{pattern: pattern}
}

func (e ErrHandlerAlreadyRegister) Error() string {
	return fmt.Sprintf("%s already register", e.pattern)
}
