package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/thinkeridea/go-extend/exnet"
)

func GetRequestIP(r *http.Request) string {
	ip := exnet.ClientPublicIP(r)
	if ip == "" {
		ip = exnet.ClientIP(r)
	}
	return ip
}

func DoTask(
	method string,
	url string,
	body interface{},
	timeout time.Duration,
	onPrepare func(*http.Request) (*http.Request, error),
	onCompleted func(request *http.Request, response *http.Response, client *http.Client) (err error),
) (err error) {

	if method == "" {
		err = fmt.Errorf("method must not empty")
		return
	}

	if url == "" {
		err = fmt.Errorf("url must not empty")
		return
	}

	if timeout <= 0 {
		err = fmt.Errorf("timeout must > 0")
		return
	}

	var request *http.Request
	var e error
	if body == nil {
		request, e = http.NewRequest(method, url, nil)
	} else {
		request, e = http.NewRequest(method, url, body.(io.Reader))
	}
	if e != nil {
		err = fmt.Errorf("http.NewRequest fail, %w", e)
		return
	}

	if onPrepare != nil {
		request, e = onPrepare(request)
		if e != nil {
			err = fmt.Errorf("onPrepare fail, %w", e)
			return
		}
		if request == nil {
			err = fmt.Errorf("request is nil after onPrepare")
			return
		}
	}

	client := &http.Client{Timeout: timeout}

	response, e := client.Do(request)
	if e != nil {
		err = fmt.Errorf("do request fail, %s", e)
		return
	}

	if onCompleted != nil {
		err = onCompleted(request, response, client)
	}

	_ = response.Body.Close()

	return
}

func DoJSONTask(
	method string,
	url string,
	timeout time.Duration,
	send interface{},
	recv interface{},
	onPrepare func(*http.Request) (*http.Request, error),
) (err error) {
	var sendBody interface{}

	if send != nil {
		sendJson, e := json.Marshal(send)
		if e != nil {
			err = fmt.Errorf("marshal send[%T] fail, %w", send, e)
			return
		}
		sendBody = bytes.NewReader(sendJson)
	}

	err = DoTask(method, url, sendBody, timeout, func(request *http.Request) (*http.Request, error) {
		if send != nil {
			request.Header.Set("Content-Type", "application/json")
		}
		if onPrepare != nil {
			return onPrepare(request)
		}
		return request, nil
	}, func(request *http.Request, response *http.Response, client *http.Client) (err error) {
		if recv == nil {
			return
		}

		recvBody, e := ioutil.ReadAll(response.Body)
		if e != nil {
			err = fmt.Errorf("get data from res.Body fail, %w", e)
			return
		}

		e = json.Unmarshal(recvBody, recv)
		if e != nil {
			err = fmt.Errorf("unmarshal recv[%T] from [%s] fail, %w", recv, recvBody, e)
			return
		}
		return
	})

	return
}

func GetJSON(
	url string,
	timeout time.Duration,
	send interface{},
	recv interface{},
	onPrepare func(*http.Request) (*http.Request, error),
) error {
	return DoJSONTask("GET", url, timeout, send, recv, onPrepare)
}

func PostJSON(
	url string,
	timeout time.Duration,
	send interface{},
	recv interface{},
	onPrepare func(*http.Request) (*http.Request, error),
) error {
	return DoJSONTask("POST", url, timeout, send, recv, onPrepare)
}
