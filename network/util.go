package network

import (
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"strconv"
	"strings"
)

func GetMacAddrs() ([]string, error) {
	addrs := []string{}

	netInterfaces, err := net.Interfaces()
	if err != nil {
		return nil, fmt.Errorf("fail to get net interfaces, %v", err)
	}

	for _, netInterface := range netInterfaces {
		addr := netInterface.HardwareAddr.String()
		if len(addr) == 0 {
			continue
		}
		addrs = append(addrs, addr)
	}

	return addrs, nil
}

func GetMacAddr() (string, error) {
	addrs, err := GetMacAddrs()
	if err != nil {
		return "", err
	}
	if len(addrs) > 0 {
		return addrs[0], nil
	}
	return "", nil
}

func MustGetMacAddr() string {
	addr, err := GetMacAddr()
	if err != nil {
		log.Panicf("fail to get mac addr, %v", err)
	}
	if addr == "" {
		log.Panicf("mac addr is empty")
	}
	return addr
}

func GetIPs() ([]string, error) {
	var ips []string

	interfaceAddrs, err := net.InterfaceAddrs()
	if err != nil {
		return nil, fmt.Errorf("fail to get net interface addrs, %v", err)
	}

	for _, addr := range interfaceAddrs {
		ipNet, isValid := addr.(*net.IPNet)
		if isValid && !ipNet.IP.IsLoopback() {
			if ipNet.IP.To4() != nil {
				ips = append(ips, ipNet.IP.String())
			}
		}
	}

	return ips, nil
}

// func GetIP() (string, error) {
// 	ips, err := GetIPs()
// 	if err != nil {
// 		return "", err
// 	}
// 	if len(ips) > 0 {
// 		return ips[0], nil
// 	}
// 	return "", nil
// }

// func MustGetIP() string {
// 	ip, err := GetIP()
// 	if err != nil {
// 		log.Panicf("get ip fail, %v", err)
// 	}
// 	if ip == "" {
// 		log.Panicf("ip is empty")
// 	}
// 	return ip
// }

func GetInternalIP() (string, error) {
	// 思路来自于Python版本的内网IP获取，其他版本不准确
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		return "", fmt.Errorf("get internal ip failed, %v", err)
	}
	defer conn.Close()

	// udp 面向无连接，所以这些东西只在你本地捣鼓
	ip := conn.LocalAddr().String()
	ip = strings.Split(ip, ":")[0]
	return ip, nil
}

func MustGetInternalIP() string {
	ip, err := GetInternalIP()
	if err != nil {
		panic(err)
	}
	if ip == "" {
		log.Panicf("internal ip is empty")
	}
	return ip
}

func GetExternalIP() (string, error) {
	// 有很多类似网站提供这种服务，这是我知道且正在用的
	// 备用：https://myexternalip.com/raw （cip.cc 应该是够快了，我连手机热点的时候不太稳，其他自己查）
	response, err := http.Get("http://ip.cip.cc")
	if err != nil {
		return "", fmt.Errorf("get external ip failed, %v", err)
	}

	defer response.Body.Close()
	ip := ""

	// 类似的API应当返回一个纯净的IP地址
	for {
		tmp := make([]byte, 32)
		n, err := response.Body.Read(tmp)
		if err != nil {
			if err != io.EOF {
				return "", fmt.Errorf("get external ip failed, %v", err)
			}
			ip += string(tmp[:n])
			break
		}
		ip += string(tmp[:n])
	}

	return strings.TrimSpace(ip), nil
}

func SplitHostPort(hostport string) (string, int, error) {
	host, sPort, err := net.SplitHostPort(hostport)
	if err != nil {
		return "", 0, err
	}
	port, err := strconv.Atoi(sPort)
	if err != nil {
		return "", 0, fmt.Errorf("convert port from string to int fail, %v", err)
	}
	return host, port, nil
}

func JoinHostPort(host string, port int) string {
	sPort := strconv.Itoa(port)
	return net.JoinHostPort(host, sPort)
}
