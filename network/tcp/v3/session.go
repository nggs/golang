package link

import (
	"errors"
	"sync"
	"sync/atomic"

	v3 "nggs/network/protocol/protobuf/v3"
)

var SessionClosedError = errors.New("Session Closed")
var SessionBlockedError = errors.New("Session Blocked")

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type SessionID = uint64

var gNextSessionID uint64

// todo 将id生成规则替换成snowflake
func nextSessionID() SessionID {
	return atomic.AddUint64(&gNextSessionID, 1)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type ISessionHandler interface {
	HandleMessage(*Session, []byte)
	HandleError(*Session, error)
	//HandleClose(*Session, int, string) error
	HandleConnect(*Session) error
	HandleDisconnect(*Session)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type SessionHandler struct {
}

func (SessionHandler) HandleMessage(*Session, []byte) {
}

func (SessionHandler) HandleError(*Session, error) {
}

//func (SessionHandler) HandleClose(*Session, int, string) error {
//	return nil
//}

func (SessionHandler) HandleConnect(*Session) error {
	return nil
}

func (SessionHandler) HandleDisconnect(*Session) {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type Session struct {
	id        uint64
	codec     Codec
	manager   *Manager
	sendChan  chan v3.IMessage
	recvMutex sync.Mutex
	sendMutex sync.RWMutex

	closeFlag          int32
	closeChan          chan int
	closeMutex         sync.Mutex
	firstCloseCallback *closeCallback
	lastCloseCallback  *closeCallback

	State interface{}
}

func NewSession(codec Codec, sendChanSize int) *Session {
	return newSession(nil, codec, sendChanSize)
}

func newSession(manager *Manager, codec Codec, sendChanSize int) *Session {
	session := &Session{
		codec:     codec,
		manager:   manager,
		closeChan: make(chan int),
		id:        nextSessionID(),
	}
	if sendChanSize > 0 {
		session.sendChan = make(chan v3.IMessage, sendChanSize)
		go session.sendLoop()
	}
	return session
}

func (session *Session) ID() uint64 {
	return session.id
}

func (session *Session) IsClosed() bool {
	return atomic.LoadInt32(&session.closeFlag) == 1
}

func (session *Session) Close() error {
	if atomic.CompareAndSwapInt32(&session.closeFlag, 0, 1) {
		close(session.closeChan)

		if session.sendChan != nil {
			session.sendMutex.Lock()
			close(session.sendChan)
			if clear, ok := session.codec.(ClearSendChan); ok {
				clear.ClearSendChan(session.sendChan)
			}
			session.sendMutex.Unlock()
		}

		err := session.codec.Close()

		go func() {
			session.invokeCloseCallbacks()

			if session.manager != nil {
				session.manager.delSession(session)
			}
		}()
		return err
	}
	return SessionClosedError
}

func (session *Session) Codec() Codec {
	return session.codec
}

func (session *Session) Receive() (iMsg v3.IMessage, seq v3.MessageSeq, err error) {
	session.recvMutex.Lock()
	defer session.recvMutex.Unlock()

	iMsg, seq, err = session.codec.Receive()
	if err != nil {
		session.Close()
	}
	return
}

func (session *Session) sendLoop() {
	defer session.Close()
	for {
		select {
		case msg, ok := <-session.sendChan:
			if !ok || session.codec.Send(msg, 0) != nil {
				return
			}
		case <-session.closeChan:
			return
		}
	}
}

func (session *Session) Send(iMsg v3.IMessage, seq v3.MessageSeq) error {
	if session.sendChan == nil {
		if session.IsClosed() {
			return SessionClosedError
		}

		session.sendMutex.Lock()
		defer session.sendMutex.Unlock()

		err := session.codec.Send(iMsg, seq)
		if err != nil {
			session.Close()
		}
		return err
	}

	session.sendMutex.RLock()
	if session.IsClosed() {
		session.sendMutex.RUnlock()
		return SessionClosedError
	}

	select {
	case session.sendChan <- iMsg:
		session.sendMutex.RUnlock()
		return nil
	default:
		session.sendMutex.RUnlock()
		session.Close()
		return SessionBlockedError
	}
}

type closeCallback struct {
	Handler interface{}
	Key     interface{}
	Func    func()
	Next    *closeCallback
}

func (session *Session) AddCloseCallback(handler, key interface{}, callback func()) {
	if session.IsClosed() {
		return
	}

	session.closeMutex.Lock()
	defer session.closeMutex.Unlock()

	newItem := &closeCallback{handler, key, callback, nil}

	if session.firstCloseCallback == nil {
		session.firstCloseCallback = newItem
	} else {
		session.lastCloseCallback.Next = newItem
	}
	session.lastCloseCallback = newItem
}

func (session *Session) RemoveCloseCallback(handler, key interface{}) {
	if session.IsClosed() {
		return
	}

	session.closeMutex.Lock()
	defer session.closeMutex.Unlock()

	var prev *closeCallback
	for callback := session.firstCloseCallback; callback != nil; prev, callback = callback, callback.Next {
		if callback.Handler == handler && callback.Key == key {
			if session.firstCloseCallback == callback {
				session.firstCloseCallback = callback.Next
			} else {
				prev.Next = callback.Next
			}
			if session.lastCloseCallback == callback {
				session.lastCloseCallback = prev
			}
			return
		}
	}
}

func (session *Session) invokeCloseCallbacks() {
	session.closeMutex.Lock()
	defer session.closeMutex.Unlock()

	for callback := session.firstCloseCallback; callback != nil; callback = callback.Next {
		callback.Func()
	}
}
