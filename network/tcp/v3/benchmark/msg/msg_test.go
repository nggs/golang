package msg

import (
	"bytes"
	"math"
	"sync"
	"testing"

	"nggs/random"

	link "nggs/network/tcp/v3"

	"github.com/funny/utest"
)

func test(t *testing.T, sendChanSize int, test func(*testing.T, *link.Session)) {
	server, err := link.Listen("0.0.0.0:0", Protocol, sendChanSize, link.HandlerFunc(func(session *link.Session) {
		defer session.Close()
		for {
			iMsg, seq, err := session.Receive()
			if err != nil {
				return
			}
			err = session.Send(iMsg, seq)
			if err != nil {
				return
			}
		}
	}))
	utest.IsNilNow(t, err)
	go server.Serve()

	addr := server.Listener().Addr().String()

	clientWait := new(sync.WaitGroup)
	for i := 0; i < 100; i++ {
		clientWait.Add(1)
		go func() {
			session, err := link.Dial(addr, Protocol, sendChanSize)
			utest.IsNilNow(t, err)
			test(t, session)
			session.Close()
			clientWait.Done()
		}()
	}
	clientWait.Wait()

	server.Stop()
}

func Test(t *testing.T) {
	test(t, 0, func(t *testing.T, session *link.Session) {
		send := Get_C2S_Ping()
		send.Content = random.String(512)
		sendSeq := uint32(random.Int(0, math.MaxInt32))
		err := session.Send(send, sendSeq)
		utest.IsNilNow(t, err)

		iRecv, recvSeq, err := session.Receive()
		utest.IsNilNow(t, err)
		recv := iRecv.(*C2S_Ping)
		utest.Assert(t, bytes.Equal([]byte(send.Content), []byte(recv.Content)))
		utest.Assert(t, sendSeq == recvSeq)
	})
}
