@setlocal enabledelayedexpansion enableextensions
@set PROTO_FILES=
@for %%x in (*.proto) do @set PROTO_FILES=!PROTO_FILES! %%x
@set PROTO_FILES=%PROTO_FILES:~1%

protoc -I=. --gogofaster_out=. %PROTO_FILES%
protoc -I=. --pbex-go_out=. --pbex-go_opt=version:v1;tpl:v1.tpl %PROTO_FILES%