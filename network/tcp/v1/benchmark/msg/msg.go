package msg

import (
	"nggs/network/tcp/v1/codec"

	jsoniter "github.com/json-iterator/go"
)

const (
	sizeofRecvBuff = 2 * 1024
	sizeofSendBuff = 2 * 1024
)

var Protocol = codec.ProtoBuf(sizeofRecvBuff, sizeofSendBuff, nil, nil /*p.EncryptFunc(p.DefaultDecryptor)*/, nil /*p.DecryptFunc(p.DefaultDecryptor)*/)

var json = jsoniter.ConfigCompatibleWithStandardLibrary
