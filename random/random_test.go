package random

import (
	"log"
	"testing"
)

func Test_ByteArray(t *testing.T) {
	for i := 0; i < 10; i++ {
		log.Println(ByteArray(16))
	}
}

func Benchmark_ByteArray(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ByteArray(Int(8, 32))
	}
}

func Test_String(t *testing.T) {
	for i := 0; i < 10; i++ {
		log.Println(String(64))
	}
}

func Benchmark_String(b *testing.B) {
	for i := 0; i < b.N; i++ {
		String(Int(8, 32))
	}
}

func Test_DiscreteDistributionDice(t *testing.T) {
	dice := NewDiscreteDistributionDice()

	dice.Append(NewDiscreteDistributionCandidate(1, func(ctx interface{}) (int, interface{}) {
		return 20, 1
	})).Append(NewDiscreteDistributionCandidate(2, func(ctx interface{}) (int, interface{}) {
		return 10, 2
	})).Append(NewDiscreteDistributionCandidate(3, func(ctx interface{}) (int, interface{}) {
		return 20, 3
	})).Append(NewDiscreteDistributionCandidate(4, func(ctx interface{}) (int, interface{}) {
		return 5, 4
	})).Append(NewDiscreteDistributionCandidate(5, func(ctx interface{}) (int, interface{}) {
		return 0, 5
	})).Append(NewDiscreteDistributionCandidate(6, func(ctx interface{}) (int, interface{}) {
		return 45, 6
	}))

	err := dice.Build()
	if err != nil {
		t.Error(err)
		return
	}

	log.Printf("dice=%v\n", dice)

	hits := map[int]int{}
	for i := 0; i < 100000; i++ {
		ctx, _ := dice.Roll()
		hits[ctx.(int)] += 1
	}

	log.Printf("hits=%v\n", hits)
}

func Benchmark_DiscreteDistributionDice(b *testing.B) {
	dice := NewDiscreteDistributionDice()

	dice.Append(NewDiscreteDistributionCandidate(1, func(ctx interface{}) (int, interface{}) {
		return 20, 1
	})).Append(NewDiscreteDistributionCandidate(2, func(ctx interface{}) (int, interface{}) {
		return 10, 2
	})).Append(NewDiscreteDistributionCandidate(3, func(ctx interface{}) (int, interface{}) {
		return 20, 3
	})).Append(NewDiscreteDistributionCandidate(4, func(ctx interface{}) (int, interface{}) {
		return 5, 4
	})).Append(NewDiscreteDistributionCandidate(5, func(ctx interface{}) (int, interface{}) {
		return 0, 5
	})).Append(NewDiscreteDistributionCandidate(6, func(ctx interface{}) (int, interface{}) {
		return 45, 6
	}))

	err := dice.Build()
	if err != nil {
		b.Error(err)
		return
	}

	//log.Printf("dice=%v\n", dice)

	hits := map[int]int{}
	for i := 0; i < b.N; i++ {
		ctx, _ := dice.Roll()
		hits[ctx.(int)] += 1
	}

	//log.Printf("hits=%v\n", hits)
}

func Test_NormalDistributionRandom(t *testing.T) {
	for i := 0; i < 10000; i++ {
		t.Log(NormalDistributionRandom(80, 40))
	}
}

func Test_NormalDiscreteDistributionInt(t *testing.T) {
	var weights = map[int]int{1: 50, 2: 30, 3: 19, 4: 0, 5: 1}
	var fixRates, _ = GenRandomRatesInt(weights)

	var randomRates map[int]float64
	var hit int
	var hits = map[int]int{}
	for i := 0; i < 1000000; i++ {
		randomRates, hit, _ = NormalDiscreteDistributionInt(fixRates, randomRates)
		hits[hit] += 1
	}

	t.Logf("hits=%v\n", hits)
}

func Test_NormalDiscreteDistributionDice(t *testing.T) {
	dice := NewNormalDiscreteDistributionIntDice()

	dice.Append(NewDiscreteDistributionCandidate(1, func(ctx interface{}) (int, interface{}) {
		return 20, 1
	})).Append(NewDiscreteDistributionCandidate(2, func(ctx interface{}) (int, interface{}) {
		return 10, 2
	})).Append(NewDiscreteDistributionCandidate(3, func(ctx interface{}) (int, interface{}) {
		return 20, 3
	})).Append(NewDiscreteDistributionCandidate(4, func(ctx interface{}) (int, interface{}) {
		return 5, 4
	})).Append(NewDiscreteDistributionCandidate(5, func(ctx interface{}) (int, interface{}) {
		return 0, 5
	})).Append(NewDiscreteDistributionCandidate(6, func(ctx interface{}) (int, interface{}) {
		return 45, 6
	}))

	err := dice.Build()
	if err != nil {
		t.Error(err)
		return
	}

	log.Printf("dice=%+v\n", dice)

	hits := map[int]int{}
	for i := 0; i < 1000000; i++ {
		ctx, _ := dice.Roll()
		hit := ctx.(int)
		hits[hit] += 1
	}

	log.Printf("hits=%v\n", hits)
}

func Benchmark_NormalDiscreteDistributionDice(b *testing.B) {
	dice := NewNormalDiscreteDistributionIntDice()

	dice.Append(NewDiscreteDistributionCandidate(1, func(ctx interface{}) (int, interface{}) {
		return 20, 1
	})).Append(NewDiscreteDistributionCandidate(2, func(ctx interface{}) (int, interface{}) {
		return 10, 2
	})).Append(NewDiscreteDistributionCandidate(3, func(ctx interface{}) (int, interface{}) {
		return 20, 3
	})).Append(NewDiscreteDistributionCandidate(4, func(ctx interface{}) (int, interface{}) {
		return 5, 4
	})).Append(NewDiscreteDistributionCandidate(5, func(ctx interface{}) (int, interface{}) {
		return 0, 5
	})).Append(NewDiscreteDistributionCandidate(6, func(ctx interface{}) (int, interface{}) {
		return 45, 6
	}))

	err := dice.Build()
	if err != nil {
		b.Error(err)
		return
	}

	//log.Printf("dice=%+v\n", dice)

	hits := map[int]int{}
	for i := 0; i < b.N; i++ {
		ctx, _ := dice.Roll()
		hit := ctx.(int)
		hits[hit] += 1
	}

	//log.Printf("hits=%v\n", hits)
}

func Test_Coin(t *testing.T) {
	c, err := NewCoin(0.02)
	if err != nil {
		t.Error(err)
		return
	}

	hits := map[bool]int{}
	for i := 0; i < 1000000; i++ {
		hit := c.Roll()
		hits[hit] += 1
	}

	log.Printf("hits=%v\n", hits)
}
