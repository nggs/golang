package random

import "errors"

var ErrWeightsIsEmpty = errors.New("weights is empty")

var ErrTotalWeightsIsZero = errors.New("total weights is 0")

var ErrRawRatesIsEmpty = errors.New("raw randomRates is empty")

var ErrCandidateIsEmpty = errors.New("candidate is empty")

var ErrNotHit = errors.New("not hit")

var ErrInvalidRate = errors.New("invalid rate, must > 0 and <= 1")
