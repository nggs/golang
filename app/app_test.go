package app

import (
	"testing"

	nlog "nggs/log"
)

type config struct {
	Config
}

func newConfig() *config {
	return &config{}
}

func TestSuper_Run(t *testing.T) {
	cfg := newConfig()
	cfg.SetID(1)

	app := New(cfg, nlog.NewConsoleLogger())
	err := app.Run()
	if err != nil {
		t.Errorf("start app fail, %s", err)
		return
	}
}
