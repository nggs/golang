package util

import (
	"testing"
)

func Test_FormatFieldName(t *testing.T) {
	names := []string{
		"id",
		"Id",
		"idle",
		"testId",
		"testId1",
		"testId12",
		"testiD123",
		"testID22",
		"testId99",
		"testid1",
		"test12",
	}
	for _, name := range names {
		t.Logf("%s => %s\n", name, FormatFieldName(name))
	}
}

func Test_ToLower(t *testing.T) {
	ss := []string{
		"id",
		"Id",
		"TestString",
		"testString",
		"teststring",
	}
	for _, s := range ss {
		t.Logf("%s => %s\n", s, LowerFirst(s))
	}
}
