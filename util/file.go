package util

import (
	"os"
	"path"
	"path/filepath"
	"strings"
)

func GetFileName(file string) string {
	_, file = filepath.Split(file)
	return file
}

func TruncateExtend(file string) string {
	if len(file) > 0 {
		ext := path.Ext(file)
		if len(ext) > 0 {
			return strings.TrimSuffix(file, ext)
		}
	}
	return file
}

func GetFileBaseName(file string) string {
	file = GetFileName(file)
	file = TruncateExtend(file)
	return file
}

// 获取当前可执行文件绝对路径
func GetProgramFileAbsPath() (string, error) {
	path, err := os.Executable()
	if err != nil {
		return "", err
	}
	path = filepath.ToSlash(path)
	return path, nil
}

func MustGetProgramFileAbsPath() string {
	path, err := GetProgramFileAbsPath()
	if err != nil {
		panic(err)
	}
	return path
}

// 获取当前可执行文件名（含扩展名）
func GetProgramFileName() (string, error) {
	path, err := GetProgramFileAbsPath()
	if err != nil {
		return "", err
	}
	_, file := filepath.Split(path)
	return file, nil
}

func MustGetProgramFileName() string {
	file, err := GetProgramFileName()
	if err != nil {
		panic(err)
	}
	return file
}

// 获取当前可执行文件名（不含扩展名）
func GetProgramFileBaseName() (string, error) {
	file, err := GetProgramFileName()
	if err != nil {
		return "", err
	}
	file = TruncateExtend(file)
	return file, nil
}

func MustGetProgramFileBaseName() string {
	file, err := GetProgramFileBaseName()
	if err != nil {
		panic(err)
	}
	return file
}

// 如果目录不存在创建指定目录
func IsDirOrFileExist(path string) error {
	_, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			return err
		}
		panic(err)
	}
	return nil
}

// 如果目录不存在创建指定目录
func MustMkdirIfNotExist(path string) {
	if err := IsDirOrFileExist(path); err != nil {
		os.MkdirAll(path, os.ModePerm)
	}
}

func Walk(dir string, extend string, whiteList []string, blackList []string, fn func(path string, info os.FileInfo, name string, isInBlackList bool) (continued bool)) (err error) {
	absDir, err := filepath.Abs(dir)
	if err != nil {
		return
	}

	dotExtend := "." + extend

	if len(whiteList) > 0 {
		for _, w := range whiteList {
			p := filepath.Join(absDir, w+dotExtend)
			info, err := os.Lstat(p)
			if err != nil {
				continue
			}
			if !fn(p, info, w, false) {
				break
			}
		}
	} else {
		ms, err := filepath.Glob(filepath.Join(absDir, "*."+extend))
		if err != nil {
			return err
		}
		for _, m := range ms {
			info, err := os.Lstat(m)
			if err != nil {
				continue
			}
			name := strings.TrimSuffix(info.Name(), dotExtend)
			var isInBlackList bool
			if len(blackList) > 0 {
				for _, n := range blackList {
					if name == n {
						isInBlackList = true
						break
					}
				}
			}
			if !fn(m, info, name, isInBlackList) {
				break
			}
		}
	}

	return
}
