//go:build ignore
// +build ignore

package service

import (
	"fmt"
	"sync"

	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	napp "nggs/app"
	nexport "nggs/export"
	nlog "nggs/log"
)

type Service struct {
	*nactor.Actor

	appCfg *napp.Config

	instance *Instance
}

func New() *Service {
	svc := &Service{
		instance: NewInstance(),
	}
	return svc
}

func (svc *Service) Init(iAppConfig nexport.IAppConfig, serviceID int, startedWg *sync.WaitGroup, stoppedWg *sync.WaitGroup, args ...interface{}) (err error) {
	var ok bool
	svc.appCfg, ok = iAppConfig.(*napp.Config)
	if !ok || svc.appCfg == nil {
		err = fmt.Errorf("init app config fail")
		return
	}

	// 根据配置初始化日志
	if svc.appCfg.LogDir != "" {
		logDir := fmt.Sprintf("%s/%s-%d", svc.appCfg.LogDir, ServiceName, svc.cfg.GetID())
		logger = nlog.New(logDir, ServiceName)
		logger.SetLevel(initLogLevel)
	}

	return
}

func (svc *Service) Run(ctx actor.Context, pprofAddress string, args ...interface{}) (err error) {
	err = svc.Actor.Start(ctx, fmt.Sprintf("%s-%d", ServiceActorName, svc.cfg.GetID()))
	if err != nil {
		return
	}

	svc.instance.PID = svc.PID()
	svc.instance.PprofAddress = pprofAddress

	return
}
