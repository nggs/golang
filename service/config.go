package service

import (
	"fmt"
	"time"
)

const GlobalConfigServiceID = "global"

type Config struct {
	ID            int
	PutPayloadSec time.Duration // 更新负载间隔(单位:秒)
}

func NewConfig() *Config {
	return &Config{
		ID:            0,
		PutPayloadSec: 5 * time.Second,
	}
}

func (c *Config) TidyAndCheck() error {
	if c.ID <= 0 {
		return fmt.Errorf("invalid Service.ID")
	}

	if c.PutPayloadSec > 0 {
		if c.PutPayloadSec < 5 {
			return fmt.Errorf("invalid Service.PutPayloadSec, must >= 5")
		}
	} else {
		c.PutPayloadSec = 5
	}
	c.PutPayloadSec = c.PutPayloadSec * time.Second

	return nil
}

func (c Config) GetID() int {
	return c.ID
}

func (c *Config) SetID(id int) {
	c.ID = id
}

func (c Config) GetPutPayloadSec() time.Duration {
	return c.PutPayloadSec
}

func (c *Config) SetPutPayloadSec(putPayloadSec time.Duration) {
	c.PutPayloadSec = putPayloadSec
}
