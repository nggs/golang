package service

import "errors"

var ErrProducerIsNil = errors.New("producer is nil")

var ErrDuplicateFactory = errors.New("duplicate factory")

var ErrUnsupportedService = errors.New("unsupported service")
