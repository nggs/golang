package static_data

// xlsx表类型到go类型的映射
var gXlsxType2GoType = map[string]string{
	"num": "int",
	"str": "string",
}

func XlsxType2GoType(i string) (o string, ok bool) {
	o, ok = gXlsxType2GoType[i]
	return
}

//var gSupportedGoBaseTypes = map[string]struct{}{
//	"int":     struct{}{},
//	"string":  struct{}{},
//	"bool":    struct{}{},
//	"float64": struct{}{},
//}
