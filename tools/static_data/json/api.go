package json

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"nggs/tools/static_data"
)

func ReadJsonFile(path string, info os.FileInfo, enums []*static_data.Enum, pkg string, replaceNames map[string]string) (tbl *Table, err error) {
	bs, err := ioutil.ReadFile(path)
	if err != nil {
		return
	}

	tbl = NewTable(pkg, enums)

	err = json.Unmarshal(bs, tbl)
	if err != nil {
		return
	}

	err = tbl.format(info, replaceNames)

	return
}
