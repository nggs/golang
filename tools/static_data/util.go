package static_data

import (
	"fmt"
	"strings"

	"github.com/tealeg/xlsx"
)

func ParseSchemaString(input string) (output map[string]string) {
	output = make(map[string]string)
	kvs := strings.Split(input, ";")
	for _, kv := range kvs {
		pair := strings.Split(kv, "=")
		if len(pair) == 1 {
			output[strings.TrimSpace(kv)] = ""
		} else if len(pair) == 2 {
			output[strings.TrimSpace(pair[0])] = strings.TrimSpace(pair[1])
		}
	}
	return
}

// 数据是否可以跳过
func CanIgnoreSheetCell(cell *xlsx.Cell) (ignore bool, err error) {
	if cell == nil {
		err = fmt.Errorf("cell is nil")
		ignore = true
		return
	}
	if cell.Value == "" {
		err = fmt.Errorf("cell value is empty")
		ignore = true
		return
	}
	ignore = cell.Value[0] == '#'
	return
}

// 行是否可以跳过
func CanIgnoreSheetRow(sheet *xlsx.Sheet, row int) (ignore bool, err error) {
	if sheet == nil {
		err = fmt.Errorf("sheet is nil")
		ignore = true
		return
	}
	return CanIgnoreSheetCell(sheet.Cell(row, 0))
}
