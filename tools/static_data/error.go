package static_data

type ErrNeedSchema struct {
}

func (e ErrNeedSchema) Error() string {
	return "需要结构信息"
}

type ErrNotField struct {
}

func (e ErrNotField) Error() string {
	return "不是字段"
}

type ErrNeedType struct {
}

func (e ErrNeedType) Error() string {
	return "需要类型信息"
}

type ErrIgnoreField struct {
}

func (e ErrIgnoreField) Error() string {
	return "被忽略的字段"
}

type ErrTableFieldEmpty struct {
}

func (e ErrTableFieldEmpty) Error() string {
	return "表字段是空的"
}
