package static_data

import (
	"regexp"
)

func init() {
	// 匹配字母开头，最多2位数字结尾的切片字段，具体参考测试用例TestArrayRegexp
	gSlicePattern = regexp.MustCompile(`^[A-Za-z]+[0-9][0-9]{0,1}$`)
}
