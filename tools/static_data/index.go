package static_data

type IndexType int

const (
	IndexTypeNot    IndexType = iota // 不是索引
	IndexTypeUnique                  // 唯一索引，值不可以重复
	IndexTypeNormal                  // 普通索引，值可以重复

	IndexNameNot    = ""
	IndexNameUnique = "unique"
	IndexNameNormal = "normal"
)

// 索引类型到名字的映射
var gIndexName2Type = map[string]IndexType{
	IndexNameNot:    IndexTypeNot,
	IndexNameUnique: IndexTypeUnique,
	IndexNameNormal: IndexTypeNormal,
}

func IndexName2Type(i string) (o IndexType, ok bool) {
	o, ok = gIndexName2Type[i]
	return
}

// 索引类型到名字的映射
var gIndexType2Name = map[IndexType]string{
	IndexTypeNot:    IndexNameNot,
	IndexTypeUnique: IndexNameUnique,
	IndexTypeNormal: IndexNameNormal,
}

func IndexType2Name(i IndexType) (o string, ok bool) {
	o, ok = gIndexType2Name[i]
	return
}

func (i IndexType) String() string {
	return gIndexType2Name[i]
}
