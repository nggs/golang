package static_data

import (
	"strings"
	"testing"

	"nggs/tools/print"
)

func TestParseSchemaString(t *testing.T) {
	t.Logf("%v\n", ParseSchemaString("t=AffixType;i=normal"))
	t.Logf("%v\n", ParseSchemaString("i=unique"))
	t.Logf("%v\n", ParseSchemaString("t=bool"))
	t.Logf("%v\n", ParseSchemaString("t = AffixType; i = normal"))
}

//func Test_newField(t *testing.T) {
//	{
//		// 名字为空，不是字段
//		f := newField(0, "", "num", "测试", "")
//		t.Logf("%+v\n", f)
//		err := f.format()
//		if err != nil && err.(*ErrNotField) == nil {
//			t.Errorf("%v\n", err)
//			return
//		}
//	}
//	{
//		// 类型为空，不是字段
//		f := newField(0, "test", "", "测试", "")
//		t.Logf("%+v\n", f)
//		err := f.format()
//		if err != nil && err.(*ErrNotField) == nil {
//			t.Errorf("%v\n", err)
//			return
//		}
//	}
//	{
//		// 类型非法
//		f := newField(0, "test", "n", "测试", "")
//		t.Logf("%+v\n", f)
//		err := f.format()
//		if err == nil {
//			t.Errorf("%v\n", err)
//			return
//		}
//	}
//	{
//		// 字段被注释掉
//		f := newField(0, "test", "str", "测试", "#")
//		t.Logf("%+v\n", f)
//		err := f.format()
//		if err != nil && err.(*ErrIgnoreField) == nil {
//			t.Errorf("%v\n", err)
//			return
//		}
//	}
//	{
//		// 字段被注释掉2
//		f := newField(0, "test", "str", "测试", "#t=AffixType")
//		t.Logf("%+v\n", f)
//		err := f.format()
//		if err != nil && err.(*ErrIgnoreField) == nil {
//			t.Errorf("%v\n", err)
//			return
//		}
//	}
//	{
//		// 修正字段名
//		f := newField(0, "test", "num", "测试", "n=Test")
//		t.Logf("%+v\n", f)
//		err := f.format()
//		if err != nil {
//			t.Errorf("%v\n", err)
//			return
//		}
//	}
//	{
//		// 修正字段类型
//		f := newField(0, "test", "num", "测试", "t=EquipType")
//		t.Logf("%+v\n", f)
//		err := f.format()
//		if err != nil {
//			t.Errorf("%v\n", err)
//			return
//		}
//	}
//	{
//		// 修正字段索引类型
//		f := newField(0, "test", "num", "测试", "i=normal")
//		t.Logf("%+v\n", f)
//		err := f.format()
//		if err != nil {
//			t.Errorf("%v\n", err)
//			return
//		}
//	}
//}

func TestRead(t *testing.T) {
	print.SetDebugMode(true)
	sd := New()
	var err error
	sd.Enums, err = ReadEnum("./enums.xlsx")
	if err != nil {
		t.Error(err)
		return
	}
	_, err = Read("../gen_static_data_json/excel/test.xlsx", []string{"[Table]"}, []string{"[Global]"}, []string{"[Datas]"}, true, sd, false)
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("%+v\n", sd)
}

func TestArrayRegexp(t *testing.T) {
	ss := map[string]bool{
		`itype0`:  true,
		`itype1`:  true,
		`itype2`:  true,
		`itype3`:  true,
		`itype10`: true,
		`itype99`: true,
		`a`:       false,
		`1`:       false,
		`a 1`:     false,
		` a1 `:    false,
	}
	for s, m := range ss {
		tm := gSlicePattern.MatchString(s)
		if tm != m {
			t.Errorf("[%s]=[%t] not pass", s, m)
			continue
		}
		if !tm {
			t.Logf("[%s] not match", s)
			continue
		}
		i := strings.IndexAny(s, "0123456789")
		if i > 0 {
			t.Logf("after cut num, [%s]=[%s]\n", s, s[:i])
		}
	}
}
