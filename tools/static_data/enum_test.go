package static_data

import (
	"encoding/json"
	"encoding/xml"
	"io/ioutil"
	"testing"

	"gopkg.in/yaml.v2"
)

func TestReadEnum(t *testing.T) {
	es, err := ReadEnum("./enums.xlsx")
	if err != nil {
		t.Error(err)
		return
	}

	bs, err := yaml.Marshal(es)
	if err != nil {
		t.Error(err)
		return
	}
	err = ioutil.WriteFile("./enums.yaml", bs, 0766)
	if err != nil {
		t.Error(err)
		return
	}

	bs, err = xml.MarshalIndent(es, "", "  ")
	if err != nil {
		t.Error(err)
		return
	}
	err = ioutil.WriteFile("./enums.xml", bs, 0766)
	if err != nil {
		t.Error(err)
		return
	}

	bs, err = json.MarshalIndent(es, "", "  ")
	if err != nil {
		t.Error(err)
		return
	}
	err = ioutil.WriteFile("./enums.json", bs, 0766)
	if err != nil {
		t.Error(err)
		return
	}

}
