package main

import (
	"io/ioutil"

	"nggs/tools/pbplugin"
	"nggs/tools/pbplugin/golang"
	cm "nggs/tools/pbplugin/golang/client_model"
	"nggs/tools/pbplugin/golang/command"
)

func main() {
	req := command.Read()

	reqParams := map[string]string{}
	if req.GetParameter() != "" {
		reqParams = pbplugin.ParseRequestParameterString(req.GetParameter())
	}

	p := cm.New()

	if templateFilePath, ok := reqParams["tpl"]; ok {
		templateContent, err := ioutil.ReadFile(templateFilePath)
		if err != nil {
			panic(err)
		}
		p.MustLoadTemplate("client-model-go", string(templateContent), golang.TemplateFunc)
	} else {
		p.MustLoadTemplate("client-model-go", cm.DefaultTemplate, golang.TemplateFunc)
	}

	resp := command.GeneratePlugin(req, p, ".cm.go")
	command.Write(resp)
}
