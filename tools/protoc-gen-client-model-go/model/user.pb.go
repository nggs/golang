// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: user.proto

package model

import (
	fmt "fmt"
	proto "github.com/gogo/protobuf/proto"
	io "io"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion2 // please upgrade the proto package

/// 测试结构体
//@map_key=int64
//@map_key=int32
//@slice
type Test struct {
	//@msg=i
	I32 int32 `protobuf:"varint,1,opt,name=i32,proto3" json:"i32,omitempty"`
	//@msg
	U32 uint32 `protobuf:"varint,2,opt,name=u32,proto3" json:"u32,omitempty"`
	//@msg
	Str string `protobuf:"bytes,3,opt,name=str,proto3" json:"str,omitempty"`
}

func (m *Test) Reset()         { *m = Test{} }
func (m *Test) String() string { return proto.CompactTextString(m) }
func (*Test) ProtoMessage()    {}
func (*Test) Descriptor() ([]byte, []int) {
	return fileDescriptor_116e343673f7ffaf, []int{0}
}
func (m *Test) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *Test) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_Test.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalTo(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *Test) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Test.Merge(m, src)
}
func (m *Test) XXX_Size() int {
	return m.Size()
}
func (m *Test) XXX_DiscardUnknown() {
	xxx_messageInfo_Test.DiscardUnknown(m)
}

var xxx_messageInfo_Test proto.InternalMessageInfo

func (m *Test) GetI32() int32 {
	if m != nil {
		return m.I32
	}
	return 0
}

func (m *Test) GetU32() uint32 {
	if m != nil {
		return m.U32
	}
	return 0
}

func (m *Test) GetStr() string {
	if m != nil {
		return m.Str
	}
	return ""
}

/// 用户数据
type User struct {
	/// 用户id
	//@bson=_id @msg
	ID int64 `protobuf:"varint,1,opt,name=ID,proto3" json:"ID,omitempty"`
	/// 帐号id
	// @msg
	AccountID int64 `protobuf:"varint,2,opt,name=AccountID,proto3" json:"AccountID,omitempty"`
	/// 服务器ID
	//@msg
	ServerID int32 `protobuf:"varint,3,opt,name=ServerID,proto3" json:"ServerID,omitempty"`
	/// 名字
	//@msg
	Name string `protobuf:"bytes,4,opt,name=Name,proto3" json:"Name,omitempty"`
	/// 性别
	//@msg
	Sex int32 `protobuf:"varint,5,opt,name=Sex,proto3" json:"Sex,omitempty"`
	/// 创建时刻
	//@msg
	CreateTime int64 `protobuf:"varint,6,opt,name=CreateTime,proto3" json:"CreateTime,omitempty"`
	/// 测试数组
	//@msg
	Arr []int32 `protobuf:"varint,7,rep,packed,name=Arr,proto3" json:"Arr,omitempty"`
	/// 测试字符串数组
	//@msg
	StrArr []string `protobuf:"bytes,8,rep,name=StrArr,proto3" json:"StrArr,omitempty"`
	/// 测试结构体
	//@msg
	Test *Test `protobuf:"bytes,9,opt,name=Test,proto3" json:"Test,omitempty"`
	/// 测试结构体切片
	//@msg
	Tests []*Test `protobuf:"bytes,10,rep,name=Tests,proto3" json:"Tests,omitempty"`
	/// 测试整型map
	//@msg
	IMap map[int32]int32 `protobuf:"bytes,11,rep,name=IMap,proto3" json:"IMap,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"varint,2,opt,name=value,proto3"`
	/// 测试结构体map
	//@msg
	TestMap map[int32]*Test `protobuf:"bytes,12,rep,name=TestMap,proto3" json:"TestMap,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
}

func (m *User) Reset()         { *m = User{} }
func (m *User) String() string { return proto.CompactTextString(m) }
func (*User) ProtoMessage()    {}
func (*User) Descriptor() ([]byte, []int) {
	return fileDescriptor_116e343673f7ffaf, []int{1}
}
func (m *User) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *User) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_User.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalTo(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *User) XXX_Merge(src proto.Message) {
	xxx_messageInfo_User.Merge(m, src)
}
func (m *User) XXX_Size() int {
	return m.Size()
}
func (m *User) XXX_DiscardUnknown() {
	xxx_messageInfo_User.DiscardUnknown(m)
}

var xxx_messageInfo_User proto.InternalMessageInfo

func (m *User) GetID() int64 {
	if m != nil {
		return m.ID
	}
	return 0
}

func (m *User) GetAccountID() int64 {
	if m != nil {
		return m.AccountID
	}
	return 0
}

func (m *User) GetServerID() int32 {
	if m != nil {
		return m.ServerID
	}
	return 0
}

func (m *User) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *User) GetSex() int32 {
	if m != nil {
		return m.Sex
	}
	return 0
}

func (m *User) GetCreateTime() int64 {
	if m != nil {
		return m.CreateTime
	}
	return 0
}

func (m *User) GetArr() []int32 {
	if m != nil {
		return m.Arr
	}
	return nil
}

func (m *User) GetStrArr() []string {
	if m != nil {
		return m.StrArr
	}
	return nil
}

func (m *User) GetTest() *Test {
	if m != nil {
		return m.Test
	}
	return nil
}

func (m *User) GetTests() []*Test {
	if m != nil {
		return m.Tests
	}
	return nil
}

func (m *User) GetIMap() map[int32]int32 {
	if m != nil {
		return m.IMap
	}
	return nil
}

func (m *User) GetTestMap() map[int32]*Test {
	if m != nil {
		return m.TestMap
	}
	return nil
}

func init() {
	proto.RegisterType((*Test)(nil), "model.Test")
	proto.RegisterType((*User)(nil), "model.User")
	proto.RegisterMapType((map[int32]int32)(nil), "model.User.IMapEntry")
	proto.RegisterMapType((map[int32]*Test)(nil), "model.User.TestMapEntry")
}

func init() { proto.RegisterFile("user.proto", fileDescriptor_116e343673f7ffaf) }

var fileDescriptor_116e343673f7ffaf = []byte{
	// 379 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x6c, 0x52, 0xbd, 0x6e, 0xe2, 0x40,
	0x18, 0x64, 0xbd, 0x36, 0xe0, 0x0f, 0xee, 0x74, 0x5a, 0xdd, 0x9d, 0x56, 0xe8, 0xe4, 0x33, 0x54,
	0x4e, 0xe3, 0xc2, 0x14, 0x89, 0xa2, 0x34, 0x24, 0x8e, 0x22, 0x17, 0x49, 0xb1, 0x26, 0x0f, 0xe0,
	0x90, 0x2d, 0x10, 0x3f, 0x46, 0x6b, 0x1b, 0xc1, 0x5b, 0xe4, 0xb1, 0x52, 0x52, 0xa6, 0x8c, 0x40,
	0x79, 0x8f, 0xe8, 0x5b, 0x03, 0xb1, 0x50, 0x2a, 0xcf, 0x37, 0x3b, 0x33, 0x9f, 0x77, 0x6c, 0x80,
	0x22, 0x93, 0xca, 0x5f, 0xa8, 0x34, 0x4f, 0x99, 0x35, 0x4b, 0x9f, 0xe5, 0xb4, 0x77, 0x05, 0xe6,
	0x50, 0x66, 0x39, 0xfb, 0x05, 0x74, 0xdc, 0x0f, 0x38, 0x71, 0x89, 0x67, 0x09, 0x84, 0xc8, 0x14,
	0xfd, 0x80, 0x1b, 0x2e, 0xf1, 0x7e, 0x08, 0x84, 0xc8, 0x64, 0xb9, 0xe2, 0xd4, 0x25, 0x9e, 0x2d,
	0x10, 0xf6, 0x3e, 0x28, 0x98, 0x8f, 0x99, 0x54, 0xec, 0x27, 0x18, 0x51, 0xa8, 0xdd, 0x54, 0x18,
	0x51, 0xc8, 0xfe, 0x81, 0x3d, 0x18, 0x8d, 0xd2, 0x62, 0x9e, 0x47, 0xa1, 0x8e, 0xa0, 0xe2, 0x8b,
	0x60, 0x1d, 0x68, 0xc6, 0x52, 0x2d, 0xa5, 0x8a, 0x42, 0x9d, 0x66, 0x89, 0xe3, 0xcc, 0x18, 0x98,
	0x0f, 0xc9, 0x4c, 0x72, 0x53, 0x6f, 0xd1, 0x18, 0x17, 0xc7, 0x72, 0xc5, 0xad, 0xf2, 0xe5, 0x62,
	0xb9, 0x62, 0x0e, 0xc0, 0x8d, 0x92, 0x49, 0x2e, 0x87, 0xe3, 0x99, 0xe4, 0x75, 0xbd, 0xa0, 0xc2,
	0xa0, 0x63, 0xa0, 0x14, 0x6f, 0xb8, 0x14, 0x1d, 0x03, 0xa5, 0xd8, 0x5f, 0xa8, 0xc7, 0xb9, 0x42,
	0xb2, 0xe9, 0x52, 0xcf, 0x16, 0xfb, 0x89, 0xfd, 0x2f, 0x0b, 0xe0, 0xb6, 0x4b, 0xbc, 0x56, 0xd0,
	0xf2, 0x75, 0x2d, 0x3e, 0x52, 0xa2, 0x6c, 0xa6, 0x0b, 0x16, 0x3e, 0x33, 0x0e, 0x2e, 0x3d, 0x55,
	0x94, 0x27, 0xec, 0x0c, 0xcc, 0xe8, 0x3e, 0x59, 0xf0, 0x96, 0x56, 0xfc, 0xd9, 0x2b, 0xb0, 0x18,
	0x1f, 0xf9, 0xdb, 0x79, 0xae, 0xd6, 0x42, 0x4b, 0x58, 0x00, 0x0d, 0xf4, 0xa0, 0xba, 0xad, 0xd5,
	0xbc, 0xaa, 0xde, 0x1f, 0x95, 0x86, 0x83, 0xb0, 0x73, 0x0e, 0xf6, 0x31, 0x06, 0x6f, 0x36, 0x91,
	0xeb, 0xc3, 0x87, 0x9a, 0xc8, 0x35, 0xfb, 0x0d, 0xd6, 0x32, 0x99, 0x16, 0x52, 0xf7, 0x6c, 0x89,
	0x72, 0xb8, 0x34, 0x2e, 0x48, 0xe7, 0x0e, 0xda, 0xd5, 0xc4, 0x6f, 0xbc, 0xdd, 0xaa, 0xf7, 0xf4,
	0x72, 0xc7, 0xa0, 0x6b, 0xfe, 0xba, 0x75, 0xc8, 0x66, 0xeb, 0x90, 0xf7, 0xad, 0x43, 0x5e, 0x76,
	0x4e, 0x6d, 0xb3, 0x73, 0x6a, 0x6f, 0x3b, 0xa7, 0xf6, 0x54, 0xd7, 0x7f, 0x53, 0xff, 0x33, 0x00,
	0x00, 0xff, 0xff, 0xf6, 0x7f, 0xc9, 0x8d, 0x5b, 0x02, 0x00, 0x00,
}

func (m *Test) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalTo(dAtA)
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *Test) MarshalTo(dAtA []byte) (int, error) {
	var i int
	_ = i
	var l int
	_ = l
	if m.I32 != 0 {
		dAtA[i] = 0x8
		i++
		i = encodeVarintUser(dAtA, i, uint64(m.I32))
	}
	if m.U32 != 0 {
		dAtA[i] = 0x10
		i++
		i = encodeVarintUser(dAtA, i, uint64(m.U32))
	}
	if len(m.Str) > 0 {
		dAtA[i] = 0x1a
		i++
		i = encodeVarintUser(dAtA, i, uint64(len(m.Str)))
		i += copy(dAtA[i:], m.Str)
	}
	return i, nil
}

func (m *User) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalTo(dAtA)
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *User) MarshalTo(dAtA []byte) (int, error) {
	var i int
	_ = i
	var l int
	_ = l
	if m.ID != 0 {
		dAtA[i] = 0x8
		i++
		i = encodeVarintUser(dAtA, i, uint64(m.ID))
	}
	if m.AccountID != 0 {
		dAtA[i] = 0x10
		i++
		i = encodeVarintUser(dAtA, i, uint64(m.AccountID))
	}
	if m.ServerID != 0 {
		dAtA[i] = 0x18
		i++
		i = encodeVarintUser(dAtA, i, uint64(m.ServerID))
	}
	if len(m.Name) > 0 {
		dAtA[i] = 0x22
		i++
		i = encodeVarintUser(dAtA, i, uint64(len(m.Name)))
		i += copy(dAtA[i:], m.Name)
	}
	if m.Sex != 0 {
		dAtA[i] = 0x28
		i++
		i = encodeVarintUser(dAtA, i, uint64(m.Sex))
	}
	if m.CreateTime != 0 {
		dAtA[i] = 0x30
		i++
		i = encodeVarintUser(dAtA, i, uint64(m.CreateTime))
	}
	if len(m.Arr) > 0 {
		dAtA2 := make([]byte, len(m.Arr)*10)
		var j1 int
		for _, num1 := range m.Arr {
			num := uint64(num1)
			for num >= 1<<7 {
				dAtA2[j1] = uint8(uint64(num)&0x7f | 0x80)
				num >>= 7
				j1++
			}
			dAtA2[j1] = uint8(num)
			j1++
		}
		dAtA[i] = 0x3a
		i++
		i = encodeVarintUser(dAtA, i, uint64(j1))
		i += copy(dAtA[i:], dAtA2[:j1])
	}
	if len(m.StrArr) > 0 {
		for _, s := range m.StrArr {
			dAtA[i] = 0x42
			i++
			l = len(s)
			for l >= 1<<7 {
				dAtA[i] = uint8(uint64(l)&0x7f | 0x80)
				l >>= 7
				i++
			}
			dAtA[i] = uint8(l)
			i++
			i += copy(dAtA[i:], s)
		}
	}
	if m.Test != nil {
		dAtA[i] = 0x4a
		i++
		i = encodeVarintUser(dAtA, i, uint64(m.Test.Size()))
		n3, err := m.Test.MarshalTo(dAtA[i:])
		if err != nil {
			return 0, err
		}
		i += n3
	}
	if len(m.Tests) > 0 {
		for _, msg := range m.Tests {
			dAtA[i] = 0x52
			i++
			i = encodeVarintUser(dAtA, i, uint64(msg.Size()))
			n, err := msg.MarshalTo(dAtA[i:])
			if err != nil {
				return 0, err
			}
			i += n
		}
	}
	if len(m.IMap) > 0 {
		for k, _ := range m.IMap {
			dAtA[i] = 0x5a
			i++
			v := m.IMap[k]
			mapSize := 1 + sovUser(uint64(k)) + 1 + sovUser(uint64(v))
			i = encodeVarintUser(dAtA, i, uint64(mapSize))
			dAtA[i] = 0x8
			i++
			i = encodeVarintUser(dAtA, i, uint64(k))
			dAtA[i] = 0x10
			i++
			i = encodeVarintUser(dAtA, i, uint64(v))
		}
	}
	if len(m.TestMap) > 0 {
		for k, _ := range m.TestMap {
			dAtA[i] = 0x62
			i++
			v := m.TestMap[k]
			msgSize := 0
			if v != nil {
				msgSize = v.Size()
				msgSize += 1 + sovUser(uint64(msgSize))
			}
			mapSize := 1 + sovUser(uint64(k)) + msgSize
			i = encodeVarintUser(dAtA, i, uint64(mapSize))
			dAtA[i] = 0x8
			i++
			i = encodeVarintUser(dAtA, i, uint64(k))
			if v != nil {
				dAtA[i] = 0x12
				i++
				i = encodeVarintUser(dAtA, i, uint64(v.Size()))
				n4, err := v.MarshalTo(dAtA[i:])
				if err != nil {
					return 0, err
				}
				i += n4
			}
		}
	}
	return i, nil
}

func encodeVarintUser(dAtA []byte, offset int, v uint64) int {
	for v >= 1<<7 {
		dAtA[offset] = uint8(v&0x7f | 0x80)
		v >>= 7
		offset++
	}
	dAtA[offset] = uint8(v)
	return offset + 1
}
func (m *Test) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if m.I32 != 0 {
		n += 1 + sovUser(uint64(m.I32))
	}
	if m.U32 != 0 {
		n += 1 + sovUser(uint64(m.U32))
	}
	l = len(m.Str)
	if l > 0 {
		n += 1 + l + sovUser(uint64(l))
	}
	return n
}

func (m *User) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if m.ID != 0 {
		n += 1 + sovUser(uint64(m.ID))
	}
	if m.AccountID != 0 {
		n += 1 + sovUser(uint64(m.AccountID))
	}
	if m.ServerID != 0 {
		n += 1 + sovUser(uint64(m.ServerID))
	}
	l = len(m.Name)
	if l > 0 {
		n += 1 + l + sovUser(uint64(l))
	}
	if m.Sex != 0 {
		n += 1 + sovUser(uint64(m.Sex))
	}
	if m.CreateTime != 0 {
		n += 1 + sovUser(uint64(m.CreateTime))
	}
	if len(m.Arr) > 0 {
		l = 0
		for _, e := range m.Arr {
			l += sovUser(uint64(e))
		}
		n += 1 + sovUser(uint64(l)) + l
	}
	if len(m.StrArr) > 0 {
		for _, s := range m.StrArr {
			l = len(s)
			n += 1 + l + sovUser(uint64(l))
		}
	}
	if m.Test != nil {
		l = m.Test.Size()
		n += 1 + l + sovUser(uint64(l))
	}
	if len(m.Tests) > 0 {
		for _, e := range m.Tests {
			l = e.Size()
			n += 1 + l + sovUser(uint64(l))
		}
	}
	if len(m.IMap) > 0 {
		for k, v := range m.IMap {
			_ = k
			_ = v
			mapEntrySize := 1 + sovUser(uint64(k)) + 1 + sovUser(uint64(v))
			n += mapEntrySize + 1 + sovUser(uint64(mapEntrySize))
		}
	}
	if len(m.TestMap) > 0 {
		for k, v := range m.TestMap {
			_ = k
			_ = v
			l = 0
			if v != nil {
				l = v.Size()
				l += 1 + sovUser(uint64(l))
			}
			mapEntrySize := 1 + sovUser(uint64(k)) + l
			n += mapEntrySize + 1 + sovUser(uint64(mapEntrySize))
		}
	}
	return n
}

func sovUser(x uint64) (n int) {
	for {
		n++
		x >>= 7
		if x == 0 {
			break
		}
	}
	return n
}
func sozUser(x uint64) (n int) {
	return sovUser(uint64((x << 1) ^ uint64((int64(x) >> 63))))
}
func (m *Test) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowUser
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: Test: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: Test: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field I32", wireType)
			}
			m.I32 = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowUser
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.I32 |= int32(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 2:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field U32", wireType)
			}
			m.U32 = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowUser
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.U32 |= uint32(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 3:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Str", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowUser
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthUser
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthUser
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Str = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		default:
			iNdEx = preIndex
			skippy, err := skipUser(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthUser
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthUser
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *User) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowUser
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: User: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: User: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field ID", wireType)
			}
			m.ID = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowUser
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.ID |= int64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 2:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field AccountID", wireType)
			}
			m.AccountID = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowUser
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.AccountID |= int64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 3:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field ServerID", wireType)
			}
			m.ServerID = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowUser
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.ServerID |= int32(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 4:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Name", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowUser
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthUser
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthUser
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Name = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 5:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Sex", wireType)
			}
			m.Sex = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowUser
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.Sex |= int32(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 6:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field CreateTime", wireType)
			}
			m.CreateTime = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowUser
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.CreateTime |= int64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 7:
			if wireType == 0 {
				var v int32
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return ErrIntOverflowUser
					}
					if iNdEx >= l {
						return io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					v |= int32(b&0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				m.Arr = append(m.Arr, v)
			} else if wireType == 2 {
				var packedLen int
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return ErrIntOverflowUser
					}
					if iNdEx >= l {
						return io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					packedLen |= int(b&0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				if packedLen < 0 {
					return ErrInvalidLengthUser
				}
				postIndex := iNdEx + packedLen
				if postIndex < 0 {
					return ErrInvalidLengthUser
				}
				if postIndex > l {
					return io.ErrUnexpectedEOF
				}
				var elementCount int
				var count int
				for _, integer := range dAtA[iNdEx:postIndex] {
					if integer < 128 {
						count++
					}
				}
				elementCount = count
				if elementCount != 0 && len(m.Arr) == 0 {
					m.Arr = make([]int32, 0, elementCount)
				}
				for iNdEx < postIndex {
					var v int32
					for shift := uint(0); ; shift += 7 {
						if shift >= 64 {
							return ErrIntOverflowUser
						}
						if iNdEx >= l {
							return io.ErrUnexpectedEOF
						}
						b := dAtA[iNdEx]
						iNdEx++
						v |= int32(b&0x7F) << shift
						if b < 0x80 {
							break
						}
					}
					m.Arr = append(m.Arr, v)
				}
			} else {
				return fmt.Errorf("proto: wrong wireType = %d for field Arr", wireType)
			}
		case 8:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field StrArr", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowUser
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthUser
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthUser
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.StrArr = append(m.StrArr, string(dAtA[iNdEx:postIndex]))
			iNdEx = postIndex
		case 9:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Test", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowUser
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthUser
			}
			postIndex := iNdEx + msglen
			if postIndex < 0 {
				return ErrInvalidLengthUser
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			if m.Test == nil {
				m.Test = &Test{}
			}
			if err := m.Test.Unmarshal(dAtA[iNdEx:postIndex]); err != nil {
				return err
			}
			iNdEx = postIndex
		case 10:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Tests", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowUser
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthUser
			}
			postIndex := iNdEx + msglen
			if postIndex < 0 {
				return ErrInvalidLengthUser
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Tests = append(m.Tests, &Test{})
			if err := m.Tests[len(m.Tests)-1].Unmarshal(dAtA[iNdEx:postIndex]); err != nil {
				return err
			}
			iNdEx = postIndex
		case 11:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field IMap", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowUser
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthUser
			}
			postIndex := iNdEx + msglen
			if postIndex < 0 {
				return ErrInvalidLengthUser
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			if m.IMap == nil {
				m.IMap = make(map[int32]int32)
			}
			var mapkey int32
			var mapvalue int32
			for iNdEx < postIndex {
				entryPreIndex := iNdEx
				var wire uint64
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return ErrIntOverflowUser
					}
					if iNdEx >= l {
						return io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					wire |= uint64(b&0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				fieldNum := int32(wire >> 3)
				if fieldNum == 1 {
					for shift := uint(0); ; shift += 7 {
						if shift >= 64 {
							return ErrIntOverflowUser
						}
						if iNdEx >= l {
							return io.ErrUnexpectedEOF
						}
						b := dAtA[iNdEx]
						iNdEx++
						mapkey |= int32(b&0x7F) << shift
						if b < 0x80 {
							break
						}
					}
				} else if fieldNum == 2 {
					for shift := uint(0); ; shift += 7 {
						if shift >= 64 {
							return ErrIntOverflowUser
						}
						if iNdEx >= l {
							return io.ErrUnexpectedEOF
						}
						b := dAtA[iNdEx]
						iNdEx++
						mapvalue |= int32(b&0x7F) << shift
						if b < 0x80 {
							break
						}
					}
				} else {
					iNdEx = entryPreIndex
					skippy, err := skipUser(dAtA[iNdEx:])
					if err != nil {
						return err
					}
					if skippy < 0 {
						return ErrInvalidLengthUser
					}
					if (iNdEx + skippy) > postIndex {
						return io.ErrUnexpectedEOF
					}
					iNdEx += skippy
				}
			}
			m.IMap[mapkey] = mapvalue
			iNdEx = postIndex
		case 12:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field TestMap", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowUser
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthUser
			}
			postIndex := iNdEx + msglen
			if postIndex < 0 {
				return ErrInvalidLengthUser
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			if m.TestMap == nil {
				m.TestMap = make(map[int32]*Test)
			}
			var mapkey int32
			var mapvalue *Test
			for iNdEx < postIndex {
				entryPreIndex := iNdEx
				var wire uint64
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return ErrIntOverflowUser
					}
					if iNdEx >= l {
						return io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					wire |= uint64(b&0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				fieldNum := int32(wire >> 3)
				if fieldNum == 1 {
					for shift := uint(0); ; shift += 7 {
						if shift >= 64 {
							return ErrIntOverflowUser
						}
						if iNdEx >= l {
							return io.ErrUnexpectedEOF
						}
						b := dAtA[iNdEx]
						iNdEx++
						mapkey |= int32(b&0x7F) << shift
						if b < 0x80 {
							break
						}
					}
				} else if fieldNum == 2 {
					var mapmsglen int
					for shift := uint(0); ; shift += 7 {
						if shift >= 64 {
							return ErrIntOverflowUser
						}
						if iNdEx >= l {
							return io.ErrUnexpectedEOF
						}
						b := dAtA[iNdEx]
						iNdEx++
						mapmsglen |= int(b&0x7F) << shift
						if b < 0x80 {
							break
						}
					}
					if mapmsglen < 0 {
						return ErrInvalidLengthUser
					}
					postmsgIndex := iNdEx + mapmsglen
					if postmsgIndex < 0 {
						return ErrInvalidLengthUser
					}
					if postmsgIndex > l {
						return io.ErrUnexpectedEOF
					}
					mapvalue = &Test{}
					if err := mapvalue.Unmarshal(dAtA[iNdEx:postmsgIndex]); err != nil {
						return err
					}
					iNdEx = postmsgIndex
				} else {
					iNdEx = entryPreIndex
					skippy, err := skipUser(dAtA[iNdEx:])
					if err != nil {
						return err
					}
					if skippy < 0 {
						return ErrInvalidLengthUser
					}
					if (iNdEx + skippy) > postIndex {
						return io.ErrUnexpectedEOF
					}
					iNdEx += skippy
				}
			}
			m.TestMap[mapkey] = mapvalue
			iNdEx = postIndex
		default:
			iNdEx = preIndex
			skippy, err := skipUser(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthUser
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthUser
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func skipUser(dAtA []byte) (n int, err error) {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return 0, ErrIntOverflowUser
			}
			if iNdEx >= l {
				return 0, io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= (uint64(b) & 0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		wireType := int(wire & 0x7)
		switch wireType {
		case 0:
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowUser
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				iNdEx++
				if dAtA[iNdEx-1] < 0x80 {
					break
				}
			}
			return iNdEx, nil
		case 1:
			iNdEx += 8
			return iNdEx, nil
		case 2:
			var length int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowUser
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				length |= (int(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if length < 0 {
				return 0, ErrInvalidLengthUser
			}
			iNdEx += length
			if iNdEx < 0 {
				return 0, ErrInvalidLengthUser
			}
			return iNdEx, nil
		case 3:
			for {
				var innerWire uint64
				var start int = iNdEx
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return 0, ErrIntOverflowUser
					}
					if iNdEx >= l {
						return 0, io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					innerWire |= (uint64(b) & 0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				innerWireType := int(innerWire & 0x7)
				if innerWireType == 4 {
					break
				}
				next, err := skipUser(dAtA[start:])
				if err != nil {
					return 0, err
				}
				iNdEx = start + next
				if iNdEx < 0 {
					return 0, ErrInvalidLengthUser
				}
			}
			return iNdEx, nil
		case 4:
			return iNdEx, nil
		case 5:
			iNdEx += 4
			return iNdEx, nil
		default:
			return 0, fmt.Errorf("proto: illegal wireType %d", wireType)
		}
	}
	panic("unreachable")
}

var (
	ErrInvalidLengthUser = fmt.Errorf("proto: negative length found during unmarshaling")
	ErrIntOverflowUser   = fmt.Errorf("proto: integer overflow")
)
