package log

import nlog "nggs/log"

var (
	logger = nlog.NewConsoleLogger()
)

func Logger() nlog.ILogger {
	return logger
}

func SetLogger(l nlog.ILogger) {
	logger = l
}
