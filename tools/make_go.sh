#!/usr/bin/env bash

WORK_DIR=$(pwd)
SRC_DIR=${WORK_DIR}/src
if [ ! -d ${SRC_DIR} ]; then
    # 如果src目录不存在, 则将WORK_DIR当成SRC_DIR
    SRC_DIR=${WORK_DIR}
fi
VENDOR_DIR=${SRC_DIR}/vendor

case $1 in
    glide-up)
    cd ${SRC_DIR}
    rm -rf ${VENDOR_DIR}
    #glide --debug up --strip-vendor
    glide up --strip-vendor
    rm -f ${VENDOR_DIR}/gitee.com/nggs/vendor.zip
    rm -f ${VENDOR_DIR}/gitee.com/lwj8507/light-protoactor-go/vendor.zip
    ;;

    zip-vendor)
    cd ${SRC_DIR}
    rm -rf ./vendor.zip
    zip -r vendor.zip ./vendor/*
    ;;

    unzip-vendor)
    cd ${SRC_DIR}
    rm -rf ./vendor
    unzip vendor.zip
    ;;

    *)
    echo unsupported operate
    ;;
esac