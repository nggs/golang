package main

import (
	"nggs/tools/pbplugin/golang"
	"nggs/tools/pbplugin/golang/generator"
)

const (
	DefaultResponseTimeoutMillisecond = "10000"
)

type message struct {
	*golang.Message
	Response        string
	ResponseTimeout string
}

func newMessage(g *generator.Generator, d *generator.Descriptor) *message {
	m := &message{
		Message:         golang.NewMessage(g, d),
		ResponseTimeout: DefaultResponseTimeoutMillisecond,
	}
	return m
}

type file struct {
	*golang.File
	Messages       []*message
	messagesByName map[string]*message
}

func newFile(fd *generator.FileDescriptor) *file {
	f := &file{
		File:           golang.NewFile(fd),
		messagesByName: map[string]*message{},
	}
	return f
}
