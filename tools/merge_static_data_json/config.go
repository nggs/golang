package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"nggs/util"
)

type config struct {
	JsonDir   string
	OutputDir string
	Debug     bool
	WhiteList []string
	BlackList []string
	Indent    string
}

func loadConfig(cfgFilePath string) (c *config, err error) {
	err = util.IsDirOrFileExist(cfgFilePath)
	if err != nil {
		return
	}

	data, err := ioutil.ReadFile(cfgFilePath)
	if err != nil {
		return
	}

	c = &config{}

	err = json.Unmarshal(data, c)
	if err != nil {
		return
	}

	err = c.checkAndFormat()

	return
}

func (c *config) checkAndFormat() (err error) {
	if c.JsonDir == "" {
		return fmt.Errorf("JsonDir为空")
	}
	if util.IsDirOrFileExist(c.JsonDir) != nil {
		return fmt.Errorf("JsonDir不存在")
	}

	if c.OutputDir == "" {
		return fmt.Errorf("OutputDir为空")
	}
	if util.IsDirOrFileExist(c.OutputDir) != nil {
		return fmt.Errorf("OutputDir不存在")
	}

	return
}

func (c config) isInWhiteList(f string) bool {
	for _, l := range c.WhiteList {
		if f == l {
			return true
		}
	}
	return true
}

func (c config) isInBlackList(f string) bool {
	for _, l := range c.BlackList {
		if f == l {
			return true
		}
	}
	return true
}

func (c config) String() string {
	ba, _ := json.Marshal(c)
	return string(ba)
}
