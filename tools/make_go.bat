@set WORK_DIR=%cd%
@set GOPATH=%WORK_DIR%
@set OUTPUT_DIR=%WORK_DIR%\bin
@set VENDOR_DIR=%WORK_DIR%\src\vendor
@set LOG_DIR=%WORK_DIR%\log

@IF not "%3" == "" set OUTPUT_DIR=%3

@IF "%1" == "clean"  call :clean & goto exit

@IF "%1" == "clean-log"  call :clean-log & goto exit

@IF "%1" == "clean-all"  call :clean & call :clean_log & goto exit

@IF "%1" == "glide-up" call :glide-up & goto :exit

@IF "%1" == "zip-vendor" call :zip-vendor & goto :exit

@IF "%1" == "unzip-vendor" call :unzip-vendor & goto :exit

@IF "%1" == "build"  call :build %2 & cd %cd% & goto exit

@IF "%1" == "test"  call :test %2 & cd %cd% & goto exit

@echo unsupported operate [%1][%2]

@goto exit


:clean
rmdir /q /s "%WORK_DIR%\pkg"
del "%OUTPUT_DIR%\*.exe"
del "%OUTPUT_DIR%\debug"
@goto exit


:clean-log
del "%LOG_DIR%\*.log"
@goto exit


:glide-up
@echo glide up begin
cd %WORK_DIR%\src
rmdir /s /q "%VENDOR_DIR%"
::glide --debug up --strip-vendor
glide up --strip-vendor
rmdir /s /q "%VENDOR_DIR%\github.com\coreos\etcd\cmd"
del "%VENDOR_DIR%\gitee.com\lwj8507\nggs\vendor.zip"
del "%VENDOR_DIR%\gitee.com\lwj8507\light-protoactor-go\vendor.zip"
@echo glide up end
@goto :exit


:zip-vendor
@echo zip [vendor] begin
del "%VENDOR_DIR%.zip"
7z a -tzip "%VENDOR_DIR%.zip" "%VENDOR_DIR%"
@echo zip [vendor] end
@goto :exit


:unzip-vendor
@echo unzip [vendor] begin
rmdir /q /s "%VENDOR_DIR%"
7z x -tzip "%VENDOR_DIR%.zip" -y -aos -o"%WORK_DIR%\src"
@echo unzip [vendor] end
@goto :exit


:build
@echo build [%1] begin
rmdir /q /s "%WORK_DIR%\pkg"
go build -o "%OUTPUT_DIR%\%1.exe" %1
@echo build [%1] end
@goto exit

:test
@echo test [%1] begin
go test %1
@echo test [%1]end
@goto exit


:exit
