package main

import (
	"strings"
	"text/template"

	"nggs/util"
)

var gTemplateFuncMap = template.FuncMap{
	"FormatFieldName": FormatFieldName,
}

func FormatFieldName(n string) (nn string) {
	words := strings.Split(n, "/")
	if len(words) > 1 {
		nn = util.FormatFieldName(words[1])
	} else {
		nn = util.FormatFieldName(n)
	}
	return
}
