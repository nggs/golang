import { msg } from './pb';
import * as message from '../../core/network/websocket';
import * as $protobuf from "../../lib/protobuf";

export namespace Pb{{FormatFieldName .Name}} {
    export function Register{{FormatFieldName .Name}}() {
    {{range .Messages}}{{if .ID}}        message.MessageFactory.Instance.Register({{.ID}}, {{.Name}}.Decode, {{.Name}}.Encode){{end}}
    {{end}}
    }
    {{range .Messages}}
    {{if .ID}}export function {{.Name}}_MessageID(): number {
        return {{.ID}}
    }
    {{end}}
	{{if .ID}}
    /**
     * {{.Comment}}
     */
    export class {{.Name}} extends msg.{{.Name}} implements message.IMessage {
    {{if .ID}}        public MessageID(): number {
    		return {{.Name}}_MessageID()
    	}
    {{end}}

    	public static Decode(reader: ($protobuf.Reader | Uint8Array), length?: number): {{.Name}} {
    		let m = msg.{{.Name}}.decode(reader, length)
    		return new {{.Name}}(m)
    	}

    	public static Encode(message: message.IMessage, writer: $protobuf.Writer): $protobuf.Writer {
    		msg.{{.Name}}.encode(<msg.I{{.Name}}>message, writer)
    		return writer
    	}
    }
	{{end}}
{{end}}
}
