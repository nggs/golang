package main

import (
	"fmt"
	"go/format"
	"strings"

	"bytes"

	"github.com/gogo/protobuf/proto"
	plugin "github.com/gogo/protobuf/protoc-gen-gogo/plugin"
)

// filenameSuffix replaces the .pb.go at the end of each filename.
func Generate(req *plugin.CodeGeneratorRequest, p Plugin, filenameSuffix string) *plugin.CodeGeneratorResponse {
	g := New()
	g.Request = req
	if len(g.Request.FileToGenerate) == 0 {
		g.Fail("no files to generate")
	}

	g.CommandLineParameters(g.Request.GetParameter())

	g.WrapTypes()
	g.SetPackageNames()
	g.BuildTypeNameMap()
	g.GeneratePlugin(p)

	for i := 0; i < len(g.Response.File); i++ {
		//g.Response.File[i].Name = proto.String(
		//	strings.Title(*g.Response.File[i].Name),
		//)
		g.Response.File[i].Name = proto.String(
			strings.Replace(*g.Response.File[i].Name, ".pb.go", filenameSuffix, -1),
		)
	}
	//if err := formatSource(g.Response); err != nil {
	//	g.Error(err)
	//}
	return g.Response
}

func formatSource(resp *plugin.CodeGeneratorResponse) error {
	for i := 0; i < len(resp.File); i++ {
		formatted, err := format.Source([]byte(resp.File[i].GetContent()))
		if err != nil {
			return fmt.Errorf("go format error: %v", err)
		}
		content := string(formatted)
		resp.File[i].Content = &content
	}
	return nil
}

func MakeFirstLowerCase(s string) string {
	if len(s) < 2 {
		return strings.ToLower(s)
	}

	bts := []byte(s)

	lc := bytes.ToLower([]byte{bts[0]})
	rest := bts[1:]

	return string(bytes.Join([][]byte{lc, rest}, nil))
}
