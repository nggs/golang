package print

import (
	"fmt"
	"log"
)

var DebugMode bool

func SetDebugMode(debugMode bool) {
	DebugMode = debugMode
}

func Panicf(f string, args ...interface{}) {
	panic(fmt.Sprintf(f, args...))
}

func Errorf(f string, args ...interface{}) {
	log.Printf("[ERROR] "+f+"\n", args...)
}

func Infof(f string, args ...interface{}) {
	fmt.Printf("[INFO] "+f+"\n", args...)
}

func Debugf(f string, args ...interface{}) {
	if DebugMode {
		fmt.Printf("[DEBUG] "+f+"\n", args...)
	}
}
