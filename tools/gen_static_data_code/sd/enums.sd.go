// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！
package sd

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_AffixType] begin

// 词缀类型
type E_AffixType int

const (
	E_AffixType_Null E_AffixType = 0

	// 前缀
	E_AffixType_Prefix E_AffixType = 1
	// 后缀
	E_AffixType_Suffix E_AffixType = 2
)

var E_AffixType_name = map[int]string{
	1: "E_AffixType_Prefix",
	2: "E_AffixType_Suffix",
}

var E_AffixType_value = map[string]int{
	"E_AffixType_Prefix": 1,
	"E_AffixType_Suffix": 2,
}

var E_AffixType_Slice = []int{
	1,
	2,
}

func (x E_AffixType) String() string {
	if name, ok := E_AffixType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_AffixType_Size() int {
	return len(E_AffixType_Slice)
}

func Check_E_AffixType_I(value int) bool {
	if _, ok := E_AffixType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_AffixType(value E_AffixType) bool {
	return Check_E_AffixType_I(int(value))
}

func Each_E_AffixType(f func(E_AffixType) (continued bool)) {
	for _, value := range E_AffixType_Slice {
		if !f(E_AffixType(value)) {
			break
		}
	}
}

func Each_E_AffixType_I(f func(int) (continued bool)) {
	for _, value := range E_AffixType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_AffixType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_AttributeType] begin

// 属性类型
type E_AttributeType int

const (
	E_AttributeType_Null E_AttributeType = 0

	// 力量
	E_AttributeType_Str E_AttributeType = 1
	// 敏捷
	E_AttributeType_Dex E_AttributeType = 2
	// 智慧
	E_AttributeType_Int E_AttributeType = 3
	// 体力
	E_AttributeType_Vit E_AttributeType = 4
)

var E_AttributeType_name = map[int]string{
	1: "E_AttributeType_Str",
	2: "E_AttributeType_Dex",
	3: "E_AttributeType_Int",
	4: "E_AttributeType_Vit",
}

var E_AttributeType_value = map[string]int{
	"E_AttributeType_Str": 1,
	"E_AttributeType_Dex": 2,
	"E_AttributeType_Int": 3,
	"E_AttributeType_Vit": 4,
}

var E_AttributeType_Slice = []int{
	1,
	2,
	3,
	4,
}

func (x E_AttributeType) String() string {
	if name, ok := E_AttributeType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_AttributeType_Size() int {
	return len(E_AttributeType_Slice)
}

func Check_E_AttributeType_I(value int) bool {
	if _, ok := E_AttributeType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_AttributeType(value E_AttributeType) bool {
	return Check_E_AttributeType_I(int(value))
}

func Each_E_AttributeType(f func(E_AttributeType) (continued bool)) {
	for _, value := range E_AttributeType_Slice {
		if !f(E_AttributeType(value)) {
			break
		}
	}
}

func Each_E_AttributeType_I(f func(int) (continued bool)) {
	for _, value := range E_AttributeType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_AttributeType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_EquipType] begin

// 装备类型
type E_EquipType int

const (
	E_EquipType_Null E_EquipType = 0

	// 头盔
	E_EquipType_Helmet E_EquipType = 1
	// 盔甲
	E_EquipType_Armour E_EquipType = 2
	// 手套
	E_EquipType_Glove E_EquipType = 3
	// 腰带
	E_EquipType_Belt E_EquipType = 4
	// 鞋子
	E_EquipType_Boot E_EquipType = 5
	// 项链
	E_EquipType_Amulet E_EquipType = 6
	// 戒指
	E_EquipType_Ring E_EquipType = 7
	// 盾牌
	E_EquipType_Shield E_EquipType = 8
)

var E_EquipType_name = map[int]string{
	1: "E_EquipType_Helmet",
	2: "E_EquipType_Armour",
	3: "E_EquipType_Glove",
	4: "E_EquipType_Belt",
	5: "E_EquipType_Boot",
	6: "E_EquipType_Amulet",
	7: "E_EquipType_Ring",
	8: "E_EquipType_Shield",
}

var E_EquipType_value = map[string]int{
	"E_EquipType_Helmet": 1,
	"E_EquipType_Armour": 2,
	"E_EquipType_Glove":  3,
	"E_EquipType_Belt":   4,
	"E_EquipType_Boot":   5,
	"E_EquipType_Amulet": 6,
	"E_EquipType_Ring":   7,
	"E_EquipType_Shield": 8,
}

var E_EquipType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
}

func (x E_EquipType) String() string {
	if name, ok := E_EquipType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_EquipType_Size() int {
	return len(E_EquipType_Slice)
}

func Check_E_EquipType_I(value int) bool {
	if _, ok := E_EquipType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_EquipType(value E_EquipType) bool {
	return Check_E_EquipType_I(int(value))
}

func Each_E_EquipType(f func(E_EquipType) (continued bool)) {
	for _, value := range E_EquipType_Slice {
		if !f(E_EquipType(value)) {
			break
		}
	}
}

func Each_E_EquipType_I(f func(int) (continued bool)) {
	for _, value := range E_EquipType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_EquipType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_ItemBigType] begin

// 道具大类型
type E_ItemBigType int

const (
	E_ItemBigType_Null E_ItemBigType = 0

	// 货币
	E_ItemBigType_Currency E_ItemBigType = 1
	// 道具
	E_ItemBigType_Item E_ItemBigType = 2
	// 英雄
	E_ItemBigType_Hero E_ItemBigType = 3
	// 装备
	E_ItemBigType_Equip E_ItemBigType = 4
)

var E_ItemBigType_name = map[int]string{
	1: "E_ItemBigType_Currency",
	2: "E_ItemBigType_Item",
	3: "E_ItemBigType_Hero",
	4: "E_ItemBigType_Equip",
}

var E_ItemBigType_value = map[string]int{
	"E_ItemBigType_Currency": 1,
	"E_ItemBigType_Item":     2,
	"E_ItemBigType_Hero":     3,
	"E_ItemBigType_Equip":    4,
}

var E_ItemBigType_Slice = []int{
	1,
	2,
	3,
	4,
}

func (x E_ItemBigType) String() string {
	if name, ok := E_ItemBigType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_ItemBigType_Size() int {
	return len(E_ItemBigType_Slice)
}

func Check_E_ItemBigType_I(value int) bool {
	if _, ok := E_ItemBigType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_ItemBigType(value E_ItemBigType) bool {
	return Check_E_ItemBigType_I(int(value))
}

func Each_E_ItemBigType(f func(E_ItemBigType) (continued bool)) {
	for _, value := range E_ItemBigType_Slice {
		if !f(E_ItemBigType(value)) {
			break
		}
	}
}

func Each_E_ItemBigType_I(f func(int) (continued bool)) {
	for _, value := range E_ItemBigType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_ItemBigType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_ItemType] begin

// 道具类型
type E_ItemType int

const (
	E_ItemType_Null E_ItemType = 0

	// 招募令
	E_ItemType_HeroDrawTickets E_ItemType = 1
	// 时光沙漏
	E_ItemType_Accelerator E_ItemType = 2
	// 装备强化石
	E_ItemType_Iron E_ItemType = 3
	// 消耗品
	E_ItemType_Consumables E_ItemType = 4
	// 门票钥匙
	E_ItemType_Key E_ItemType = 5
	// 礼包
	E_ItemType_GiftPack E_ItemType = 6
	// 武将碎片
	E_ItemType_HeroDebris E_ItemType = 7
)

var E_ItemType_name = map[int]string{
	1: "E_ItemType_HeroDrawTickets",
	2: "E_ItemType_Accelerator",
	3: "E_ItemType_Iron",
	4: "E_ItemType_Consumables",
	5: "E_ItemType_Key",
	6: "E_ItemType_GiftPack",
	7: "E_ItemType_HeroDebris",
}

var E_ItemType_value = map[string]int{
	"E_ItemType_HeroDrawTickets": 1,
	"E_ItemType_Accelerator":     2,
	"E_ItemType_Iron":            3,
	"E_ItemType_Consumables":     4,
	"E_ItemType_Key":             5,
	"E_ItemType_GiftPack":        6,
	"E_ItemType_HeroDebris":      7,
}

var E_ItemType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
}

func (x E_ItemType) String() string {
	if name, ok := E_ItemType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_ItemType_Size() int {
	return len(E_ItemType_Slice)
}

func Check_E_ItemType_I(value int) bool {
	if _, ok := E_ItemType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_ItemType(value E_ItemType) bool {
	return Check_E_ItemType_I(int(value))
}

func Each_E_ItemType(f func(E_ItemType) (continued bool)) {
	for _, value := range E_ItemType_Slice {
		if !f(E_ItemType(value)) {
			break
		}
	}
}

func Each_E_ItemType_I(f func(int) (continued bool)) {
	for _, value := range E_ItemType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_ItemType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_CurrencyType] begin

// 货币类型
type E_CurrencyType int

const (
	E_CurrencyType_Null E_CurrencyType = 0

	// 元宝
	E_CurrencyType_GoldCoin E_CurrencyType = 1
	// 银币
	E_CurrencyType_SilverCoin E_CurrencyType = 2
	// 御将旗
	E_CurrencyType_HeroMasterFlag E_CurrencyType = 3
	// 公会勋章
	E_CurrencyType_GuildPoints E_CurrencyType = 4
	// 将魂
	E_CurrencyType_HeroSoul E_CurrencyType = 5
	// 奇门令
	E_CurrencyType_DaoistMagicToken E_CurrencyType = 6
	// 公会活跃值
	E_CurrencyType_GuildActivityPoints E_CurrencyType = 7
	// 日常活动活跃值
	E_CurrencyType_ActivityPoints E_CurrencyType = 8
	// 好友点
	E_CurrencyType_CompanionPoints E_CurrencyType = 9
	// 美酒
	E_CurrencyType_GoodWine E_CurrencyType = 10
	// VIP经验
	E_CurrencyType_VipExp E_CurrencyType = 51
	// 角色经验
	E_CurrencyType_RoleExp E_CurrencyType = 52
	// 武将经验
	E_CurrencyType_HeroExp E_CurrencyType = 53
	// 公会经验
	E_CurrencyType_GuildExp E_CurrencyType = 54
	// 讨伐功勋
	E_CurrencyType_AssaultPoints E_CurrencyType = 55
	// 装备经验
	E_CurrencyType_EquipmentExp E_CurrencyType = 56
	// 竞技场积分
	E_CurrencyType_ArenaPoints E_CurrencyType = 57
	// 阵营积分
	E_CurrencyType_FactionPoints E_CurrencyType = 58
)

var E_CurrencyType_name = map[int]string{
	1:  "E_CurrencyType_GoldCoin",
	2:  "E_CurrencyType_SilverCoin",
	3:  "E_CurrencyType_HeroMasterFlag",
	4:  "E_CurrencyType_GuildPoints",
	5:  "E_CurrencyType_HeroSoul",
	6:  "E_CurrencyType_DaoistMagicToken",
	7:  "E_CurrencyType_GuildActivityPoints",
	8:  "E_CurrencyType_ActivityPoints",
	9:  "E_CurrencyType_CompanionPoints",
	10: "E_CurrencyType_GoodWine",
	51: "E_CurrencyType_VipExp",
	52: "E_CurrencyType_RoleExp",
	53: "E_CurrencyType_HeroExp",
	54: "E_CurrencyType_GuildExp",
	55: "E_CurrencyType_AssaultPoints",
	56: "E_CurrencyType_EquipmentExp",
	57: "E_CurrencyType_ArenaPoints",
	58: "E_CurrencyType_FactionPoints",
}

var E_CurrencyType_value = map[string]int{
	"E_CurrencyType_GoldCoin":            1,
	"E_CurrencyType_SilverCoin":          2,
	"E_CurrencyType_HeroMasterFlag":      3,
	"E_CurrencyType_GuildPoints":         4,
	"E_CurrencyType_HeroSoul":            5,
	"E_CurrencyType_DaoistMagicToken":    6,
	"E_CurrencyType_GuildActivityPoints": 7,
	"E_CurrencyType_ActivityPoints":      8,
	"E_CurrencyType_CompanionPoints":     9,
	"E_CurrencyType_GoodWine":            10,
	"E_CurrencyType_VipExp":              51,
	"E_CurrencyType_RoleExp":             52,
	"E_CurrencyType_HeroExp":             53,
	"E_CurrencyType_GuildExp":            54,
	"E_CurrencyType_AssaultPoints":       55,
	"E_CurrencyType_EquipmentExp":        56,
	"E_CurrencyType_ArenaPoints":         57,
	"E_CurrencyType_FactionPoints":       58,
}

var E_CurrencyType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
	51,
	52,
	53,
	54,
	55,
	56,
	57,
	58,
}

func (x E_CurrencyType) String() string {
	if name, ok := E_CurrencyType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_CurrencyType_Size() int {
	return len(E_CurrencyType_Slice)
}

func Check_E_CurrencyType_I(value int) bool {
	if _, ok := E_CurrencyType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_CurrencyType(value E_CurrencyType) bool {
	return Check_E_CurrencyType_I(int(value))
}

func Each_E_CurrencyType(f func(E_CurrencyType) (continued bool)) {
	for _, value := range E_CurrencyType_Slice {
		if !f(E_CurrencyType(value)) {
			break
		}
	}
}

func Each_E_CurrencyType_I(f func(int) (continued bool)) {
	for _, value := range E_CurrencyType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_CurrencyType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_AcceleratorType] begin

// 急行军令掉落类型
type E_AcceleratorType int

const (
	E_AcceleratorType_Null E_AcceleratorType = 0

	// 急行军令掉落银币
	E_AcceleratorType_SilverCoin E_AcceleratorType = 1
	// 急行军令掉落;武将经验
	E_AcceleratorType_HeroExp E_AcceleratorType = 2
	// 急行军令掉落武将突破丹
	E_AcceleratorType_LimitPassPill E_AcceleratorType = 3
	// 急行军令三者都掉
	E_AcceleratorType_Total E_AcceleratorType = 4
)

var E_AcceleratorType_name = map[int]string{
	1: "E_AcceleratorType_SilverCoin",
	2: "E_AcceleratorType_HeroExp",
	3: "E_AcceleratorType_LimitPassPill",
	4: "E_AcceleratorType_Total",
}

var E_AcceleratorType_value = map[string]int{
	"E_AcceleratorType_SilverCoin":    1,
	"E_AcceleratorType_HeroExp":       2,
	"E_AcceleratorType_LimitPassPill": 3,
	"E_AcceleratorType_Total":         4,
}

var E_AcceleratorType_Slice = []int{
	1,
	2,
	3,
	4,
}

func (x E_AcceleratorType) String() string {
	if name, ok := E_AcceleratorType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_AcceleratorType_Size() int {
	return len(E_AcceleratorType_Slice)
}

func Check_E_AcceleratorType_I(value int) bool {
	if _, ok := E_AcceleratorType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_AcceleratorType(value E_AcceleratorType) bool {
	return Check_E_AcceleratorType_I(int(value))
}

func Each_E_AcceleratorType(f func(E_AcceleratorType) (continued bool)) {
	for _, value := range E_AcceleratorType_Slice {
		if !f(E_AcceleratorType(value)) {
			break
		}
	}
}

func Each_E_AcceleratorType_I(f func(int) (continued bool)) {
	for _, value := range E_AcceleratorType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_AcceleratorType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_Quality] begin

// 品质类型
type E_Quality int

const (
	E_Quality_Null E_Quality = 0

	// 灰色
	E_Quality_Gray E_Quality = 1
	// 绿色
	E_Quality_Green E_Quality = 2
	// 蓝色
	E_Quality_Blue E_Quality = 3
	// 蓝色+
	E_Quality_BluePlus E_Quality = 4
	// 紫色
	E_Quality_Purple E_Quality = 5
	// 紫色+
	E_Quality_PurplePlus E_Quality = 6
	// 橙色
	E_Quality_Gold E_Quality = 7
	// 橙色+
	E_Quality_GoldPlus E_Quality = 8
	// 红色
	E_Quality_Red E_Quality = 9
	// 红色+
	E_Quality_RedPlus E_Quality = 10
	// 白金
	E_Quality_Platinum E_Quality = 11
	// 白金+
	E_Quality_PlatinumPlus E_Quality = 12
)

var E_Quality_name = map[int]string{
	1:  "E_Quality_Gray",
	2:  "E_Quality_Green",
	3:  "E_Quality_Blue",
	4:  "E_Quality_BluePlus",
	5:  "E_Quality_Purple",
	6:  "E_Quality_PurplePlus",
	7:  "E_Quality_Gold",
	8:  "E_Quality_GoldPlus",
	9:  "E_Quality_Red",
	10: "E_Quality_RedPlus",
	11: "E_Quality_Platinum",
	12: "E_Quality_PlatinumPlus",
}

var E_Quality_value = map[string]int{
	"E_Quality_Gray":         1,
	"E_Quality_Green":        2,
	"E_Quality_Blue":         3,
	"E_Quality_BluePlus":     4,
	"E_Quality_Purple":       5,
	"E_Quality_PurplePlus":   6,
	"E_Quality_Gold":         7,
	"E_Quality_GoldPlus":     8,
	"E_Quality_Red":          9,
	"E_Quality_RedPlus":      10,
	"E_Quality_Platinum":     11,
	"E_Quality_PlatinumPlus": 12,
}

var E_Quality_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
	11,
	12,
}

func (x E_Quality) String() string {
	if name, ok := E_Quality_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_Quality_Size() int {
	return len(E_Quality_Slice)
}

func Check_E_Quality_I(value int) bool {
	if _, ok := E_Quality_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_Quality(value E_Quality) bool {
	return Check_E_Quality_I(int(value))
}

func Each_E_Quality(f func(E_Quality) (continued bool)) {
	for _, value := range E_Quality_Slice {
		if !f(E_Quality(value)) {
			break
		}
	}
}

func Each_E_Quality_I(f func(int) (continued bool)) {
	for _, value := range E_Quality_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_Quality] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_Faction] begin

// 种族类型
type E_Faction int

const (
	E_Faction_Null E_Faction = 0

	// 魏国
	E_Faction_Wei E_Faction = 1
	// 蜀国
	E_Faction_Shu E_Faction = 2
	// 吴国
	E_Faction_Wu E_Faction = 3
	// 群雄
	E_Faction_Qun E_Faction = 4
	// 仙
	E_Faction_Xian E_Faction = 5
	// 魔
	E_Faction_Mo E_Faction = 6
	// 无
	E_Faction_Nothing E_Faction = 7
)

var E_Faction_name = map[int]string{
	1: "E_Faction_Wei",
	2: "E_Faction_Shu",
	3: "E_Faction_Wu",
	4: "E_Faction_Qun",
	5: "E_Faction_Xian",
	6: "E_Faction_Mo",
	7: "E_Faction_Nothing",
}

var E_Faction_value = map[string]int{
	"E_Faction_Wei":     1,
	"E_Faction_Shu":     2,
	"E_Faction_Wu":      3,
	"E_Faction_Qun":     4,
	"E_Faction_Xian":    5,
	"E_Faction_Mo":      6,
	"E_Faction_Nothing": 7,
}

var E_Faction_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
}

func (x E_Faction) String() string {
	if name, ok := E_Faction_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_Faction_Size() int {
	return len(E_Faction_Slice)
}

func Check_E_Faction_I(value int) bool {
	if _, ok := E_Faction_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_Faction(value E_Faction) bool {
	return Check_E_Faction_I(int(value))
}

func Each_E_Faction(f func(E_Faction) (continued bool)) {
	for _, value := range E_Faction_Slice {
		if !f(E_Faction(value)) {
			break
		}
	}
}

func Each_E_Faction_I(f func(int) (continued bool)) {
	for _, value := range E_Faction_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_Faction] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_Class] begin

// 职业类型
type E_Class int

const (
	E_Class_Null E_Class = 0

	// 智力
	E_Class_Intelligence E_Class = 1
	// 力量
	E_Class_Strength E_Class = 2
	// 敏捷
	E_Class_Agile E_Class = 3
)

var E_Class_name = map[int]string{
	1: "E_Class_Intelligence",
	2: "E_Class_Strength",
	3: "E_Class_Agile",
}

var E_Class_value = map[string]int{
	"E_Class_Intelligence": 1,
	"E_Class_Strength":     2,
	"E_Class_Agile":        3,
}

var E_Class_Slice = []int{
	1,
	2,
	3,
}

func (x E_Class) String() string {
	if name, ok := E_Class_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_Class_Size() int {
	return len(E_Class_Slice)
}

func Check_E_Class_I(value int) bool {
	if _, ok := E_Class_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_Class(value E_Class) bool {
	return Check_E_Class_I(int(value))
}

func Each_E_Class(f func(E_Class) (continued bool)) {
	for _, value := range E_Class_Slice {
		if !f(E_Class(value)) {
			break
		}
	}
}

func Each_E_Class_I(f func(int) (continued bool)) {
	for _, value := range E_Class_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_Class] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_EquipmentType] begin

// 装备类型
type E_EquipmentType int

const (
	E_EquipmentType_Null E_EquipmentType = 0

	// 法仗
	E_EquipmentType_Staff E_EquipmentType = 1
	// 布甲
	E_EquipmentType_Cloth E_EquipmentType = 2
	// 重武器
	E_EquipmentType_HeavyWeapon E_EquipmentType = 3
	// 板甲
	E_EquipmentType_Plate E_EquipmentType = 4
	// 轻武器
	E_EquipmentType_LightWeapons E_EquipmentType = 5
	// 皮甲
	E_EquipmentType_LeatherArmor E_EquipmentType = 6
)

var E_EquipmentType_name = map[int]string{
	1: "E_EquipmentType_Staff",
	2: "E_EquipmentType_Cloth",
	3: "E_EquipmentType_HeavyWeapon",
	4: "E_EquipmentType_Plate",
	5: "E_EquipmentType_LightWeapons",
	6: "E_EquipmentType_LeatherArmor",
}

var E_EquipmentType_value = map[string]int{
	"E_EquipmentType_Staff":        1,
	"E_EquipmentType_Cloth":        2,
	"E_EquipmentType_HeavyWeapon":  3,
	"E_EquipmentType_Plate":        4,
	"E_EquipmentType_LightWeapons": 5,
	"E_EquipmentType_LeatherArmor": 6,
}

var E_EquipmentType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
}

func (x E_EquipmentType) String() string {
	if name, ok := E_EquipmentType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_EquipmentType_Size() int {
	return len(E_EquipmentType_Slice)
}

func Check_E_EquipmentType_I(value int) bool {
	if _, ok := E_EquipmentType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_EquipmentType(value E_EquipmentType) bool {
	return Check_E_EquipmentType_I(int(value))
}

func Each_E_EquipmentType(f func(E_EquipmentType) (continued bool)) {
	for _, value := range E_EquipmentType_Slice {
		if !f(E_EquipmentType(value)) {
			break
		}
	}
}

func Each_E_EquipmentType_I(f func(int) (continued bool)) {
	for _, value := range E_EquipmentType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_EquipmentType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_EquipmentPos] begin

// 装备部位类型
type E_EquipmentPos int

const (
	E_EquipmentPos_Null E_EquipmentPos = 0

	// 武器
	E_EquipmentPos_Arms E_EquipmentPos = 1
	// 帽子
	E_EquipmentPos_Hat E_EquipmentPos = 2
	// 衣服
	E_EquipmentPos_Cloth E_EquipmentPos = 3
	// 鞋子
	E_EquipmentPos_Shoe E_EquipmentPos = 4
)

var E_EquipmentPos_name = map[int]string{
	1: "E_EquipmentPos_Arms",
	2: "E_EquipmentPos_Hat",
	3: "E_EquipmentPos_Cloth",
	4: "E_EquipmentPos_Shoe",
}

var E_EquipmentPos_value = map[string]int{
	"E_EquipmentPos_Arms":  1,
	"E_EquipmentPos_Hat":   2,
	"E_EquipmentPos_Cloth": 3,
	"E_EquipmentPos_Shoe":  4,
}

var E_EquipmentPos_Slice = []int{
	1,
	2,
	3,
	4,
}

func (x E_EquipmentPos) String() string {
	if name, ok := E_EquipmentPos_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_EquipmentPos_Size() int {
	return len(E_EquipmentPos_Slice)
}

func Check_E_EquipmentPos_I(value int) bool {
	if _, ok := E_EquipmentPos_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_EquipmentPos(value E_EquipmentPos) bool {
	return Check_E_EquipmentPos_I(int(value))
}

func Each_E_EquipmentPos(f func(E_EquipmentPos) (continued bool)) {
	for _, value := range E_EquipmentPos_Slice {
		if !f(E_EquipmentPos(value)) {
			break
		}
	}
}

func Each_E_EquipmentPos_I(f func(int) (continued bool)) {
	for _, value := range E_EquipmentPos_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_EquipmentPos] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_LimitRefreshType] begin

// 限制类型
type E_LimitRefreshType int

const (
	E_LimitRefreshType_Null E_LimitRefreshType = 0

	// 永远不刷新，用完拉倒
	E_LimitRefreshType_Never E_LimitRefreshType = 1
	// 每隔一段时间刷新，次数用完才开始走CD
	E_LimitRefreshType_Dur E_LimitRefreshType = 2
	// 每日固定时间点刷新
	E_LimitRefreshType_Day E_LimitRefreshType = 3
	// 每周固定时间点刷新
	E_LimitRefreshType_Week E_LimitRefreshType = 4
)

var E_LimitRefreshType_name = map[int]string{
	1: "E_LimitRefreshType_Never",
	2: "E_LimitRefreshType_Dur",
	3: "E_LimitRefreshType_Day",
	4: "E_LimitRefreshType_Week",
}

var E_LimitRefreshType_value = map[string]int{
	"E_LimitRefreshType_Never": 1,
	"E_LimitRefreshType_Dur":   2,
	"E_LimitRefreshType_Day":   3,
	"E_LimitRefreshType_Week":  4,
}

var E_LimitRefreshType_Slice = []int{
	1,
	2,
	3,
	4,
}

func (x E_LimitRefreshType) String() string {
	if name, ok := E_LimitRefreshType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_LimitRefreshType_Size() int {
	return len(E_LimitRefreshType_Slice)
}

func Check_E_LimitRefreshType_I(value int) bool {
	if _, ok := E_LimitRefreshType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_LimitRefreshType(value E_LimitRefreshType) bool {
	return Check_E_LimitRefreshType_I(int(value))
}

func Each_E_LimitRefreshType(f func(E_LimitRefreshType) (continued bool)) {
	for _, value := range E_LimitRefreshType_Slice {
		if !f(E_LimitRefreshType(value)) {
			break
		}
	}
}

func Each_E_LimitRefreshType_I(f func(int) (continued bool)) {
	for _, value := range E_LimitRefreshType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_LimitRefreshType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_FunctionType] begin

// 功能类型
type E_FunctionType int

const (
	E_FunctionType_Null E_FunctionType = 0

	// 关卡
	E_FunctionType_Stage E_FunctionType = 100
	// 挂机
	E_FunctionType_Idle E_FunctionType = 200
	// 英雄系统（英雄背包、英雄图鉴、英雄升级、英雄技能展示）
	E_FunctionType_Hero E_FunctionType = 300
	// 英雄背包
	E_FunctionType_HeroBag E_FunctionType = 400
	// 英雄图鉴
	E_FunctionType_HeroIllustration E_FunctionType = 500
	// 英雄升级
	E_FunctionType_HeroLevelUp E_FunctionType = 600
	// 主界面逻辑框架
	E_FunctionType_MainUILogic E_FunctionType = 700
	// 英雄装备
	E_FunctionType_Equip E_FunctionType = 800
	// 装备强化
	E_FunctionType_EquipStrengthen E_FunctionType = 900
	// 背包系统
	E_FunctionType_Bag E_FunctionType = 1000
	// 月桂酒馆
	E_FunctionType_EmployHero E_FunctionType = 1100
	// 英雄系统（英雄进化--大圣堂）
	E_FunctionType_HeroEvolution E_FunctionType = 1200
	// 英雄系统（英雄遣散--马车）
	E_FunctionType_HeroReborn E_FunctionType = 1301
	// 英雄系统（英雄重置--马车）
	E_FunctionType_HeroDismiss E_FunctionType = 1302
	// 英雄系统（英雄等级共享--共鸣水晶）
	E_FunctionType_HeroShare E_FunctionType = 1400
	// 战斗系统（布阵页、战斗模块、结算面板）
	E_FunctionType_Battle E_FunctionType = 1500
	// 英雄技能
	E_FunctionType_Skill E_FunctionType = 1600
	// 竞技场系统
	E_FunctionType_Arena E_FunctionType = 1700
	// 悬赏栏个人系统
	E_FunctionType_BountyOneself E_FunctionType = 1801
	// 悬赏栏团队系统
	E_FunctionType_BountyTeam E_FunctionType = 1802
	// 商店系统
	E_FunctionType_Shop E_FunctionType = 1900
	// 异界迷宫系统
	E_FunctionType_Maze E_FunctionType = 2000
	// 图书馆系统
	E_FunctionType_Library E_FunctionType = 2100
	// 邮件系统
	E_FunctionType_Mall E_FunctionType = 2200
	// 王座之塔系统
	E_FunctionType_Tower E_FunctionType = 2300
	// 任务系统
	E_FunctionType_Task E_FunctionType = 2400
	// 聊天系统
	E_FunctionType_Chat E_FunctionType = 2500
	// 排行榜系统（凯旋丰碑）
	E_FunctionType_Rank E_FunctionType = 2600
	// 公会（主面板、大厅、狩猎）
	E_FunctionType_Guild E_FunctionType = 2700
	// 大厅
	E_FunctionType_Hall E_FunctionType = 2800
	// 狩猎
	E_FunctionType_Hunt E_FunctionType = 2900
	// 好友系统
	E_FunctionType_Friend E_FunctionType = 3000
	// 时光之巅
	E_FunctionType_TimeStage E_FunctionType = 3100
	// 设置面板（音量调节、角色设置、服务器选择）
	E_FunctionType_Setting E_FunctionType = 3200
	// 占星师
	E_FunctionType_Stargazer E_FunctionType = 3300
	// 充值系统
	E_FunctionType_Charge E_FunctionType = 3400
	// VIP系统
	E_FunctionType_Vip E_FunctionType = 3500
	// 礼包系统（月卡、成长礼包、礼包、首充礼包、限时礼包）
	E_FunctionType_GiftPack E_FunctionType = 4000
	// 活动模块
	E_FunctionType_Activity E_FunctionType = 5000
	// 升级到101级
	E_FunctionType_Level101 E_FunctionType = 6000
)

var E_FunctionType_name = map[int]string{
	100:  "E_FunctionType_Stage",
	200:  "E_FunctionType_Idle",
	300:  "E_FunctionType_Hero",
	400:  "E_FunctionType_HeroBag",
	500:  "E_FunctionType_HeroIllustration",
	600:  "E_FunctionType_HeroLevelUp",
	700:  "E_FunctionType_MainUILogic",
	800:  "E_FunctionType_Equip",
	900:  "E_FunctionType_EquipStrengthen",
	1000: "E_FunctionType_Bag",
	1100: "E_FunctionType_EmployHero",
	1200: "E_FunctionType_HeroEvolution",
	1301: "E_FunctionType_HeroReborn",
	1302: "E_FunctionType_HeroDismiss",
	1400: "E_FunctionType_HeroShare",
	1500: "E_FunctionType_Battle",
	1600: "E_FunctionType_Skill",
	1700: "E_FunctionType_Arena",
	1801: "E_FunctionType_BountyOneself",
	1802: "E_FunctionType_BountyTeam",
	1900: "E_FunctionType_Shop",
	2000: "E_FunctionType_Maze",
	2100: "E_FunctionType_Library",
	2200: "E_FunctionType_Mall",
	2300: "E_FunctionType_Tower",
	2400: "E_FunctionType_Task",
	2500: "E_FunctionType_Chat",
	2600: "E_FunctionType_Rank",
	2700: "E_FunctionType_Guild",
	2800: "E_FunctionType_Hall",
	2900: "E_FunctionType_Hunt",
	3000: "E_FunctionType_Friend",
	3100: "E_FunctionType_TimeStage",
	3200: "E_FunctionType_Setting",
	3300: "E_FunctionType_Stargazer",
	3400: "E_FunctionType_Charge",
	3500: "E_FunctionType_Vip",
	4000: "E_FunctionType_GiftPack",
	5000: "E_FunctionType_Activity",
	6000: "E_FunctionType_Level101",
}

var E_FunctionType_value = map[string]int{
	"E_FunctionType_Stage":            100,
	"E_FunctionType_Idle":             200,
	"E_FunctionType_Hero":             300,
	"E_FunctionType_HeroBag":          400,
	"E_FunctionType_HeroIllustration": 500,
	"E_FunctionType_HeroLevelUp":      600,
	"E_FunctionType_MainUILogic":      700,
	"E_FunctionType_Equip":            800,
	"E_FunctionType_EquipStrengthen":  900,
	"E_FunctionType_Bag":              1000,
	"E_FunctionType_EmployHero":       1100,
	"E_FunctionType_HeroEvolution":    1200,
	"E_FunctionType_HeroReborn":       1301,
	"E_FunctionType_HeroDismiss":      1302,
	"E_FunctionType_HeroShare":        1400,
	"E_FunctionType_Battle":           1500,
	"E_FunctionType_Skill":            1600,
	"E_FunctionType_Arena":            1700,
	"E_FunctionType_BountyOneself":    1801,
	"E_FunctionType_BountyTeam":       1802,
	"E_FunctionType_Shop":             1900,
	"E_FunctionType_Maze":             2000,
	"E_FunctionType_Library":          2100,
	"E_FunctionType_Mall":             2200,
	"E_FunctionType_Tower":            2300,
	"E_FunctionType_Task":             2400,
	"E_FunctionType_Chat":             2500,
	"E_FunctionType_Rank":             2600,
	"E_FunctionType_Guild":            2700,
	"E_FunctionType_Hall":             2800,
	"E_FunctionType_Hunt":             2900,
	"E_FunctionType_Friend":           3000,
	"E_FunctionType_TimeStage":        3100,
	"E_FunctionType_Setting":          3200,
	"E_FunctionType_Stargazer":        3300,
	"E_FunctionType_Charge":           3400,
	"E_FunctionType_Vip":              3500,
	"E_FunctionType_GiftPack":         4000,
	"E_FunctionType_Activity":         5000,
	"E_FunctionType_Level101":         6000,
}

var E_FunctionType_Slice = []int{
	100,
	200,
	300,
	400,
	500,
	600,
	700,
	800,
	900,
	1000,
	1100,
	1200,
	1301,
	1302,
	1400,
	1500,
	1600,
	1700,
	1801,
	1802,
	1900,
	2000,
	2100,
	2200,
	2300,
	2400,
	2500,
	2600,
	2700,
	2800,
	2900,
	3000,
	3100,
	3200,
	3300,
	3400,
	3500,
	4000,
	5000,
	6000,
}

func (x E_FunctionType) String() string {
	if name, ok := E_FunctionType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_FunctionType_Size() int {
	return len(E_FunctionType_Slice)
}

func Check_E_FunctionType_I(value int) bool {
	if _, ok := E_FunctionType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_FunctionType(value E_FunctionType) bool {
	return Check_E_FunctionType_I(int(value))
}

func Each_E_FunctionType(f func(E_FunctionType) (continued bool)) {
	for _, value := range E_FunctionType_Slice {
		if !f(E_FunctionType(value)) {
			break
		}
	}
}

func Each_E_FunctionType_I(f func(int) (continued bool)) {
	for _, value := range E_FunctionType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_FunctionType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加枚举扩展代码
//<Extend>//</Extend>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
