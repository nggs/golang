// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！
package sd

import (
	"log"
	"path/filepath"
)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var (
	affixMgr   = newAffixManager()
	contentMgr = newContentManager()
	globalMgr  = newGlobalManager()
	monsterMgr = newMonsterManager()
	rewardMgr  = newRewardManager()
)

var (
	nilAffix   Affix
	nilContent Content
	nilGlobal  Global
	nilMonster Monster
	nilReward  Reward
)

func LoadAll(jsonDir string) (success bool) {
	absJsonDir, err := filepath.Abs(jsonDir)
	if err != nil {
		log.Println(err)
		return false
	}

	success = true

	success = affixMgr.load(filepath.Join(absJsonDir, "affix.json")) && success
	success = contentMgr.load(filepath.Join(absJsonDir, "content.json")) && success
	success = globalMgr.load(filepath.Join(absJsonDir, "global.json")) && success
	success = monsterMgr.load(filepath.Join(absJsonDir, "monster.json")) && success
	success = rewardMgr.load(filepath.Join(absJsonDir, "reward.json")) && success

	return
}

func AfterLoadAll(jsonDir string) (success bool) {
	absJsonDir, err := filepath.Abs(jsonDir)
	if err != nil {
		log.Println(err)
		return false
	}

	success = true

	success = affixMgr.afterLoadAll(filepath.Join(absJsonDir, "affix.json")) && success
	success = contentMgr.afterLoadAll(filepath.Join(absJsonDir, "content.json")) && success
	success = globalMgr.afterLoadAll(filepath.Join(absJsonDir, "global.json")) && success
	success = monsterMgr.afterLoadAll(filepath.Join(absJsonDir, "monster.json")) && success
	success = rewardMgr.afterLoadAll(filepath.Join(absJsonDir, "reward.json")) && success

	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
