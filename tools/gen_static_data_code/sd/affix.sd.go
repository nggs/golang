// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 词缀表
type Affix struct {
	tYpE      E_AffixType     // 词缀类别 test
	id        int             // 词缀ID
	name      string          // 词缀名称
	rare      bool            // 稀有
	level     int             // 词缀等级
	level_req int             // 等级需求
	frequency int             // 词缀频率
	group     int             // 词缀组
	mod1code  string          // 词缀1属性
	mod1param int             // 词缀1参数
	mod1min   int             // 词缀1最小值参数
	mod1max   int             // 词缀1最大值参数
	mod2code  string          // 词缀2属性
	mod2param int             // 词缀2参数
	mod2min   int             // 词缀2最小值参数
	mod2max   int             // 词缀2最大值参数
	is        []int           // 整型数组
	fs        []float64       // 浮点数组
	bs        []bool          // 布尔数组
	ss        []string        // 字符串数组
	dur       time.Duration   // 时间段
	durs      []time.Duration // 时间段数组
	enums     []E_EquipType   // 枚举数组
	itype     []E_EquipType   // 适用装备的切片
	etype     []time.Duration // 不适用装备的切片

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewAffix() *Affix {
	sd := &Affix{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 词缀类别 test
func (sd Affix) Type() E_AffixType {
	return sd.tYpE
}

// 词缀ID
func (sd Affix) ID() int {
	return sd.id
}

// 词缀名称
func (sd Affix) Name() string {
	return sd.name
}

// 稀有
func (sd Affix) Rare() bool {
	return sd.rare
}

// 词缀等级
func (sd Affix) Level() int {
	return sd.level
}

// 等级需求
func (sd Affix) LevelReq() int {
	return sd.level_req
}

// 词缀频率
func (sd Affix) Frequency() int {
	return sd.frequency
}

// 词缀组
func (sd Affix) Group() int {
	return sd.group
}

// 词缀1属性
func (sd Affix) Mod1code() string {
	return sd.mod1code
}

// 词缀1参数
func (sd Affix) Mod1param() int {
	return sd.mod1param
}

// 词缀1最小值参数
func (sd Affix) Mod1min() int {
	return sd.mod1min
}

// 词缀1最大值参数
func (sd Affix) Mod1max() int {
	return sd.mod1max
}

// 词缀2属性
func (sd Affix) Mod2code() string {
	return sd.mod2code
}

// 词缀2参数
func (sd Affix) Mod2param() int {
	return sd.mod2param
}

// 词缀2最小值参数
func (sd Affix) Mod2min() int {
	return sd.mod2min
}

// 词缀2最大值参数
func (sd Affix) Mod2max() int {
	return sd.mod2max
}

// 整型数组
func (sd Affix) Is() []int {
	return sd.is
}

// 浮点数组
func (sd Affix) Fs() []float64 {
	return sd.fs
}

// 布尔数组
func (sd Affix) Bs() []bool {
	return sd.bs
}

// 字符串数组
func (sd Affix) Ss() []string {
	return sd.ss
}

// 时间段
func (sd Affix) Dur() time.Duration {
	return sd.dur
}

// 时间段数组
func (sd Affix) Durs() []time.Duration {
	return sd.durs
}

// 枚举数组
func (sd Affix) Enums() []E_EquipType {
	return sd.enums
}

// 适用装备的切片
func (sd Affix) Itype() []E_EquipType {
	return sd.itype
}

// 不适用装备的切片
func (sd Affix) Etype() []time.Duration {
	return sd.etype
}

func (sd Affix) Clone() *Affix {
	n := NewAffix()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 词缀表全局属性
type AffixGlobal struct {
	i   int             // 整型
	str string          // 字符串
	f64 float64         // 浮点
	dur time.Duration   // 时间段
	b   bool            // 布尔
	is  []int           // 整型数组
	ds  []time.Duration // 时间段数组
	fs  []float64       // 浮点数组
	ss  []string        // 字符串数组
	bs  []bool          // 布尔数组
	e   E_ItemBigType   // 枚举
	es  []E_ItemBigType // 枚举数组

}

// 整型
func (g AffixGlobal) I() int {
	return g.i
}

// 字符串
func (g AffixGlobal) Str() string {
	return g.str
}

// 浮点
func (g AffixGlobal) F64() float64 {
	return g.f64
}

// 时间段
func (g AffixGlobal) Dur() time.Duration {
	return g.dur
}

// 布尔
func (g AffixGlobal) B() bool {
	return g.b
}

// 整型数组
func (g AffixGlobal) Is() []int {
	return g.is
}

// 时间段数组
func (g AffixGlobal) Ds() []time.Duration {
	return g.ds
}

// 浮点数组
func (g AffixGlobal) Fs() []float64 {
	return g.fs
}

// 字符串数组
func (g AffixGlobal) Ss() []string {
	return g.ss
}

// 布尔数组
func (g AffixGlobal) Bs() []bool {
	return g.bs
}

// 枚举
func (g AffixGlobal) E() E_ItemBigType {
	return g.e
}

// 枚举数组
func (g AffixGlobal) Es() []E_ItemBigType {
	return g.es
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type AffixManager struct {
	Datas  []*Affix
	Global AffixGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*Affix // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	byType map[E_AffixType][]*Affix // NormalIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newAffixManager() *AffixManager {
	mgr := &AffixManager{
		Datas:  []*Affix{},
		byID:   map[int]*Affix{},
		byType: map[E_AffixType][]*Affix{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *AffixManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

		mgr.byType[d.tYpE] = append(mgr.byType[d.tYpE], d)

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func AffixSize() int {
	return affixMgr.size
}

func (mgr AffixManager) check(path string, row int, sd *Affix) error {
	if _, ok := affixMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id重复", path, row)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *AffixManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *AffixManager) each(f func(sd *Affix) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *AffixManager) findIf(f func(sd *Affix) (find bool)) *Affix {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachAffix(f func(sd Affix) (continued bool)) {
	for _, sd := range affixMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindAffixIf(f func(sd Affix) bool) (Affix, bool) {
	for _, sd := range affixMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilAffix, false
}

func GetAffixByID(id int) (Affix, bool) {
	temp, ok := affixMgr.byID[id]
	if !ok {
		return nilAffix, false
	}
	return *temp, true
}

func GetAffixByType(tYpE E_AffixType) (vs []Affix, ok bool) {
	var ds []*Affix
	ds, ok = affixMgr.byType[tYpE]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachAffixByType(tYpE E_AffixType, fn func(d Affix) (continued bool)) bool {
	ds, ok := affixMgr.byType[tYpE]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetAffixGlobal() AffixGlobal {
	return affixMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
