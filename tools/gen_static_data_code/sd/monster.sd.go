// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 野怪表
type Monster struct {
	id           int     // 怪物机器人ID
	heroId       int     // 英雄ID，关联heroBasic表 对应的品质、阵营、职业、描述、技能分别读取heroBasic表或heroGroup表
	level        int     // 野怪等级
	hp           int     // 生命，野怪的属性读取本表，不读英雄表
	atk          int     // 攻击
	def          int     // 防御
	crit         float64 // 暴击
	hit          int     // 命中
	miss         int     // 闪避
	speed        int     // 急速
	recover      int     // 每秒恢复
	physicReduce float64 // 物理减伤率
	magicReduce  float64 // 魔法减伤率
	lifeLeech    int     // 吸血等级
	enlarge      float64 // 模型缩放

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewMonster() *Monster {
	sd := &Monster{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 怪物机器人ID
func (sd Monster) ID() int {
	return sd.id
}

// 英雄ID，关联heroBasic表 对应的品质、阵营、职业、描述、技能分别读取heroBasic表或heroGroup表
func (sd Monster) HeroID() int {
	return sd.heroId
}

// 野怪等级
func (sd Monster) Level() int {
	return sd.level
}

// 生命，野怪的属性读取本表，不读英雄表
func (sd Monster) Hp() int {
	return sd.hp
}

// 攻击
func (sd Monster) Atk() int {
	return sd.atk
}

// 防御
func (sd Monster) Def() int {
	return sd.def
}

// 暴击
func (sd Monster) Crit() float64 {
	return sd.crit
}

// 命中
func (sd Monster) Hit() int {
	return sd.hit
}

// 闪避
func (sd Monster) Miss() int {
	return sd.miss
}

// 急速
func (sd Monster) Speed() int {
	return sd.speed
}

// 每秒恢复
func (sd Monster) Recover() int {
	return sd.recover
}

// 物理减伤率
func (sd Monster) PhysicReduce() float64 {
	return sd.physicReduce
}

// 魔法减伤率
func (sd Monster) MagicReduce() float64 {
	return sd.magicReduce
}

// 吸血等级
func (sd Monster) LifeLeech() int {
	return sd.lifeLeech
}

// 模型缩放
func (sd Monster) Enlarge() float64 {
	return sd.enlarge
}

func (sd Monster) Clone() *Monster {
	n := NewMonster()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type MonsterManager struct {
	Datas []*Monster

	size int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*Monster // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newMonsterManager() *MonsterManager {
	mgr := &MonsterManager{
		Datas: []*Monster{},
		byID:  map[int]*Monster{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *MonsterManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func MonsterSize() int {
	return monsterMgr.size
}

func (mgr MonsterManager) check(path string, row int, sd *Monster) error {
	if _, ok := monsterMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id重复", path, row)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *MonsterManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *MonsterManager) each(f func(sd *Monster) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *MonsterManager) findIf(f func(sd *Monster) (find bool)) *Monster {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachMonster(f func(sd Monster) (continued bool)) {
	for _, sd := range monsterMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindMonsterIf(f func(sd Monster) bool) (Monster, bool) {
	for _, sd := range monsterMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilMonster, false
}

func GetMonsterByID(id int) (Monster, bool) {
	temp, ok := monsterMgr.byID[id]
	if !ok {
		return nilMonster, false
	}
	return *temp, true
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
