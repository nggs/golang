// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 奖励表
type Reward struct {
	id       int           // 掉落主键ID
	itemName string        // 物品名称
	group    int           // 掉落组ID
	number   int           // 掉落的数量
	dataFrom E_ItemBigType // 掉落取哪张表 1、货币表currency 2、道具表item   3、英雄表heroBasic   4、装备表equipment
	itemId   int           // 掉落的物品ID
	mustDrop bool          // 是否必掉 0=随机 1=必掉
	minTimes int           // 掉落的最少次数
	maxTimes int           // 掉落的最多次数
	weight   int           // 掉落的权重
	desc     string        // 掉落描述

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewReward() *Reward {
	sd := &Reward{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 掉落主键ID
func (sd Reward) ID() int {
	return sd.id
}

// 物品名称
func (sd Reward) ItemName() string {
	return sd.itemName
}

// 掉落组ID
func (sd Reward) Group() int {
	return sd.group
}

// 掉落的数量
func (sd Reward) Number() int {
	return sd.number
}

// 掉落取哪张表 1、货币表currency 2、道具表item   3、英雄表heroBasic   4、装备表equipment
func (sd Reward) DataFrom() E_ItemBigType {
	return sd.dataFrom
}

// 掉落的物品ID
func (sd Reward) ItemID() int {
	return sd.itemId
}

// 是否必掉 0=随机 1=必掉
func (sd Reward) MustDrop() bool {
	return sd.mustDrop
}

// 掉落的最少次数
func (sd Reward) MinTimes() int {
	return sd.minTimes
}

// 掉落的最多次数
func (sd Reward) MaxTimes() int {
	return sd.maxTimes
}

// 掉落的权重
func (sd Reward) Weight() int {
	return sd.weight
}

// 掉落描述
func (sd Reward) Desc() string {
	return sd.desc
}

func (sd Reward) Clone() *Reward {
	n := NewReward()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type RewardManager struct {
	Datas []*Reward

	size int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*Reward // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	byGroup map[int][]*Reward // NormalIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newRewardManager() *RewardManager {
	mgr := &RewardManager{
		Datas:   []*Reward{},
		byID:    map[int]*Reward{},
		byGroup: map[int][]*Reward{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *RewardManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

		mgr.byGroup[d.group] = append(mgr.byGroup[d.group], d)

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func RewardSize() int {
	return rewardMgr.size
}

func (mgr RewardManager) check(path string, row int, sd *Reward) error {
	if _, ok := rewardMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id重复", path, row)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *RewardManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *RewardManager) each(f func(sd *Reward) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *RewardManager) findIf(f func(sd *Reward) (find bool)) *Reward {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachReward(f func(sd Reward) (continued bool)) {
	for _, sd := range rewardMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindRewardIf(f func(sd Reward) bool) (Reward, bool) {
	for _, sd := range rewardMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilReward, false
}

func GetRewardByID(id int) (Reward, bool) {
	temp, ok := rewardMgr.byID[id]
	if !ok {
		return nilReward, false
	}
	return *temp, true
}

func GetRewardByGroup(group int) (vs []Reward, ok bool) {
	var ds []*Reward
	ds, ok = rewardMgr.byGroup[group]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachRewardByGroup(group int, fn func(d Reward) (continued bool)) bool {
	ds, ok := rewardMgr.byGroup[group]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
