package main

import (
	"fmt"
	"strings"
	"text/template"

	"nggs/util"
)

var gTemplateFuncMap = template.FuncMap{
	"GetFormatName":       util.FormatFieldName,
	"IsGoBaseType":        IsGoBaseType,
	"ToTypeScriptType":    ToTypeScriptType,
	"ToTypeScriptKeyType": ToTypeScriptKeyType,
}

func IsGoBaseType(t string) bool {
	switch t {
	case "int", "string", "bool", "float64":
		return true
	}
	return false
}

func ToTypeScriptType(t string) string {
	switch t {
	case "int", "time.Duration", "float64":
		return "number"
	case "string":
		return "string"
	case "bool":
		return "boolean"
	}
	if strings.HasPrefix(t, "[]") {
		elementType := strings.TrimPrefix(t, "[]")
		switch elementType {
		case "int", "time.Duration", "float64":
			return "Array<number>"
		case "string":
			return "Array<string>"
		case "bool":
			return "Array<boolean>"
		default:
			return fmt.Sprintf("Array<%s>", elementType)
		}
	}
	return t
}

func ToTypeScriptKeyType(t string) string {
	switch t {
	case "int", "time.Duration", "float64":
		return "number"
	case "string":
		return "string"
	case "bool":
		return "boolean"
	}
	if strings.HasPrefix(t, "[]") {
		elementType := strings.TrimPrefix(t, "[]")
		switch elementType {
		case "int", "time.Duration", "float64":
			return "Array<number>"
		case "string":
			return "Array<string>"
		case "bool":
			return "Array<boolean>"
		default:
			return fmt.Sprintf("Array<%s>", elementType)
		}
	}
	if strings.HasPrefix(t, "E_") {
		return "number"
	}
	return t
}
