package main

import (
	"encoding/json"
	"testing"
)

type e struct {
	I int
	s string
}

type test struct {
	Is []int
	Ss []string
	E  *e
	Es []e
}

func (t test) String() string {
	bs, _ := json.Marshal(t)
	return string(bs)
}

func TestClone(t *testing.T) {
	t1 := &test{
		Is: []int{123},
		Ss: []string{"456"},
		E:  &e{I: 789, s: "012"},
		Es: []e{{I: 345, s: "678"}},
	}
	t2 := &test{}
	*t2 = *t1

	t2.Is = append(t2.Is, 1)
	t2.Ss = append(t2.Ss, "2")
	t2.E.I = 13434234
	t2.Es = append(t2.Es, e{I: 5, s: "6"})

	t.Logf("t1=%+v\nt2=%+v\n", t1, t2)

	t.Logf("&t1.Is[0]=%p\n&t1.Es[0]=%p\n", &t1.Is[0], &t1.Es[0])
	t.Logf("&t2.Is[0]=%p\n&t2.Es[0]=%p\n", &t2.Is[0], &t2.Es[0])
}
