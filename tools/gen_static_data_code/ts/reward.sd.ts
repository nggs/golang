// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！


import { E_AffixType } from './enums.sd';
import { E_AttributeType } from './enums.sd';
import { E_EquipType } from './enums.sd';
import { E_ItemBigType } from './enums.sd';
import { E_ItemType } from './enums.sd';
import { E_CurrencyType } from './enums.sd';
import { E_AcceleratorType } from './enums.sd';
import { E_Quality } from './enums.sd';
import { E_Faction } from './enums.sd';
import { E_Class } from './enums.sd';
import { E_EquipmentType } from './enums.sd';
import { E_EquipmentPos } from './enums.sd';
import { E_LimitRefreshType } from './enums.sd';
import { E_FunctionType } from './enums.sd';

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////
/**奖励表 */
export class TableReward {

    
    
    private id: number = 0;
    

    
    
    private itemName: string = "";
    

    
    
    private group: number = 0;
    

    
    
    private number: number = 0;
    

    
    
    private dataFrom: E_ItemBigType;
    

    
    
    private itemId: number = 0;
    

    
    
    private mustDrop: boolean = false;
    

    
    
    private minTimes: number = 0;
    

    
    
    private maxTimes: number = 0;
    

    
    
    private weight: number = 0;
    

    
    
    private desc: string = "";
    


    /**掉落主键ID */
    public get ID(): number {
        return this.id;
    }

    /**物品名称 */
    public get ItemName(): string {
        return this.itemName;
    }

    /**掉落组ID */
    public get Group(): number {
        return this.group;
    }

    /**掉落的数量 */
    public get Number(): number {
        return this.number;
    }

    /**掉落取哪张表 1、货币表currency 2、道具表item   3、英雄表heroBasic   4、装备表equipment */
    public get DataFrom(): E_ItemBigType {
        return this.dataFrom;
    }

    /**掉落的物品ID */
    public get ItemID(): number {
        return this.itemId;
    }

    /**是否必掉 0=随机 1=必掉 */
    public get MustDrop(): boolean {
        return this.mustDrop;
    }

    /**掉落的最少次数 */
    public get MinTimes(): number {
        return this.minTimes;
    }

    /**掉落的最多次数 */
    public get MaxTimes(): number {
        return this.maxTimes;
    }

    /**掉落的权重 */
    public get Weight(): number {
        return this.weight;
    }

    /**掉落描述 */
    public get Desc(): string {
        return this.desc;
    }

    //请勿手动调用此方法
    public init(data){
        let self = this;
        for (const key in data) {
            if (data.hasOwnProperty(key)) {
                const element = data[key];
                self[key] = element;
            }
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////
    // TODO 添加表结构扩展代码
    //<Table>//</Table>
    //////////////////////////////////////////////////////////////////////////////////////////////////
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
