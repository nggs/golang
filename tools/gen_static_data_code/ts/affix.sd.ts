// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！


import { E_AffixType } from './enums.sd';
import { E_AttributeType } from './enums.sd';
import { E_EquipType } from './enums.sd';
import { E_ItemBigType } from './enums.sd';
import { E_ItemType } from './enums.sd';
import { E_CurrencyType } from './enums.sd';
import { E_AcceleratorType } from './enums.sd';
import { E_Quality } from './enums.sd';
import { E_Faction } from './enums.sd';
import { E_Class } from './enums.sd';
import { E_EquipmentType } from './enums.sd';
import { E_EquipmentPos } from './enums.sd';
import { E_LimitRefreshType } from './enums.sd';
import { E_FunctionType } from './enums.sd';

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////
/**词缀表 */
export class TableAffix {

    
    
    private type: E_AffixType;
    

    
    
    private id: number = 0;
    

    
    
    private name: string = "";
    

    
    
    private rare: boolean = false;
    

    
    
    private level: number = 0;
    

    
    
    private level_req: number = 0;
    

    
    
    private frequency: number = 0;
    

    
    
    private group: number = 0;
    

    
    
    private mod1code: string = "";
    

    
    
    private mod1param: number = 0;
    

    
    
    private mod1min: number = 0;
    

    
    
    private mod1max: number = 0;
    

    
    
    private mod2code: string = "";
    

    
    
    private mod2param: number = 0;
    

    
    
    private mod2min: number = 0;
    

    
    
    private mod2max: number = 0;
    

    
    
    private is: Array<number>;
    

    
    
    private fs: Array<number>;
    

    
    
    private bs: Array<boolean>;
    

    
    
    private ss: Array<string>;
    

    
    
    private dur: number = 0;
    

    
    
    private durs: Array<number>;
    

    
    
    private enums: Array<E_EquipType>;
    

    
    
    private itype: Array<E_EquipType>;
    

    
    
    private etype: Array<number>;
    


    /**词缀类别 test */
    public get Type(): E_AffixType {
        return this.type;
    }

    /**词缀ID */
    public get ID(): number {
        return this.id;
    }

    /**词缀名称 */
    public get Name(): string {
        return this.name;
    }

    /**稀有 */
    public get Rare(): boolean {
        return this.rare;
    }

    /**词缀等级 */
    public get Level(): number {
        return this.level;
    }

    /**等级需求 */
    public get LevelReq(): number {
        return this.level_req;
    }

    /**词缀频率 */
    public get Frequency(): number {
        return this.frequency;
    }

    /**词缀组 */
    public get Group(): number {
        return this.group;
    }

    /**词缀1属性 */
    public get Mod1code(): string {
        return this.mod1code;
    }

    /**词缀1参数 */
    public get Mod1param(): number {
        return this.mod1param;
    }

    /**词缀1最小值参数 */
    public get Mod1min(): number {
        return this.mod1min;
    }

    /**词缀1最大值参数 */
    public get Mod1max(): number {
        return this.mod1max;
    }

    /**词缀2属性 */
    public get Mod2code(): string {
        return this.mod2code;
    }

    /**词缀2参数 */
    public get Mod2param(): number {
        return this.mod2param;
    }

    /**词缀2最小值参数 */
    public get Mod2min(): number {
        return this.mod2min;
    }

    /**词缀2最大值参数 */
    public get Mod2max(): number {
        return this.mod2max;
    }

    /**整型数组 */
    public get Is(): Array<number> {
        return this.is;
    }

    /**浮点数组 */
    public get Fs(): Array<number> {
        return this.fs;
    }

    /**布尔数组 */
    public get Bs(): Array<boolean> {
        return this.bs;
    }

    /**字符串数组 */
    public get Ss(): Array<string> {
        return this.ss;
    }

    /**时间段 */
    public get Dur(): number {
        return this.dur;
    }

    /**时间段数组 */
    public get Durs(): Array<number> {
        return this.durs;
    }

    /**枚举数组 */
    public get Enums(): Array<E_EquipType> {
        return this.enums;
    }

    /**适用装备的切片 */
    public get Itype(): Array<E_EquipType> {
        return this.itype;
    }

    /**不适用装备的切片 */
    public get Etype(): Array<number> {
        return this.etype;
    }

    //请勿手动调用此方法
    public init(data){
        let self = this;
        for (const key in data) {
            if (data.hasOwnProperty(key)) {
                const element = data[key];
                self[key] = element;
            }
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////
    // TODO 添加表结构扩展代码
    //<Table>//</Table>
    //////////////////////////////////////////////////////////////////////////////////////////////////
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
/**词缀表全局属性 */
export class TableAffixGlobal {
    private static i: number;
    private static str: string;
    private static f64: number;
    private static dur: number;
    private static b: boolean;
    private static is: Array<number>;
    private static ds: Array<number>;
    private static fs: Array<number>;
    private static ss: Array<string>;
    private static bs: Array<boolean>;
    private static e: E_ItemBigType;
    private static es: Array<E_ItemBigType>;


    /**整型 */
    public static get I(): number {
        return this.i;
    }

    /**字符串 */
    public static get Str(): string {
        return this.str;
    }

    /**浮点 */
    public static get F64(): number {
        return this.f64;
    }

    /**时间段 */
    public static get Dur(): number {
        return this.dur;
    }

    /**布尔 */
    public static get B(): boolean {
        return this.b;
    }

    /**整型数组 */
    public static get Is(): Array<number> {
        return this.is;
    }

    /**时间段数组 */
    public static get Ds(): Array<number> {
        return this.ds;
    }

    /**浮点数组 */
    public static get Fs(): Array<number> {
        return this.fs;
    }

    /**字符串数组 */
    public static get Ss(): Array<string> {
        return this.ss;
    }

    /**布尔数组 */
    public static get Bs(): Array<boolean> {
        return this.bs;
    }

    /**枚举 */
    public static get E(): E_ItemBigType {
        return this.e;
    }

    /**枚举数组 */
    public static get Es(): Array<E_ItemBigType> {
        return this.es;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // TODO 添加全局属性扩展代码
    //<Global>//</Global>
    //////////////////////////////////////////////////////////////////////////////////////////////////
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
