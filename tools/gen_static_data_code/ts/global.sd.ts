// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！


import { E_AffixType } from './enums.sd';
import { E_AttributeType } from './enums.sd';
import { E_EquipType } from './enums.sd';
import { E_ItemBigType } from './enums.sd';
import { E_ItemType } from './enums.sd';
import { E_CurrencyType } from './enums.sd';
import { E_AcceleratorType } from './enums.sd';
import { E_Quality } from './enums.sd';
import { E_Faction } from './enums.sd';
import { E_Class } from './enums.sd';
import { E_EquipmentType } from './enums.sd';
import { E_EquipmentPos } from './enums.sd';
import { E_LimitRefreshType } from './enums.sd';
import { E_FunctionType } from './enums.sd';

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////
/**全局表 */
export class TableGlobal {


    //请勿手动调用此方法
    public init(data){
        let self = this;
        for (const key in data) {
            if (data.hasOwnProperty(key)) {
                const element = data[key];
                self[key] = element;
            }
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////
    // TODO 添加表结构扩展代码
    //<Table>//</Table>
    //////////////////////////////////////////////////////////////////////////////////////////////////
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
/**全局表全局属性 */
export class TableGlobalGlobal {
    private static i: number;
    private static str: string;
    private static f64: number;
    private static dur: number;
    private static b: boolean;


    /**整型 */
    public static get I(): number {
        return this.i;
    }

    /**字符串 */
    public static get Str(): string {
        return this.str;
    }

    /**浮点 */
    public static get F64(): number {
        return this.f64;
    }

    /**时间段 */
    public static get Dur(): number {
        return this.dur;
    }

    /**布尔 */
    public static get B(): boolean {
        return this.b;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // TODO 添加全局属性扩展代码
    //<Global>//</Global>
    //////////////////////////////////////////////////////////////////////////////////////////////////
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
