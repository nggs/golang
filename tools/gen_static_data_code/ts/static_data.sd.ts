// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

import TplLoader from "../core/TplLoader";
import { TableAffix, TableAffixGlobal } from "./affix.sd";
import { TableContent } from "./content.sd";
import { TableGlobal, TableGlobalGlobal } from "./global.sd";
import { TableReward } from "./reward.sd";

import { E_AffixType } from './enums.sd';
import { E_AttributeType } from './enums.sd';
import { E_EquipType } from './enums.sd';
import { E_ItemBigType } from './enums.sd';
import { E_ItemType } from './enums.sd';
import { E_CurrencyType } from './enums.sd';
import { E_AcceleratorType } from './enums.sd';
import { E_Quality } from './enums.sd';
import { E_Faction } from './enums.sd';
import { E_Class } from './enums.sd';
import { E_EquipmentType } from './enums.sd';
import { E_EquipmentPos } from './enums.sd';
import { E_LimitRefreshType } from './enums.sd';
import { E_FunctionType } from './enums.sd';

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * 静态表管理类
 */
export default class TableManager {
    private static affix = new Array<TableAffix>();
    private static affixByID: { [key: number]: TableAffix } = {};
    private static affixByType: { [key: number]: Array<TableAffix> } = {};private static content = new Array<TableContent>();
    private static contentByKey: { [key: number]: TableContent } = {};
    private static global = new Array<TableGlobal>();
    
    private static reward = new Array<TableReward>();
    private static rewardByID: { [key: number]: TableReward } = {};
    private static rewardByGroup: { [key: number]: Array<TableReward> } = {};

    public static init() {
        let self = this;
        //////////////////////////////////////////////////////////////////////////////////////////////////
        self.loadAffix();
        self.loadContent();
        self.loadGlobal();
        self.loadReward();
        //////////////////////////////////////////////////////////////////////////////////////////////////
        // TODO 添加加载代码
        //<Table>//</Table>
        //////////////////////////////////////////////////////////////////////////////////////////////////
        TplLoader.Instance.close();
    }
    
    private static loadAffix(){
        let self = this;
        let jsonName = "json/affix.json";
        let data = TplLoader.Instance.LoadTplArray(jsonName);
        if (data.table) {
            let tableData = data.table;
            for (let i = 0; i < tableData.length; i++) {
                let obj = tableData[i];
                let one = new TableAffix();
                one.init(obj);
                self.affix.push(one);
            }
        }
        
        for(let i = 0; i < self.affix.length; i++){
            let ele = self.affix[i];

            self.affixByID[ele.ID] = ele;

            if(!self.affixByType[ele.Type]){
                self.affixByType[ele.Type] = new Array<TableAffix>()
            }
            self.affixByType[ele.Type].push(ele);
        }

        let globalData = data.global;
        for (let key in globalData) {
            if (globalData.hasOwnProperty(key)) {
                TableAffixGlobal[key] = globalData[key];
            }
        }
    }

    public static GetAffixByID(id: number): TableAffix {
        let self = this;
        if (self.affixByID[id]) {
            return self.affixByID[id]
        }
        return null
    }

    public static GetAffixsByType(type: E_AffixType): Array<TableAffix> {
        let self = this;
        if (self.affixByType[type]) {
            return self.affixByType[type]
        }
        return null
    }
    
    private static loadContent(){
        let self = this;
        let jsonName = "json/content.json";
        let data = TplLoader.Instance.LoadTplArray(jsonName);
        if (data.table) {
            let tableData = data.table;
            for (let i = 0; i < tableData.length; i++) {
                let obj = tableData[i];
                let one = new TableContent();
                one.init(obj);
                self.content.push(one);
            }
        }
        
        for(let i = 0; i < self.content.length; i++){
            let ele = self.content[i];

            self.contentByKey[ele.Key] = ele;

            
        }

        
    }

    public static GetContentByKey(key: number): TableContent {
        let self = this;
        if (self.contentByKey[key]) {
            return self.contentByKey[key]
        }
        return null
    }

    
    
    private static loadGlobal(){
        let self = this;
        let jsonName = "json/global.json";
        let data = TplLoader.Instance.LoadTplArray(jsonName);
        if (data.table) {
            let tableData = data.table;
            for (let i = 0; i < tableData.length; i++) {
                let obj = tableData[i];
                let one = new TableGlobal();
                one.init(obj);
                self.global.push(one);
            }
        }
        

        let globalData = data.global;
        for (let key in globalData) {
            if (globalData.hasOwnProperty(key)) {
                TableGlobalGlobal[key] = globalData[key];
            }
        }
    }

    

    
    
    private static loadReward(){
        let self = this;
        let jsonName = "json/reward.json";
        let data = TplLoader.Instance.LoadTplArray(jsonName);
        if (data.table) {
            let tableData = data.table;
            for (let i = 0; i < tableData.length; i++) {
                let obj = tableData[i];
                let one = new TableReward();
                one.init(obj);
                self.reward.push(one);
            }
        }
        
        for(let i = 0; i < self.reward.length; i++){
            let ele = self.reward[i];

            self.rewardByID[ele.ID] = ele;

            if(!self.rewardByGroup[ele.Group]){
                self.rewardByGroup[ele.Group] = new Array<TableReward>()
            }
            self.rewardByGroup[ele.Group].push(ele);
        }

        
    }

    public static GetRewardByID(id: number): TableReward {
        let self = this;
        if (self.rewardByID[id]) {
            return self.rewardByID[id]
        }
        return null
    }

    public static GetRewardsByGroup(group: number): Array<TableReward> {
        let self = this;
        if (self.rewardByGroup[group]) {
            return self.rewardByGroup[group]
        }
        return null
    }
    

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // TODO 添加管理类扩展代码
    //<Global>//</Global>
    //////////////////////////////////////////////////////////////////////////////////////////////////
}
//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
