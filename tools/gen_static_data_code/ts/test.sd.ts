// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！


import { E_AffixType } from './enums.sd';
import { E_AttributeType } from './enums.sd';
import { E_EquipType } from './enums.sd';
import { E_ItemBigType } from './enums.sd';
import { E_ItemType } from './enums.sd';
import { E_CurrencyType } from './enums.sd';
import { E_AcceleratorType } from './enums.sd';
import { E_Quality } from './enums.sd';
import { E_Faction } from './enums.sd';
import { E_Class } from './enums.sd';
import { E_EquipmentType } from './enums.sd';
import { E_EquipmentPos } from './enums.sd';
import { E_LimitRefreshType } from './enums.sd';
import { E_FunctionType } from './enums.sd';

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

export class TableTest {
    private tYpE: E_AffixType;
    private id: number;
    private name: string;
    private rare: boolean;
    private level: number;
    private level_req: number;
    private frequency: number;
    private group: number;
    private mod1code: string;
    private mod1param: number;
    private mod1min: number;
    private mod1max: number;
    private mod2code: string;
    private mod2param: number;
    private mod2min: number;
    private mod2max: number;
    private itype: Array<E_EquipType>;
    private etype: Array<number>;


    /**词缀类别 test test */
    public get Type(): E_AffixType {
        return this.tYpE;
    }

    /**词缀ID */
    public get ID(): number {
        return this.id;
    }

    /**词缀名称 */
    public get Name(): string {
        return this.name;
    }

    /**稀有 */
    public get Rare(): boolean {
        return this.rare;
    }

    /**词缀等级 */
    public get Level(): number {
        return this.level;
    }

    /**等级需求 */
    public get LevelReq(): number {
        return this.level_req;
    }

    /**词缀频率 */
    public get Frequency(): number {
        return this.frequency;
    }

    /**词缀组 */
    public get Group(): number {
        return this.group;
    }

    /**词缀1属性 */
    public get Mod1code(): string {
        return this.mod1code;
    }

    /**词缀1参数 */
    public get Mod1param(): number {
        return this.mod1param;
    }

    /**词缀1最小值参数 */
    public get Mod1min(): number {
        return this.mod1min;
    }

    /**词缀1最大值参数 */
    public get Mod1max(): number {
        return this.mod1max;
    }

    /**词缀2属性 */
    public get Mod2code(): string {
        return this.mod2code;
    }

    /**词缀2参数 */
    public get Mod2param(): number {
        return this.mod2param;
    }

    /**词缀2最小值参数 */
    public get Mod2min(): number {
        return this.mod2min;
    }

    /**词缀2最大值参数 */
    public get Mod2max(): number {
        return this.mod2max;
    }

    /**适用装备的切片 */
    public get Itype(): Array<E_EquipType> {
        return this.itype;
    }

    /**不适用装备的切片 */
    public get Etype(): Array<number> {
        return this.etype;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // TODO 添加表结构扩展代码
    //<Table>//</Table>
    //////////////////////////////////////////////////////////////////////////////////////////////////
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
