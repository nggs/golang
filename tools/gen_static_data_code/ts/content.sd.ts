// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！


import { E_AffixType } from './enums.sd';
import { E_AttributeType } from './enums.sd';
import { E_EquipType } from './enums.sd';
import { E_ItemBigType } from './enums.sd';
import { E_ItemType } from './enums.sd';
import { E_CurrencyType } from './enums.sd';
import { E_AcceleratorType } from './enums.sd';
import { E_Quality } from './enums.sd';
import { E_Faction } from './enums.sd';
import { E_Class } from './enums.sd';
import { E_EquipmentType } from './enums.sd';
import { E_EquipmentPos } from './enums.sd';
import { E_LimitRefreshType } from './enums.sd';
import { E_FunctionType } from './enums.sd';

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////
/**文本表 */
export class TableContent {

    
    
    private key: number = 0;
    

    
    
    private content: string = "";
    

    
    
    private type: number = 0;
    


    /**主键 */
    public get Key(): number {
        return this.key;
    }

    /**描述 */
    public get Content(): string {
        return this.content;
    }

    /**文本类型 0=普通文本 1=富文本 */
    public get Type(): number {
        return this.type;
    }

    //请勿手动调用此方法
    public init(data){
        let self = this;
        for (const key in data) {
            if (data.hasOwnProperty(key)) {
                const element = data[key];
                self[key] = element;
            }
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////
    // TODO 添加表结构扩展代码
    //<Table>//</Table>
    //////////////////////////////////////////////////////////////////////////////////////////////////
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
