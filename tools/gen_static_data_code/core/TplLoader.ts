import UIEventMg from "./UIEventMg";
import { UIEventSign } from "../config/CEvent";

/**
 * 配置读取
 */

export default class TplLoader {
    private static _ins: TplLoader;

    public static get Instance() {
        if (TplLoader._ins == null) {
            TplLoader._ins = new TplLoader();
        }
        return TplLoader._ins;
    }

    private _zip:JSZip;

    public InitLoader(p_zipdata: any) {
        this._zip = new JSZip();
        this._zip.load(p_zipdata, {});
    }

    public LoadTplArray(jsonFile:string) {
        const file = this._zip.file(jsonFile);
        // return JSON.parse(file.asText());
        let data = {global: null, table: null};
        data.table = JSON.parse(file.asText()).Datas;
        data.global = JSON.parse(file.asText()).Global;
        return data;
        // return JSON.parse(file.asText()).Datas;
    }

    public close(): void {
        delete this._zip;
        this._zip = null;
        cc.loader.releaseRes("tpl");
        UIEventMg.Instance.emit(UIEventSign.STATIC_TABLE_OK);
    }
}
