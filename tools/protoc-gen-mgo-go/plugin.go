package main

import (
	"bytes"
	"fmt"
	"regexp"

	"nggs/tools/pbplugin/golang"
	"nggs/tools/pbplugin/golang/generator"
)

var (
	collectionRegexp = regexp.MustCompile(`@collection(?:\s*=\s*(\d+))?`)
	msgRegexp        = regexp.MustCompile(`@msg(?:\s*=\s*(\S+))?`)
)

type mgo struct {
	*golang.PluginSuper
}

func New() *mgo {
	return &mgo{
		PluginSuper: golang.NewPluginSuper(),
	}
}

func (p *mgo) Name() string {
	return "mgo-go"
}

func (p *mgo) Init(g *generator.Generator) {
	p.Generator = g
}

func (p *mgo) GenerateImports(file *generator.FileDescriptor) {

}

func (p *mgo) Generate(fd *generator.FileDescriptor) {
	p.PluginImports = generator.NewPluginImports(p.Generator)

	p.AddImport("msg")

	syncPkg := p.NewImport("sync")
	mongodbPkg := p.NewImport("nggs/db/mongodb")
	mgoPkg := p.NewImport("github.com/globalsign/mgo")

	file := newFile(fd)

	for _, md := range file.FileDescriptor.Messages() {
		if md.DescriptorProto.GetOptions().GetMapEntry() {
			continue
		}

		if !syncPkg.IsUsed() {
			syncPkg.Use()
			p.AddImport(generator.GoImportPath(syncPkg.Location()))
		}

		message := newMessage(p.Generator, md)

		if matches := collectionRegexp.FindStringSubmatch(message.Comment); len(matches) > 0 {
			message.ID = message.Name
			if !mongodbPkg.IsUsed() {
				mongodbPkg.Use()
				p.AddImport(generator.GoImportPath(mongodbPkg.Location()))
			}
			if !mgoPkg.IsUsed() {
				mgoPkg.Use()
				p.AddImport(generator.GoImportPath(mgoPkg.Location()))
			}
		}

		if matches := msgRegexp.FindStringSubmatch(message.Comment); len(matches) > 0 {
			if len(matches) > 1 && matches[1] != "" {
				message.Msg = matches[1]
			} else {
				message.Msg = message.Name
			}
		}

		for commentIndex, fdp := range md.GetField() {
			field := newField(p.Generator, md, fdp, commentIndex)

			if matches := msgRegexp.FindStringSubmatch(field.Comment); len(matches) > 0 {
				if message.Msg == "" {
					message.Msg = message.Name
				}

				if len(matches) > 1 && matches[1] != "" {
					field.Msg = matches[1]
				} else {
					field.Msg = field.Name
				}
				if field.IsRepeated() {
					if field.IsMap {
						switch field.ValueTypeToName {
						case "int32", "uint32", "int64", "uint64",
							"sint32", "sint64", "fixed32", "fixed64", "enum", "bool", "string",
							"float64", "float32":
							field.MsgRepeatedMessageGoType = fmt.Sprintf("map[%s]%s", field.KeyType, field.ValueTypeToName)
						case "byte":
							field.MsgRepeatedMessageGoType = fmt.Sprintf("map[%s]%s", field.KeyType, field.ValueType)
						default:
							field.MsgRepeatedMessageGoType = fmt.Sprintf("map[%s]*msg.%s", field.KeyType, field.ValueTypeToName)
						}
					} else {
						if field.IsMessage() {
							field.MsgRepeatedMessageGoType = fmt.Sprintf("[]*msg.%s", field.GoTypeToName)
						}
					}
				}
			}

			p.RecordTypeUse(fdp.GetTypeName())
			message.Fields = append(message.Fields, field)
		}

		p.RecordTypeUse(generator.CamelCaseSlice(md.TypeName()))
		file.Messages = append(file.Messages, message)
	}

	for commentIndex1, ed := range fd.Enums() {
		enum := golang.NewEnum(p.Generator, ed, commentIndex1)

		for commentIndex2, edp := range ed.GetValue() {
			enumValue := golang.NewEnumValue(p.Generator, enum.Name, edp, commentIndex1, commentIndex2)
			enum.Values = append(enum.Values, enumValue)
		}

		p.RecordTypeUse(enum.Name)
		file.Enums = append(file.Enums, enum)
	}

	var code bytes.Buffer
	err := p.Template.Execute(&code, file)
	if err != nil {
		panic(err)
	}
	s := code.String()
	p.P(s)
}

func init() {
	generator.RegisterPlugin(New())
}
