package main

import (
	"io/ioutil"

	"nggs/tools/pbplugin"
	"nggs/tools/pbplugin/golang"
	"nggs/tools/pbplugin/golang/command"
)

func main() {
	req := command.Read()

	reqParams := map[string]string{}
	if req.GetParameter() != "" {
		reqParams = pbplugin.ParseRequestParameterString(req.GetParameter())
	}

	p := New()

	if templateFilePath, ok := reqParams["tpl"]; ok {
		templateContent, err := ioutil.ReadFile(templateFilePath)
		if err != nil {
			panic(err)
		}
		p.MustLoadTemplate("mgo-go", string(templateContent), golang.TemplateFunc)
	} else {
		p.MustLoadTemplate("mgo-go", DefaultTemplate, golang.TemplateFunc)
	}

	resp := command.GeneratePlugin(req, p, ".mgo.go")
	command.Write(resp)
}
