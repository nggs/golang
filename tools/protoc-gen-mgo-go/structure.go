package main

import (
	"nggs/tools/pbplugin/golang"
	"nggs/tools/pbplugin/golang/generator"

	"github.com/gogo/protobuf/protoc-gen-gogo/descriptor"
)

type field struct {
	*golang.Field

	Msg                      string
	MsgRepeatedMessageGoType string
}

func newField(g *generator.Generator, d *generator.Descriptor, fdp *descriptor.FieldDescriptorProto, commentIndex int) *field {
	f := &field{
		Field: golang.NewField(g, d, fdp, commentIndex),
	}
	return f
}

type message struct {
	*golang.Message

	Fields []*field

	Msg string
}

func newMessage(g *generator.Generator, d *generator.Descriptor) *message {
	m := &message{
		Message: golang.NewMessage(g, d),
	}
	return m
}

type file struct {
	*golang.File
	Messages []*message
}

func newFile(fd *generator.FileDescriptor) *file {
	f := &file{
		File: golang.NewFile(fd),
	}
	return f
}
