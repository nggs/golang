package main

import (
	"path/filepath"
	"testing"
)

func TestFileMatch(t *testing.T) {
	m, err := filepath.Match("*.cpp", "test.cpp")
	if err != nil {
		t.Error(err)
		return
	}
	if m {
		t.Log("matched")
	} else {
		t.Log("unmatched")
	}
}

func TestFilepathRel(t *testing.T) {
	r, err := filepath.Rel("e:/", "d:/workspace/test.txt")
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(r)
}
