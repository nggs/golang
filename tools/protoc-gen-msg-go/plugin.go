package main

import (
	"bytes"
	"fmt"
	"regexp"
	"strings"

	"nggs/tools/pbplugin/golang"
	"nggs/tools/pbplugin/golang/generator"
)

const (
	defaultMinInterval = "100"
)

var (
	idRegexp          = regexp.MustCompile(`\s*@id(?:\s*=\s*(\d+))?\s*`)
	responseRegexp    = regexp.MustCompile(`\s*@response(?:\s*=\s*(\S+))?\s*`)
	onceRegexp        = regexp.MustCompile(`\s*@once\s*`)
	minIntervalRegexp = regexp.MustCompile(`\s*@min_interval(?:\s*=\s*(\d+))?\s*`)
)

func init() {
	generator.RegisterPlugin(New())
}

type pbex struct {
	*golang.PluginSuper
	version string
}

func New() *pbex {
	return &pbex{
		PluginSuper: golang.NewPluginSuper(),
		version:     "v1",
	}
}

func (p *pbex) Name() string {
	return PluginName
}

func (p *pbex) Init(g *generator.Generator) {
	p.Generator = g
}

func (p *pbex) GenerateImports(file *generator.FileDescriptor) {

}

func (p *pbex) Generate(fd *generator.FileDescriptor) {
	p.PluginImports = generator.NewPluginImports(p.Generator)

	syncPkg := p.NewImport("sync")

	timePkg := p.NewImport("time")

	protocolImportPath := fmt.Sprintf("nggs/network/protocol/protobuf/%s", p.version)
	protocolPkg := p.NewImport(protocolImportPath)

	file := newFile(fd)
	file.ProtocolVersion = p.version

	for _, md := range file.FileDescriptor.Messages() {
		if md.DescriptorProto.GetOptions().GetMapEntry() {
			continue
		}

		if !syncPkg.IsUsed() {
			syncPkg.Use()
			p.AddImport(generator.GoImportPath(syncPkg.Location()))
		}

		message := newMessage(p.Generator, md)

		if matches := idRegexp.FindStringSubmatch(message.Comment); len(matches) > 1 && matches[1] != "" {
			message.ID = matches[1]

			message.Name += "_EX"

			if !protocolPkg.IsUsed() {
				protocolPkg.Use()
				p.AddImport(generator.GoImportPath(protocolPkg.Location()))
			}

			if !timePkg.IsUsed() {
				timePkg.Use()
				p.AddImport(generator.GoImportPath(timePkg.Location()))
			}

			if matches := onceRegexp.FindStringSubmatch(message.Comment); len(matches) > 0 {
				message.Once = true
			}

			if matches := minIntervalRegexp.FindStringSubmatch(message.Comment); len(matches) > 1 && matches[1] != "" {
				message.MinInterval = matches[1]
			}
		}

		nameWords := strings.Split(message.Name, "_")
		if len(nameWords) > 1 && (nameWords[0] == "C2S" || nameWords[0] == "S2C") {
			if matches := responseRegexp.FindStringSubmatch(message.Comment); len(matches) > 1 {
				message.Response = matches[1]
			}
		}

		for commentIndex, fdp := range md.GetField() {
			field := golang.NewField(p.Generator, md, fdp, commentIndex)
			p.RecordTypeUse(fdp.GetTypeName())
			message.Fields = append(message.Fields, field)
		}

		p.RecordTypeUse(generator.CamelCaseSlice(md.TypeName()))
		file.Messages = append(file.Messages, message)
		file.messagesByName[message.Name] = message
	}

	for _, message := range file.Messages {
		if message.Response != "" {
			continue
		}
		nameWords := strings.Split(message.Name, "_")
		if len(nameWords) <= 1 {
			continue
		}
		switch nameWords[0] {
		case "C2S":
			response := fmt.Sprintf("S2C_%s_EX", nameWords[1])
			if _, ok := file.messagesByName[response]; ok {
				message.Response = fmt.Sprintf("S2C_%s", nameWords[1])
			}
		case "S2C":
			response := fmt.Sprintf("C2S_%s_EX", nameWords[1])
			if _, ok := file.messagesByName[response]; ok {
				message.Response = fmt.Sprintf("C2S_%s", nameWords[1])
			}
		}
	}

	for commentIndex1, ed := range fd.Enums() {
		enum := golang.NewEnum(p.Generator, ed, commentIndex1)

		for commentIndex2, edp := range ed.GetValue() {
			enumValue := golang.NewEnumValue(p.Generator, enum.Name, edp, commentIndex1, commentIndex2)
			enum.Values = append(enum.Values, enumValue)
		}

		p.RecordTypeUse(enum.Name)
		file.Enums = append(file.Enums, enum)
	}

	var code bytes.Buffer
	err := p.Template.Execute(&code, file)
	if err != nil {
		panic(err)
	}
	s := code.String()
	p.P(s)
}

func (p *pbex) SetVersion(version string) {
	p.version = version
}
