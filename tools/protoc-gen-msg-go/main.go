package main

import (
	"io/ioutil"

	"nggs/tools/pbplugin"
	"nggs/tools/pbplugin/golang"
	"nggs/tools/pbplugin/golang/command"
)

func main() {
	req := command.Read()

	reqParams := map[string]string{}
	if req.GetParameter() != "" {
		reqParams = pbplugin.ParseRequestParameterString(req.GetParameter())
	}

	p := New()

	if templateFilePath, ok := reqParams["tpl"]; ok {
		templateContent, err := ioutil.ReadFile(templateFilePath)
		if err != nil {
			panic(err)
		}
		p.MustLoadTemplate(PluginName, string(templateContent), golang.TemplateFunc)
	} else {
		p.MustLoadTemplate(PluginName, DefaultTemplate, golang.TemplateFunc)
	}

	if version, ok := reqParams["version"]; ok && version != "" {
		p.SetVersion(version)
	}

	resp := command.GeneratePlugin(req, p, ".msg.go")
	command.Write(resp)
}
