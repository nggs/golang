package main

import (
	"nggs/tools/pbplugin/golang"
	"nggs/tools/pbplugin/golang/generator"
)

const (
	PluginName = "pbex-go"
)

type message struct {
	*golang.Message

	ProtoName string

	Response    string
	Once        bool
	MinInterval string
}

func newMessage(g *generator.Generator, d *generator.Descriptor) *message {
	m := &message{
		Message:     golang.NewMessage(g, d),
		MinInterval: defaultMinInterval,
	}
	m.ProtoName = m.Name
	return m
}

type file struct {
	*golang.File
	ProtocolVersion string
	Messages        []*message
	messagesByName  map[string]*message
}

func newFile(fd *generator.FileDescriptor) *file {
	f := &file{
		File:           golang.NewFile(fd),
		messagesByName: map[string]*message{},
	}
	return f
}

func (f file) ProtocolTagFunctionName() string {
	return generator.CamelCase(f.ProtocolVersion)
}
