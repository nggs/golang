@set WORK_DIR=%~dp0

@set TOOLS_DIR=%WORK_DIR%\tools

@set BIN_DIR=%WORK_DIR%\bin

@set TESTS_DIR=%WORK_DIR%\tests

@IF "%1" == "" call :install & cd %WORK_DIR% & goto :exit

@IF "%1" == "install" call :install & cd %WORK_DIR% & goto :exit

@IF "%1" == "nctl" call :nctl & cd %WORK_DIR% & goto :exit

@IF "%1" == "mod-tidy" call :mod-tidy & cd %WORK_DIR% & goto :exit

@IF "%1" == "protoc-gen-msg-go" call :protoc-gen-msg-go & cd %WORK_DIR% & goto :exit

@IF "%1" == "msg" call :msg & cd %WORK_DIR% & goto :exit

@IF "%1" == "protoc-gen-mgo-go" call :protoc-gen-mgo-go & cd %WORK_DIR% & goto :exit

@IF "%1" == "model" call :model & cd %WORK_DIR% & goto :exit

@IF "%1" == "protoc-gen-rpc-go" call :protoc-gen-rpc-go & cd %WORK_DIR% & goto :exit

@IF "%1" == "rpc" call :rpc & cd %WORK_DIR% & goto :exit

@IF "%1" == "gen_static_data_json" call :gen_static_data_json & cd %WORK_DIR% & goto :exit

@IF "%1" == "gen_static_data_code" call :gen_static_data_code & cd %WORK_DIR% & goto :exit

@echo unsupported operate [%1]

@goto :exit


:install
@echo install all begin
@call :nctl
@call :protoc-gen-msg-go
@call :protoc-gen-mgo-go
@call :protoc-gen-rpc-go
@call :gen_static_data_json
@call :gen_static_data_code
@echo install all end
@goto :exit


:nctl
@echo install [nctl] begin
go install %WORK_DIR%\tools\nctl
@echo install [nctl] end
@goto :exit


:mod-tidy
@echo [mod-tidy] begin
go mod tidy
@echo [mod-tidy] end
@goto :exit


:protoc-gen-msg-go
@echo [protoc-gen-msg-go] begin
go build -o %BIN_DIR%\tools\pbplugin\protoc-gen-msg-go.exe %TOOLS_DIR%\protoc-gen-msg-go
go install %TOOLS_DIR%\protoc-gen-msg-go
@echo [protoc-gen-msg-go] end
@goto :exit


:msg
@echo [msg] begin
cd %TESTS_DIR%\msg
del /q /s *.pb.go
del /q /s *.msg.go
del /q /s derived.gen.go
call %TESTS_DIR%\protos\gen_msg.bat %WORK_DIR%
%BIN_DIR%\tools\goderive.exe .
go test
@echo [msg] end
@goto :exit


:protoc-gen-mgo-go
@echo [protoc-gen-mgo-go] begin
go build -o %BIN_DIR%\tools\pbplugin\protoc-gen-mgo-go.exe %TOOLS_DIR%\protoc-gen-mgo-go
go install %TOOLS_DIR%\protoc-gen-mgo-go
@echo [protoc-gen-mgo-go] end
@goto :exit


:model
@echo [model] begin
cd %TESTS_DIR%\model
del /q /s *.pb.go
del /q /s *.mgo.go
del /q /s derived.gen.go
call %TESTS_DIR%\protos\gen_model.bat %WORK_DIR%
%BIN_DIR%\tools\goderive.exe .
go test
@echo [model] end
@goto :exit


:protoc-gen-rpc-go
@echo [protoc-gen-rpc-go] begin
go build -o %BIN_DIR%\tools\pbplugin\protoc-gen-rpc-go.exe %TOOLS_DIR%\protoc-gen-rpc-go
go install %TOOLS_DIR%\protoc-gen-rpc-go
@echo [protoc-gen-rpc-go] end
@goto :exit


:rpc
@echo [rpc] begin
cd %TESTS_DIR%\rpc
del /q /s *.pb.go
del /q /s *.rpc.go
del /q /s derived.gen.go
call %TESTS_DIR%\protos\gen_rpc.bat %WORK_DIR%
%BIN_DIR%\tools\goderive.exe .
go test
@echo [rpc] end
@goto :exit


:gen_static_data_json
@echo [gen_static_data_json] begin
go build -o %BIN_DIR%\tools\gen_static_data_json.exe %TOOLS_DIR%\gen_static_data_json
go install %TOOLS_DIR%\gen_static_data_json
@echo [gen_static_data_json] end
@goto :exit


:gen_static_data_code
@echo [gen_static_data_code] begin
go build -o %BIN_DIR%\tools\gen_static_data_code.exe %TOOLS_DIR%\gen_static_data_code
go install %TOOLS_DIR%\gen_static_data_code
@echo [gen_static_data_code] end
@goto :exit


:exit
