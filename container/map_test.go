package container

import "testing"

func printInt32ToBoolMap(t *testing.T, c Int32ToBoolMap) {
	c.Each(func(key int32, value bool) (continued bool) {
		t.Logf("key=%v value=%v", key, value)
		return true
	})
}

func TestInt32ToBoolMap_RemoveSome(t *testing.T) {
	m := map[int32]bool{}
	c := (*Int32ToBoolMap)(&m)
	for i := 0; i < 10; i++ {
		if i%2 == 0 {
			c.Set(int32(i), true)
		} else {
			c.Set(int32(i), false)
		}
	}

	t.Log("before remove some")
	//printInt32ToBoolMap(t, *c)
	t.Logf("%v", m)

	c.RemoveSome(func(key int32, value bool) (continued bool) {
		if key%2 == 0 {
			return true
		}
		return false
	})

	t.Log("after remove some")
	//printInt32ToBoolMap(t, *c)
	t.Logf("%v", m)
}

func Test_NilContainer(t *testing.T) {
	c := ToInt32ToBoolMap(nil)
	c.Each(func(key int32, value bool) (continued bool) {
		t.Errorf("nil container can not enter each")
		return false
	})
}
