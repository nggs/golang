package container

type Int32ToBoolMap map[int32]bool

func NewInt32ToBoolMap() (m *Int32ToBoolMap) {
	m = &Int32ToBoolMap{}
	return
}

func ToInt32ToBoolMap(m map[int32]bool) *Int32ToBoolMap {
	return (*Int32ToBoolMap)(&m)
}

func (m *Int32ToBoolMap) Get(key int32) (value bool, ok bool) {
	value, ok = (*m)[key]
	return
}

func (m *Int32ToBoolMap) Set(key int32, value bool) {
	(*m)[key] = value
}

func (m *Int32ToBoolMap) Remove(key int32) (removed bool) {
	if _, ok := (*m)[key]; ok {
		delete(*m, key)
		return true
	}
	return false
}

func (m *Int32ToBoolMap) RemoveOne(fn func(key int32, value bool) (continued bool)) {
	for key, value := range *m {
		if fn(key, value) {
			delete(*m, key)
			break
		}
	}
}

func (m *Int32ToBoolMap) RemoveSome(fn func(key int32, value bool) (continued bool)) {
	left := map[int32]bool{}
	for key, value := range *m {
		if !fn(key, value) {
			left[key] = value
		}
	}
	*m = left
}

func (m *Int32ToBoolMap) Each(f func(key int32, value bool) (continued bool)) {
	for key, value := range *m {
		if !f(key, value) {
			break
		}
	}
}

func (m Int32ToBoolMap) Size() int {
	return len(m)
}

func (m *Int32ToBoolMap) Clear() {
	*m = *NewInt32ToBoolMap()
}

func (m Int32ToBoolMap) ToMsg(n map[int32]bool) map[int32]bool {
	if m.Size() == 0 {
		return nil
	}
	n = make(map[int32]bool, m.Size())
	for k, v := range m {
		n[k] = v
	}
	return n
}

func (m Int32ToBoolMap) Clone() (n *Int32ToBoolMap) {
	if m.Size() == 0 {
		return nil
	}
	n = ToInt32ToBoolMap(make(map[int32]bool, m.Size()))
	for k, v := range m {
		(*n)[k] = v
	}
	return n
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type StringToBoolMap map[string]bool

func NewStringToBoolMap() (m *StringToBoolMap) {
	m = &StringToBoolMap{}
	return
}

func (m *StringToBoolMap) Get(key string) (value bool, ok bool) {
	value, ok = (*m)[key]
	return
}

func (m *StringToBoolMap) Set(key string, value bool) {
	(*m)[key] = value
}

func (m *StringToBoolMap) Remove(key string) (removed bool) {
	if _, ok := (*m)[key]; ok {
		delete(*m, key)
		return true
	}
	return false
}

func (m *StringToBoolMap) RemoveOne(fn func(key string, value bool) (continued bool)) {
	for key, value := range *m {
		if fn(key, value) {
			delete(*m, key)
			break
		}
	}
}

func (m *StringToBoolMap) RemoveSome(fn func(key string, value bool) (continued bool)) {
	left := map[string]bool{}
	for key, value := range *m {
		if !fn(key, value) {
			left[key] = value
		}
	}
	*m = left
}

func (m *StringToBoolMap) Each(f func(key string, value bool) (continued bool)) {
	for key, value := range *m {
		if !f(key, value) {
			break
		}
	}
}

func (m StringToBoolMap) Size() int {
	return len(m)
}

func (m *StringToBoolMap) Clear() {
	*m = *NewStringToBoolMap()
}

func (m StringToBoolMap) ToMsg(n map[string]bool) map[string]bool {
	if m.Size() == 0 {
		return nil
	}
	n = make(map[string]bool, m.Size())
	for k, v := range m {
		n[k] = v
	}
	return n
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type Int32ToInt32Map map[int32]int32

func NewInt32ToInt32Map() (m *Int32ToInt32Map) {
	m = &Int32ToInt32Map{}
	return
}

func ToInt32ToInt32Map(m map[int32]int32) *Int32ToInt32Map {
	return (*Int32ToInt32Map)(&m)
}

func (m *Int32ToInt32Map) Get(key int32) (value int32, ok bool) {
	value, ok = (*m)[key]
	return
}

func (m *Int32ToInt32Map) Set(key int32, value int32) {
	(*m)[key] = value
}

func (m *Int32ToInt32Map) Remove(key int32) (removed bool) {
	if _, ok := (*m)[key]; ok {
		delete(*m, key)
		return true
	}
	return false
}

func (m *Int32ToInt32Map) RemoveOne(fn func(key int32, value int32) (continued bool)) {
	for key, value := range *m {
		if fn(key, value) {
			delete(*m, key)
			break
		}
	}
}

func (m *Int32ToInt32Map) RemoveSome(fn func(key int32, value int32) (continued bool)) {
	left := map[int32]int32{}
	for key, value := range *m {
		if !fn(key, value) {
			left[key] = value
		}
	}
	*m = left
}

func (m *Int32ToInt32Map) Each(f func(key int32, value int32) (continued bool)) {
	for key, value := range *m {
		if !f(key, value) {
			break
		}
	}
}

func (m Int32ToInt32Map) Size() int {
	return len(m)
}

func (m *Int32ToInt32Map) Clear() {
	*m = *NewInt32ToInt32Map()
}

func (m Int32ToInt32Map) ToMsg(n map[int32]int32) map[int32]int32 {
	if m.Size() == 0 {
		return nil
	}
	n = make(map[int32]int32, m.Size())
	for k, v := range m {
		n[k] = v
	}
	return n
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type Int64ToInt32Map map[int64]int32

func NewInt64ToInt32Map() (m *Int64ToInt32Map) {
	m = &Int64ToInt32Map{}
	return
}

func ToInt64ToInt32Map(m map[int64]int32) *Int64ToInt32Map {
	return (*Int64ToInt32Map)(&m)
}

func (m *Int64ToInt32Map) Get(key int64) (value int32, ok bool) {
	value, ok = (*m)[key]
	return
}

func (m *Int64ToInt32Map) Set(key int64, value int32) {
	(*m)[key] = value
}

func (m *Int64ToInt32Map) Remove(key int64) (removed bool) {
	if _, ok := (*m)[key]; ok {
		delete(*m, key)
		return true
	}
	return false
}

func (m *Int64ToInt32Map) RemoveOne(fn func(key int64, value int32) (continued bool)) {
	for key, value := range *m {
		if fn(key, value) {
			delete(*m, key)
			break
		}
	}
}

func (m *Int64ToInt32Map) RemoveSome(fn func(int64, int32) (continued bool)) {
	left := map[int64]int32{}
	for key, value := range *m {
		if !fn(key, value) {
			left[key] = value
		}
	}
	*m = left
}

func (m *Int64ToInt32Map) Each(f func(key int64, value int32) (continued bool)) {
	for key, value := range *m {
		if !f(key, value) {
			break
		}
	}
}

func (m Int64ToInt32Map) Size() int {
	return len(m)
}

func (m *Int64ToInt32Map) Clear() {
	*m = *NewInt64ToInt32Map()
}

func (m Int64ToInt32Map) ToMsg(n map[int64]int32) map[int64]int32 {
	if m.Size() == 0 {
		return nil
	}
	n = make(map[int64]int32, m.Size())
	for k, v := range m {
		n[k] = v
	}
	return n
}

func (m Int64ToInt32Map) Clone() (n *Int64ToInt32Map) {
	if m.Size() == 0 {
		return nil
	}
	n = ToInt64ToInt32Map(make(map[int64]int32, m.Size()))
	for k, v := range m {
		(*n)[k] = v
	}
	return n
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type Int64ToInt64Map map[int64]int64

func NewInt64ToInt64Map() (m *Int64ToInt64Map) {
	m = &Int64ToInt64Map{}
	return
}

func (m *Int64ToInt64Map) Get(key int64) (value int64, ok bool) {
	value, ok = (*m)[key]
	return
}

func (m *Int64ToInt64Map) Set(key int64, value int64) {
	(*m)[key] = value
}

func (m *Int64ToInt64Map) Remove(key int64) (removed bool) {
	if _, ok := (*m)[key]; ok {
		delete(*m, key)
		return true
	}
	return false
}

func (m *Int64ToInt64Map) RemoveOne(fn func(key int64, value int64) (continued bool)) {
	for key, value := range *m {
		if fn(key, value) {
			delete(*m, key)
			break
		}
	}
}

func (m *Int64ToInt64Map) RemoveSome(fn func(key int64, value int64) (continued bool)) {
	left := map[int64]int64{}
	for key, value := range *m {
		if !fn(key, value) {
			left[key] = value
		}
	}
	*m = left
}

func (m *Int64ToInt64Map) Each(f func(key int64, value int64) (continued bool)) {
	for key, value := range *m {
		if !f(key, value) {
			break
		}
	}
}

func (m Int64ToInt64Map) Size() int {
	return len(m)
}

func (m *Int64ToInt64Map) Clear() {
	*m = *NewInt64ToInt64Map()
}

func (m Int64ToInt64Map) ToMsg(n map[int64]int64) map[int64]int64 {
	if m.Size() == 0 {
		return nil
	}
	n = make(map[int64]int64, m.Size())
	for k, v := range m {
		n[k] = v
	}
	return n
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type Int32ToInt64Map map[int32]int64

func NewInt32ToInt64Map() (m *Int32ToInt64Map) {
	m = &Int32ToInt64Map{}
	return
}

func (m *Int32ToInt64Map) Get(key int32) (value int64, ok bool) {
	value, ok = (*m)[key]
	return
}

func (m *Int32ToInt64Map) Set(key int32, value int64) {
	(*m)[key] = value
}

func (m *Int32ToInt64Map) Remove(key int32) (removed bool) {
	if _, ok := (*m)[key]; ok {
		delete(*m, key)
		return true
	}
	return false
}

func (m *Int32ToInt64Map) RemoveOne(fn func(key int32, value int64) (continued bool)) {
	for key, value := range *m {
		if fn(key, value) {
			delete(*m, key)
			break
		}
	}
}

func (m *Int32ToInt64Map) RemoveSome(fn func(key int32, value int64) (continued bool)) {
	left := map[int32]int64{}
	for key, value := range *m {
		if !fn(key, value) {
			left[key] = value
		}
	}
	*m = left
}

func (m *Int32ToInt64Map) Each(f func(key int32, value int64) (continued bool)) {
	for key, value := range *m {
		if !f(key, value) {
			break
		}
	}
}

func (m Int32ToInt64Map) Size() int {
	return len(m)
}

func (m *Int32ToInt64Map) Clear() {
	*m = *NewInt32ToInt64Map()
}

func (m Int32ToInt64Map) ToMsg(n map[int32]int64) map[int32]int64 {
	if m.Size() == 0 {
		return nil
	}
	n = make(map[int32]int64, m.Size())
	for k, v := range m {
		n[k] = v
	}
	return n
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type StringToInt32Map map[string]int32

func NewStringToInt32Map() (m *StringToInt32Map) {
	m = &StringToInt32Map{}
	return
}

func ToStringToInt32Map(m map[string]int32) *StringToInt32Map {
	return (*StringToInt32Map)(&m)
}

func (m *StringToInt32Map) Get(key string) (value int32, ok bool) {
	value, ok = (*m)[key]
	return
}

func (m *StringToInt32Map) Set(key string, value int32) {
	(*m)[key] = value
}

func (m *StringToInt32Map) Remove(key string) (removed bool) {
	if _, ok := (*m)[key]; ok {
		delete(*m, key)
		return true
	}
	return false
}

func (m *StringToInt32Map) RemoveOne(fn func(key string, value int32) (continued bool)) {
	for key, value := range *m {
		if fn(key, value) {
			delete(*m, key)
			break
		}
	}
}

func (m *StringToInt32Map) RemoveSome(fn func(key string, value int32) (continued bool)) {
	left := map[string]int32{}
	for key, value := range *m {
		if !fn(key, value) {
			left[key] = value
		}
	}
	*m = left
}

func (m *StringToInt32Map) Each(f func(key string, value int32) (continued bool)) {
	for key, value := range *m {
		if !f(key, value) {
			break
		}
	}
}

func (m StringToInt32Map) Size() int {
	return len(m)
}

func (m *StringToInt32Map) Clear() {
	*m = *NewStringToInt32Map()
}

func (m StringToInt32Map) ToMsg(n map[string]int32) map[string]int32 {
	if m.Size() == 0 {
		return nil
	}
	n = make(map[string]int32, m.Size())
	for k, v := range m {
		n[k] = v
	}
	return n
}

////////////////////////////////////////////////////////////////////////////////////////////
type StringToStringMap map[string]string

func NewStringToStringMap() (m *StringToStringMap) {
	m = &StringToStringMap{}
	return
}

func (m *StringToStringMap) Get(key string) (value string, ok bool) {
	value, ok = (*m)[key]
	return
}

func (m *StringToStringMap) Set(key string, value string) {
	(*m)[key] = value
}

func (m *StringToStringMap) Remove(key string) (removed bool) {
	if _, ok := (*m)[key]; ok {
		delete(*m, key)
		return true
	}
	return false
}

func (m *StringToStringMap) RemoveOne(fn func(key string, value string) (continued bool)) {
	for key, value := range *m {
		if fn(key, value) {
			delete(*m, key)
			break
		}
	}
}

func (m *StringToStringMap) RemoveSome(fn func(key string, value string) (continued bool)) {
	left := map[string]string{}
	for key, value := range *m {
		if !fn(key, value) {
			left[key] = value
		}
	}
	*m = left
}

func (m *StringToStringMap) Each(f func(key string, value string) (continued bool)) {
	for key, value := range *m {
		if !f(key, value) {
			break
		}
	}
}

func (m StringToStringMap) Size() int {
	return len(m)
}

func (m *StringToStringMap) Clear() {
	*m = *NewStringToStringMap()
}

func (m StringToStringMap) ToMsg(n map[string]string) map[string]string {
	if m.Size() == 0 {
		return nil
	}
	n = make(map[string]string, m.Size())
	for k, v := range m {
		n[k] = v
	}
	return n
}

////////////////////////////////////////////////////////////////////////////////////////////
type Int32ToStringMap map[int32]string

func NewInt32ToStringMap() (m *Int32ToStringMap) {
	m = &Int32ToStringMap{}
	return
}

func (m *Int32ToStringMap) Get(key int32) (value string, ok bool) {
	value, ok = (*m)[key]
	return
}

func (m *Int32ToStringMap) Set(key int32, value string) {
	(*m)[key] = value
}

func (m *Int32ToStringMap) Remove(key int32) (removed bool) {
	if _, ok := (*m)[key]; ok {
		delete(*m, key)
		return true
	}
	return false
}

func (m *Int32ToStringMap) RemoveOne(fn func(key int32, value string) (continued bool)) {
	for key, value := range *m {
		if fn(key, value) {
			delete(*m, key)
			break
		}
	}
}

func (m *Int32ToStringMap) ResetOne(fn func(key int32, value string) (continued bool)) {
	for key, value := range *m {
		if fn(key, value) {
			(*m)[key] = ""
			break
		}
	}
}

func (m *Int32ToStringMap) RemoveSome(fn func(key int32, value string) (continued bool)) {
	left := map[int32]string{}
	for key, value := range *m {
		if !fn(key, value) {
			left[key] = value
		}
	}
	*m = left
}

func (m *Int32ToStringMap) Each(f func(key int32, value string) (continued bool)) {
	for key, value := range *m {
		if !f(key, value) {
			break
		}
	}
}

func (m Int32ToStringMap) Size() int {
	return len(m)
}

func (m *Int32ToStringMap) Clear() {
	*m = *NewInt32ToStringMap()
}

func (m Int32ToStringMap) ToMsg(n map[int32]string) map[int32]string {
	if m.Size() == 0 {
		return nil
	}
	n = make(map[int32]string, m.Size())
	for k, v := range m {
		n[k] = v
	}
	return n
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type StringToInt64Map map[string]int64

func NewStringToInt64Map() (m *StringToInt64Map) {
	m = &StringToInt64Map{}
	return
}

func (m *StringToInt64Map) Get(key string) (value int64, ok bool) {
	value, ok = (*m)[key]
	return
}

func (m *StringToInt64Map) Set(key string, value int64) {
	(*m)[key] = value
}

func (m *StringToInt64Map) Remove(key string) (removed bool) {
	if _, ok := (*m)[key]; ok {
		delete(*m, key)
		return true
	}
	return false
}

func (m *StringToInt64Map) RemoveOne(fn func(key string, value int64) (continued bool)) {
	for key, value := range *m {
		if fn(key, value) {
			delete(*m, key)
			break
		}
	}
}

func (m *StringToInt64Map) RemoveSome(fn func(key string, value int64) (continued bool)) {
	left := map[string]int64{}
	for key, value := range *m {
		if !fn(key, value) {
			left[key] = value
		}
	}
	*m = left
}

func (m *StringToInt64Map) Each(f func(key string, value int64) (continued bool)) {
	for key, value := range *m {
		if !f(key, value) {
			break
		}
	}
}

func (m StringToInt64Map) Size() int {
	return len(m)
}

func (m *StringToInt64Map) Clear() {
	*m = *NewStringToInt64Map()
}

func (m StringToInt64Map) ToMsg(n map[string]int64) map[string]int64 {
	if m.Size() == 0 {
		return nil
	}
	n = make(map[string]int64, m.Size())
	for k, v := range m {
		n[k] = v
	}
	return n
}
