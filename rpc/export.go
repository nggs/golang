package rpc

import (
	"github.com/asynkron/protoactor-go/actor"
	proto "github.com/gogo/protobuf/proto"
)

const (
	PH = 0
)

type MessageID = uint32

type IMessage interface {
	proto.Message
	Size() int
	Marshal() (dAtA []byte, err error)
	Unmarshal([]byte) error
	RPC()
	MessageID() MessageID
	MessageName() string
	ResetEx()
	ResponseMessageID() MessageID
}

type MessageProducer func() IMessage
type MessageRecycler func(IMessage)

type IMessageFactoryManager interface {
	Register(iMsg IMessage, producer MessageProducer, recycler MessageRecycler)
	Produce(id MessageID) (IMessage, error)
	Recycle(iMsg IMessage) error
}

type MessageHandler func(sender *actor.PID, iRecv IMessage, iSend IMessage, ctx actor.Context, args ...interface{})

type IMessageDispatcher interface {
	Register(id MessageID, handler MessageHandler) (err error)
	Dispatch(iProtocol IProtocol, sender *actor.PID, iRecv IMessage, ctx actor.Context, args ...interface{}) (iSend IMessage, err error)
}

type IProtocol interface {
	IMessageFactoryManager
}
