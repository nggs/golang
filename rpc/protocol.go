package rpc

import (
	nproto "nggs/network/protocol"
)

type protocol struct {
	IMessageFactoryManager
	allocator nproto.IAllocator
}

func Protocol(allocator nproto.IAllocator) IProtocol {
	proto := &protocol{
		IMessageFactoryManager: newMessageFactoryManager(),
		allocator:              &nproto.NonAllocator{},
	}
	if allocator != nil {
		proto.allocator = allocator
	}
	return proto
}
