package rpc

import (
	"fmt"
	"log"
)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type messageFactory struct {
	id       MessageID
	producer MessageProducer
	recycler MessageRecycler
}

func newMessageFactory(id MessageID, producer MessageProducer, recycler MessageRecycler) *messageFactory {
	return &messageFactory{
		id:       id,
		producer: producer,
		recycler: recycler,
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type messageFactoryManager struct {
	factories map[MessageID]*messageFactory
}

func newMessageFactoryManager() IMessageFactoryManager {
	m := &messageFactoryManager{
		factories: make(map[MessageID]*messageFactory),
	}
	return m
}

func (m *messageFactoryManager) Register(iMsg IMessage, producer MessageProducer, recycler MessageRecycler) {
	if producer == nil {
		log.Panicf("register message factory fail, producer == nil, id=[%v]", iMsg.MessageID())
	}

	if recycler == nil {
		log.Panicf("register message factory fail, recycler == nil, id=[%v]", iMsg.MessageID())
	}

	if f, ok := m.factories[iMsg.MessageID()]; ok {
		log.Panicf("duplicate message factory, id=[%v], factory=[%+v]", iMsg.MessageID(), f)
	}

	m.factories[iMsg.MessageID()] = newMessageFactory(iMsg.MessageID(), producer, recycler)
}

func (m *messageFactoryManager) Produce(id MessageID) (IMessage, error) {
	if factory := m.factories[id]; factory != nil {
		return factory.producer(), nil
	}
	return nil, fmt.Errorf("unsupported message, id=[%v]", id)
}

func (m *messageFactoryManager) Recycle(iMsg IMessage) error {
	if factory := m.factories[iMsg.MessageID()]; factory != nil {
		factory.recycler(iMsg)
		return nil
	}
	return fmt.Errorf("unsupported message, id=[%v]", iMsg.MessageID())
}
