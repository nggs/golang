package rpc

import (
	"github.com/asynkron/protoactor-go/actor"

	"nggs/rpc"
)

func (p *PID) ToReal() *actor.PID {
	if p == nil {
		return nil
	}
	rp := actor.Get_PID()
	rp.Address = p.Address
	rp.Id = p.Id
	return rp
}

func FromRealPID(rp *actor.PID) *PID {
	if rp == nil {
		return nil
	}
	p := Get_PID()
	p.Address = rp.Address
	p.Id = rp.Id
	return p
}

var Protocol = rpc.Protocol(nil)
