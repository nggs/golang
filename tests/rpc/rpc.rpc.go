// Code generated by protoc-gen-rpc-go. DO NOT EDIT IT!!!
// source: rpc/rpc.proto

package rpc

import (
	fmt "fmt"
	proto "github.com/gogo/protobuf/proto"
	math "math"
	model "model"
	msg "msg"
	actor "nggs/actor"
	rpc "nggs/rpc"
	sync "sync"
	time "time"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

var _ = model.PH
var _ = msg.PH

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// message [PID] begin
func New_PID() *PID {
	m := &PID{}
	return m
}

func (m *PID) ResetEx() {

	m.Address = ""

	m.Id = ""

}

func (m *PID) Clone() *PID {
	if m == nil {
		return nil
	}
	n := Get_PID()
	deriveDeepCopyPID(n, m)
	return n
}

var g_PID_Pool = sync.Pool{}

func Get_PID() *PID {
	m, ok := g_PID_Pool.Get().(*PID)
	if !ok {
		m = New_PID()
	} else {
		if m == nil {
			m = New_PID()
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_PID(i interface{}) {
	if m, ok := i.(*PID); ok && m != nil {
		g_PID_Pool.Put(i)
	}
}

// message [PID] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// message [C2S_Ping] begin
func New_C2S_Ping() *C2S_Ping {
	m := &C2S_Ping{}
	return m
}

func (m *C2S_Ping) ResetEx() {

	if m.PID != nil {

		Put_PID(m.PID)

		m.PID = nil
	}

}

func (m *C2S_Ping) Clone() *C2S_Ping {
	if m == nil {
		return nil
	}
	n := Get_C2S_Ping()
	deriveDeepCopyC2S_Ping(n, m)
	return n
}

func (C2S_Ping) RPC() {
}

func (C2S_Ping) MessageID() rpc.MessageID {
	return 1
}

func C2S_Ping_MessageID() rpc.MessageID {
	return 1
}

func (C2S_Ping) MessageName() string {
	return "C2S_Ping"
}

func C2S_Ping_MessageName() string {
	return "C2S_Ping"
}

func (C2S_Ping) ResponseMessageID() rpc.MessageID {

	return S2C_Pong_MessageID()

}

func C2S_Ping_ResponseMessageID() rpc.MessageID {

	return S2C_Pong_MessageID()

}

func Request_S2C_Pong(pid *PID, send *C2S_Ping) (*S2C_Pong, error) {
	return Request_S2C_Pong_T(pid, send, 5000*time.Millisecond)
}

func Request_S2C_Pong_T(pid *PID, send *C2S_Ping, timeout time.Duration) (*S2C_Pong, error) {
	if pid == nil {
		return nil, fmt.Errorf("pid is nil")
	}
	f := actor.RootContext().RequestFuture(pid.ToReal(), send, timeout)
	iRecv, err := f.Result()
	if err != nil {
		return nil, err
	}
	recv, ok := iRecv.(*S2C_Pong)
	if !ok {
		return nil, fmt.Errorf("recv %#v is not S2C_Pong", recv)
	}
	return recv, nil
}

var g_C2S_Ping_Pool = sync.Pool{}

func Get_C2S_Ping() *C2S_Ping {
	m, ok := g_C2S_Ping_Pool.Get().(*C2S_Ping)
	if !ok {
		m = New_C2S_Ping()
	} else {
		if m == nil {
			m = New_C2S_Ping()
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_C2S_Ping(i interface{}) {
	if m, ok := i.(*C2S_Ping); ok && m != nil {
		g_C2S_Ping_Pool.Put(i)
	}
}

func init() {
	Protocol.Register(
		&C2S_Ping{},
		func() rpc.IMessage { return Get_C2S_Ping() },
		func(msg rpc.IMessage) { Put_C2S_Ping(msg) },
	)
}

// message [C2S_Ping] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// message [S2C_Pong] begin
func New_S2C_Pong() *S2C_Pong {
	m := &S2C_Pong{}
	return m
}

func (m *S2C_Pong) ResetEx() {

	if m.PID != nil {

		Put_PID(m.PID)

		m.PID = nil
	}

}

func (m *S2C_Pong) Clone() *S2C_Pong {
	if m == nil {
		return nil
	}
	n := Get_S2C_Pong()
	deriveDeepCopyS2C_Pong(n, m)
	return n
}

func (S2C_Pong) RPC() {
}

func (S2C_Pong) MessageID() rpc.MessageID {
	return 2
}

func S2C_Pong_MessageID() rpc.MessageID {
	return 2
}

func (S2C_Pong) MessageName() string {
	return "S2C_Pong"
}

func S2C_Pong_MessageName() string {
	return "S2C_Pong"
}

func (S2C_Pong) ResponseMessageID() rpc.MessageID {

	return 0

}

func S2C_Pong_ResponseMessageID() rpc.MessageID {

	return 0

}

var g_S2C_Pong_Pool = sync.Pool{}

func Get_S2C_Pong() *S2C_Pong {
	m, ok := g_S2C_Pong_Pool.Get().(*S2C_Pong)
	if !ok {
		m = New_S2C_Pong()
	} else {
		if m == nil {
			m = New_S2C_Pong()
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_S2C_Pong(i interface{}) {
	if m, ok := i.(*S2C_Pong); ok && m != nil {
		g_S2C_Pong_Pool.Put(i)
	}
}

func init() {
	Protocol.Register(
		&S2C_Pong{},
		func() rpc.IMessage { return Get_S2C_Pong() },
		func(msg rpc.IMessage) { Put_S2C_Pong(msg) },
	)
}

// message [S2C_Pong] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// message [C2S_RecvMsgFromClient] begin
func New_C2S_RecvMsgFromClient() *C2S_RecvMsgFromClient {
	m := &C2S_RecvMsgFromClient{}
	return m
}

func (m *C2S_RecvMsgFromClient) ResetEx() {

	m.Data = nil

}

func (m *C2S_RecvMsgFromClient) Clone() *C2S_RecvMsgFromClient {
	if m == nil {
		return nil
	}
	n := Get_C2S_RecvMsgFromClient()
	deriveDeepCopyC2S_RecvMsgFromClient(n, m)
	return n
}

func (C2S_RecvMsgFromClient) RPC() {
}

func (C2S_RecvMsgFromClient) MessageID() rpc.MessageID {
	return 3
}

func C2S_RecvMsgFromClient_MessageID() rpc.MessageID {
	return 3
}

func (C2S_RecvMsgFromClient) MessageName() string {
	return "C2S_RecvMsgFromClient"
}

func C2S_RecvMsgFromClient_MessageName() string {
	return "C2S_RecvMsgFromClient"
}

func (C2S_RecvMsgFromClient) ResponseMessageID() rpc.MessageID {

	return 0

}

func C2S_RecvMsgFromClient_ResponseMessageID() rpc.MessageID {

	return 0

}

var g_C2S_RecvMsgFromClient_Pool = sync.Pool{}

func Get_C2S_RecvMsgFromClient() *C2S_RecvMsgFromClient {
	m, ok := g_C2S_RecvMsgFromClient_Pool.Get().(*C2S_RecvMsgFromClient)
	if !ok {
		m = New_C2S_RecvMsgFromClient()
	} else {
		if m == nil {
			m = New_C2S_RecvMsgFromClient()
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_C2S_RecvMsgFromClient(i interface{}) {
	if m, ok := i.(*C2S_RecvMsgFromClient); ok && m != nil {
		g_C2S_RecvMsgFromClient_Pool.Put(i)
	}
}

func init() {
	Protocol.Register(
		&C2S_RecvMsgFromClient{},
		func() rpc.IMessage { return Get_C2S_RecvMsgFromClient() },
		func(msg rpc.IMessage) { Put_C2S_RecvMsgFromClient(msg) },
	)
}

// message [C2S_RecvMsgFromClient] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// message [C2S_SendMsgToClient] begin
func New_C2S_SendMsgToClient() *C2S_SendMsgToClient {
	m := &C2S_SendMsgToClient{}
	return m
}

func (m *C2S_SendMsgToClient) ResetEx() {

	m.Data = nil

}

func (m *C2S_SendMsgToClient) Clone() *C2S_SendMsgToClient {
	if m == nil {
		return nil
	}
	n := Get_C2S_SendMsgToClient()
	deriveDeepCopyC2S_SendMsgToClient(n, m)
	return n
}

func (C2S_SendMsgToClient) RPC() {
}

func (C2S_SendMsgToClient) MessageID() rpc.MessageID {
	return 5
}

func C2S_SendMsgToClient_MessageID() rpc.MessageID {
	return 5
}

func (C2S_SendMsgToClient) MessageName() string {
	return "C2S_SendMsgToClient"
}

func C2S_SendMsgToClient_MessageName() string {
	return "C2S_SendMsgToClient"
}

func (C2S_SendMsgToClient) ResponseMessageID() rpc.MessageID {

	return 0

}

func C2S_SendMsgToClient_ResponseMessageID() rpc.MessageID {

	return 0

}

var g_C2S_SendMsgToClient_Pool = sync.Pool{}

func Get_C2S_SendMsgToClient() *C2S_SendMsgToClient {
	m, ok := g_C2S_SendMsgToClient_Pool.Get().(*C2S_SendMsgToClient)
	if !ok {
		m = New_C2S_SendMsgToClient()
	} else {
		if m == nil {
			m = New_C2S_SendMsgToClient()
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_C2S_SendMsgToClient(i interface{}) {
	if m, ok := i.(*C2S_SendMsgToClient); ok && m != nil {
		g_C2S_SendMsgToClient_Pool.Put(i)
	}
}

func init() {
	Protocol.Register(
		&C2S_SendMsgToClient{},
		func() rpc.IMessage { return Get_C2S_SendMsgToClient() },
		func(msg rpc.IMessage) { Put_C2S_SendMsgToClient(msg) },
	)
}

// message [C2S_SendMsgToClient] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// message [C2S_BroadcastMsgToClient] begin
func New_C2S_BroadcastMsgToClient() *C2S_BroadcastMsgToClient {
	m := &C2S_BroadcastMsgToClient{}
	return m
}

func (m *C2S_BroadcastMsgToClient) ResetEx() {

	m.Include = nil

	m.Exclude = nil

	m.Data = nil

}

func (m *C2S_BroadcastMsgToClient) Clone() *C2S_BroadcastMsgToClient {
	if m == nil {
		return nil
	}
	n := Get_C2S_BroadcastMsgToClient()
	deriveDeepCopyC2S_BroadcastMsgToClient(n, m)
	return n
}

func (C2S_BroadcastMsgToClient) RPC() {
}

func (C2S_BroadcastMsgToClient) MessageID() rpc.MessageID {
	return 7
}

func C2S_BroadcastMsgToClient_MessageID() rpc.MessageID {
	return 7
}

func (C2S_BroadcastMsgToClient) MessageName() string {
	return "C2S_BroadcastMsgToClient"
}

func C2S_BroadcastMsgToClient_MessageName() string {
	return "C2S_BroadcastMsgToClient"
}

func (C2S_BroadcastMsgToClient) ResponseMessageID() rpc.MessageID {

	return 0

}

func C2S_BroadcastMsgToClient_ResponseMessageID() rpc.MessageID {

	return 0

}

var g_C2S_BroadcastMsgToClient_Pool = sync.Pool{}

func Get_C2S_BroadcastMsgToClient() *C2S_BroadcastMsgToClient {
	m, ok := g_C2S_BroadcastMsgToClient_Pool.Get().(*C2S_BroadcastMsgToClient)
	if !ok {
		m = New_C2S_BroadcastMsgToClient()
	} else {
		if m == nil {
			m = New_C2S_BroadcastMsgToClient()
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_C2S_BroadcastMsgToClient(i interface{}) {
	if m, ok := i.(*C2S_BroadcastMsgToClient); ok && m != nil {
		g_C2S_BroadcastMsgToClient_Pool.Put(i)
	}
}

func init() {
	Protocol.Register(
		&C2S_BroadcastMsgToClient{},
		func() rpc.IMessage { return Get_C2S_BroadcastMsgToClient() },
		func(msg rpc.IMessage) { Put_C2S_BroadcastMsgToClient(msg) },
	)
}

// message [C2S_BroadcastMsgToClient] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
