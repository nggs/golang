module model

go 1.18

require (
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/gogo/protobuf v1.3.1
	msg v0.0.0
	nggs v0.0.0
)

replace (
	github.com/asynkron/protoactor-go => gitee.com/lwj8507/protoactor-go v0.0.2-0.20220417043855-5f1025f7ddd1
	github.com/coreos/bbolt => go.etcd.io/bbolt v1.3.4
	google.golang.org/grpc => google.golang.org/grpc v1.26.0
	msg => ../msg
	nggs => ../..
)
