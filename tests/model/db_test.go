package model

import "testing"

func Test_(t *testing.T) {
	var s *TestSlice
	s.RemoveOne(func(index int, element *Test) (removed bool) {
		return true
	})
	s.RemoveSome(func(index int, element *Test) (removed bool) {
		return true
	})
	s.Each(func(index int, element *Test) (continued bool) {
		return true
	})
	s.Size()
	s.Clone()
	s.Clear()
	s.Add()

	var m *Int32ToTestMap
	m.Get(1)
	m.Set(1, nil)
	m.Add(1)
	m.Remove(1)
	m.RemoveOne(func(key int32, value *Test) (removed bool) {
		return true
	})
	m.RemoveSome(func(key int32, value *Test) (removed bool) {
		return true
	})
	m.Each(func(key int32, value *Test) (continued bool) {
		return true
	})
	m.Size()
	m.Clone()
	m.Clear()
}

//const (
//	url = "mongodb://admin:123456@127.0.0.1:27017/admin"
//)
//
//func TestUser(t *testing.T) {
//	if err := SC.Init(url, 1, "db_test"); err != nil {
//		t.Error(err)
//		return
//	}
//	defer SC.Release()
//
//	session := SC.GetSession()
//	defer SC.PutSession(session)
//
//	newUser, err := SC.CreateUser(1, 1, "test.proto", 1)
//	if err != nil {
//		t.Error(err)
//		return
//	}
//
//	newUser.IMap = map[int32]int32{
//		1: 1,
//		2: 2,
//		3: 3,
//	}
//
//	newUser.TestMap = map[int32]*Test{
//		1: {
//			I32: 1,
//		},
//		2: {
//			I32: 2,
//		},
//	}
//
//	if err := newUser.Insert(session, SC.DBName()); err != nil {
//		t.Error(err)
//		return
//	}
//
//	u, err := SC.FindOne_User(session, bson.M{"_id": newUser.ID})
//	if err != nil {
//		t.Error(err)
//	}
//
//	t.Logf("%v\n", u)
//
//	_, err = SC.FindOne_User(session, bson.M{"Name": "test.proto"})
//	if err != nil {
//		t.Error(err)
//	}
//
//	some, err := SC.FindSome_User(session, bson.M{"Name": "test.proto"})
//	if err != nil || len(some) != 1 {
//		t.Error(err)
//	}
//
//	err = newUser.RemoveByID(session, SC.DBName())
//	if err != nil {
//		t.Error(err)
//	}
//}
//
//func TestClone_User_Slice(t *testing.T) {
//	var dst = []*User{
//		{ID: 1},
//		{ID: 2},
//		{ID: 3},
//		{ID: 4},
//	}
//
//	t.Logf("before, dst=\n")
//	for _, i := range dst {
//		t.Log(i)
//	}
//
//	var src = []*User{
//		{ID: 2},
//		{ID: 3},
//		{ID: 4},
//		{ID: 5},
//		{ID: 6},
//	}
//
//	t.Logf("src=\n")
//	for _, i := range src {
//		t.Log(i)
//	}
//
//	t.Logf("dst=%#v\nsrc=%#v\n", dst, src)
//
//	dst = Clone_User_Slice(dst, src)
//
//	t.Logf("after clone, dst=\n")
//	for _, i := range dst {
//		t.Log(i)
//	}
//
//	t.Logf("src=\n")
//	for _, i := range src {
//		t.Log(i)
//	}
//
//	t.Logf("dst=%#v\nsrc=%#v\n", dst, src)
//}
