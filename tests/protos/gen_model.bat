@set WORK_DIR=%1

@set TESTS_DIR=%WORK_DIR%tests

@set BIN_DIR=%WORK_DIR%bin

@set TOOLS_DIR=%BIN_DIR%\tools

@set PROTOC=%TOOLS_DIR%\protoc\3.15.6-win64\bin\protoc.exe

@set PBPLUGIN_DIR=%TOOLS_DIR%\pbplugin

@setlocal enabledelayedexpansion enableextensions
@set PROTO_FILES=
@for %%x in (%TESTS_DIR%\protos\model\*.proto) do @set PROTO_FILES=!PROTO_FILES! %%x
@set PROTO_FILES=%PROTO_FILES:~1%

%PROTOC% --plugin=protoc-gen-gogoslick=%PBPLUGIN_DIR%\protoc-gen-gogoslick.exe -I=%GOPATH%\src -I=%TESTS_DIR%\protos --gogoslick_out=%TESTS_DIR% %PROTO_FILES%
%PROTOC% --plugin=protoc-gen-mgo-go=%PBPLUGIN_DIR%\protoc-gen-mgo-go.exe -I=%GOPATH%\src -I=%TESTS_DIR%\protos --mgo-go_out=%TESTS_DIR% --mgo-go_opt=tpl=%TESTS_DIR%\protos\model.gohtml %PROTO_FILES%

@set PB_FILES=
@for %%x in (%TESTS_DIR%\model\*.pb.go) do %PBPLUGIN_DIR%\protoc-go-inject-tag.exe -XXX_skip=yaml,xml -input=%%x
