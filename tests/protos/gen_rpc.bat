@set WORK_DIR=%1

@set TESTS_DIR=%WORK_DIR%tests

@set BIN_DIR=%WORK_DIR%bin

@set TOOLS_DIR=%BIN_DIR%\tools

@set PROTOC=%TOOLS_DIR%\protoc\3.15.6-win64\bin\protoc.exe

@set PBPLUGIN_DIR=%TOOLS_DIR%\pbplugin

@setlocal enabledelayedexpansion enableextensions
@set PROTO_FILES=
@for %%x in (%TESTS_DIR%\protos\rpc\*.proto) do @set PROTO_FILES=!PROTO_FILES! %%x
@set PROTO_FILES=%PROTO_FILES:~1%

%PROTOC% --plugin=protoc-gen-gogoslick=%PBPLUGIN_DIR%\protoc-gen-gogoslick.exe -I=%GOPATH%\src -I=%TESTS_DIR%\protos --gogoslick_out=%TESTS_DIR% %PROTO_FILES%
%PROTOC% --plugin=protoc-gen-rpc-go=%PBPLUGIN_DIR%\protoc-gen-rpc-go.exe -I=%GOPATH%\src -I=%TESTS_DIR%\protos --rpc-go_out=%TESTS_DIR% --rpc-go_opt=tpl=%TESTS_DIR%\protos\rpc.gohtml %PROTO_FILES%
