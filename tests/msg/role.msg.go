// Code generated by protoc-gen-pbex-go. DO NOT EDIT IT!!!
// source: msg/role.proto

package msg

import (
	fmt "fmt"
	proto "github.com/gogo/protobuf/proto"
	math "math"
	sync "sync"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// message [Role] begin

func New_Role() *Role {
	m := &Role{}
	return m
}

func (m *Role) ResetEx() {

	m.ID = 0

	m.AccountID = 0

	m.ServerID = 0

	m.Name = ""

	m.Sex = 0

	m.CreateTime = 0

}

func (m *Role) Clone() *Role {
	if m == nil {
		return nil
	}
	n := Get_Role()
	deriveDeepCopyRole(n, m)
	return n
}

type RoleSlice []*Role

func NewRoleSlice() *RoleSlice {
	return &RoleSlice{}
}

func ToRoleSlice(s []*Role) *RoleSlice {
	return (*RoleSlice)(&s)
}

func (s *RoleSlice) Add() *Role {
	if s == nil {
		return nil
	}
	return s.AddOne(Get_Role())
}

func (s *RoleSlice) AddOne(newOne *Role) *Role {
	if s == nil {
		return nil
	}
	*s = append(*s, newOne)
	return newOne
}

func (s *RoleSlice) RemoveOne(fn func(index int, element *Role) (continued bool)) {
	if s == nil {
		return
	}
	for i, e := range *s {
		if fn(i, e) {
			*s = append((*s)[:i], (*s)[i+1:]...)
			break
		}
	}
}

func (s *RoleSlice) RemoveSome(fn func(index int, element *Role) (continued bool)) {
	if s == nil {
		return
	}
	var left []*Role
	for i, e := range *s {
		if !fn(i, e) {
			left = append(left, e)
		}
	}
	*s = left
}

func (s *RoleSlice) Each(fn func(index int, element *Role) (continued bool)) {
	if s == nil {
		return
	}
	for i, e := range *s {
		if !fn(i, e) {
			break
		}
	}
}

func (s *RoleSlice) Size() int {
	if s == nil {
		return 0
	}
	return len(*s)
}

func (s *RoleSlice) Clone() (n *RoleSlice) {
	if s == nil {
		return nil
	}
	n = ToRoleSlice(make([]*Role, s.Size()))
	for i, e := range *s {
		if e != nil {
			(*n)[i] = e.Clone()
		} else {
			(*n)[i] = nil
		}
	}
	return n
}

func (s *RoleSlice) Clear() {
	if s == nil {
		return
	}
	*s = *NewRoleSlice()
}

type Int64ToRoleMap map[int64]*Role

func ToInt64ToRoleMap(m map[int64]*Role) *Int64ToRoleMap {
	if m == nil {
		return nil
	}
	return (*Int64ToRoleMap)(&m)
}

func NewInt64ToRoleMap() (m *Int64ToRoleMap) {
	m = &Int64ToRoleMap{}
	return
}

func (m *Int64ToRoleMap) Get(key int64) (value *Role, ok bool) {
	if m == nil {
		return
	}
	value, ok = (*m)[key]
	return
}

func (m *Int64ToRoleMap) Set(key int64, value *Role) {
	if m == nil {
		return
	}
	(*m)[key] = value
}

func (m *Int64ToRoleMap) Add(key int64) (value *Role) {
	if m == nil {
		return
	}
	value = Get_Role()
	(*m)[key] = value
	return
}

func (m *Int64ToRoleMap) Remove(key int64) (removed bool) {
	if m == nil {
		return
	}
	if _, ok := (*m)[key]; ok {
		delete(*m, key)
		return true
	}
	return false
}

func (m *Int64ToRoleMap) RemoveOne(fn func(key int64, value *Role) (continued bool)) {
	if m == nil {
		return
	}
	for key, value := range *m {
		if fn(key, value) {
			delete(*m, key)
			break
		}
	}
}

func (m *Int64ToRoleMap) RemoveSome(fn func(key int64, value *Role) (continued bool)) {
	if m == nil {
		return
	}
	left := map[int64]*Role{}
	for key, value := range *m {
		if !fn(key, value) {
			left[key] = value
		}
	}
	*m = left
}

func (m *Int64ToRoleMap) Each(f func(key int64, value *Role) (continued bool)) {
	if m == nil {
		return
	}
	for key, value := range *m {
		if !f(key, value) {
			break
		}
	}
}

func (m *Int64ToRoleMap) Size() int {
	if m == nil {
		return 0
	}
	return len(*m)
}

func (m *Int64ToRoleMap) Clone() (n *Int64ToRoleMap) {
	if m == nil {
		return nil
	}
	n = ToInt64ToRoleMap(make(map[int64]*Role, m.Size()))
	for k, v := range *m {
		if v != nil {
			(*n)[k] = v.Clone()
		} else {
			(*n)[k] = nil
		}
	}
	return n
}

func (m *Int64ToRoleMap) Clear() {
	if m == nil {
		return
	}
	for k, _ := range *m {
		delete(*m, k)
	}
}

type StringToRoleMap map[string]*Role

func ToStringToRoleMap(m map[string]*Role) *StringToRoleMap {
	if m == nil {
		return nil
	}
	return (*StringToRoleMap)(&m)
}

func NewStringToRoleMap() (m *StringToRoleMap) {
	m = &StringToRoleMap{}
	return
}

func (m *StringToRoleMap) Get(key string) (value *Role, ok bool) {
	if m == nil {
		return
	}
	value, ok = (*m)[key]
	return
}

func (m *StringToRoleMap) Set(key string, value *Role) {
	if m == nil {
		return
	}
	(*m)[key] = value
}

func (m *StringToRoleMap) Add(key string) (value *Role) {
	if m == nil {
		return
	}
	value = Get_Role()
	(*m)[key] = value
	return
}

func (m *StringToRoleMap) Remove(key string) (removed bool) {
	if m == nil {
		return
	}
	if _, ok := (*m)[key]; ok {
		delete(*m, key)
		return true
	}
	return false
}

func (m *StringToRoleMap) RemoveOne(fn func(key string, value *Role) (continued bool)) {
	if m == nil {
		return
	}
	for key, value := range *m {
		if fn(key, value) {
			delete(*m, key)
			break
		}
	}
}

func (m *StringToRoleMap) RemoveSome(fn func(key string, value *Role) (continued bool)) {
	if m == nil {
		return
	}
	left := map[string]*Role{}
	for key, value := range *m {
		if !fn(key, value) {
			left[key] = value
		}
	}
	*m = left
}

func (m *StringToRoleMap) Each(f func(key string, value *Role) (continued bool)) {
	if m == nil {
		return
	}
	for key, value := range *m {
		if !f(key, value) {
			break
		}
	}
}

func (m *StringToRoleMap) Size() int {
	if m == nil {
		return 0
	}
	return len(*m)
}

func (m *StringToRoleMap) Clone() (n *StringToRoleMap) {
	if m == nil {
		return nil
	}
	n = ToStringToRoleMap(make(map[string]*Role, m.Size()))
	for k, v := range *m {
		if v != nil {
			(*n)[k] = v.Clone()
		} else {
			(*n)[k] = nil
		}
	}
	return n
}

func (m *StringToRoleMap) Clear() {
	if m == nil {
		return
	}
	for k, _ := range *m {
		delete(*m, k)
	}
}

var g_Role_Pool = sync.Pool{}

func Get_Role() *Role {
	m, ok := g_Role_Pool.Get().(*Role)
	if !ok {
		m = New_Role()
	} else {
		if m == nil {
			m = New_Role()
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_Role(i interface{}) {
	if m, ok := i.(*Role); ok && m != nil {
		g_Role_Pool.Put(i)
	}
}

// message [Role] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
