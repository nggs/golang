package msg

import (
	"testing"
)

func Test_Container(t *testing.T) {
	var s *TestSlice
	s.RemoveOne(func(index int, element *Test) (removed bool) {
		return true
	})
	s.RemoveSome(func(index int, element *Test) (removed bool) {
		return true
	})
	s.Each(func(index int, element *Test) (continued bool) {
		return true
	})
	s.Size()
	s.Clone()
	s.Clear()
	s.Add()

	var m *Int32ToTestMap
	m.Get(1)
	m.Set(1, nil)
	m.Add(1)
	m.Remove(1)
	m.RemoveOne(func(key int32, value *Test) (removed bool) {
		return true
	})
	m.RemoveSome(func(key int32, value *Test) (removed bool) {
		return true
	})
	m.Each(func(key int32, value *Test) (continued bool) {
		return true
	})
	m.Size()
	m.Clone()
	m.Clear()
}

func Test_Protocol(t *testing.T) {
	newMsg := New_C2S_Test()
	newMsgID := newMsg.MessageID
	//t.Logf("new msg id=%d", newMsgID)

	getMsg := Get_C2S_Test()
	getMsgID := getMsg.MessageID
	//t.Logf("get msg id=%d", getMsgID)

	if newMsgID != getMsgID {
		t.Errorf("newMsgID != getMsgID")
		return
	}

	newMsg.ResetEx()
	getMsg.ResetEx()

	if newMsgID != getMsgID {
		t.Errorf("newMsgID != getMsgID after reset")
		return
	}

	send := Get_C2S_Test()
	send.MessageSeq = 111
	send.Content = "C2S_Test"
	send.I32 = 1
	send.D = 2.0
	send.B = true
	send.Bytes = []byte("byte")
	send.Test.Content = "C2S_Test.Test"
	send.Test.I32 = 2
	send.Test.D = 3
	send.Test.B = true

	data, err := Protocol.Encode(send)
	if err != nil {
		t.Errorf("encode C2S_Test fail, %s", err)
		return
	}

	iRecv, err := Protocol.Decode(data)
	if err != nil {
		t.Errorf("decode C2S_Test fail, %s", err)
		return
	}
	recv, ok := iRecv.(*C2S_Test_EX)
	if !ok || recv == nil {
		t.Errorf("iRecv != *C2S_Test_EX")
		return
	}

	if !send.Equal(recv) {
		t.Errorf("send != recv")
		return
	}
}
