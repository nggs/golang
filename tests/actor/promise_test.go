package tests

import (
	"fmt"
	"log"
	"sync"
	"testing"
	"time"

	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	nrpc "nggs/rpc"
)

type promiseSender struct {
	*nactor.Actor

	startedWg sync.WaitGroup
	stoppedWg sync.WaitGroup
}

func (s *promiseSender) OnStarted(ctx actor.Context) {
	log.Printf("%v OnStarted\n", s.PID())
}

func (s *promiseSender) OnStopping(ctx actor.Context) {
	log.Printf("%v OnStopping\n", s.PID())
}

func (s *promiseSender) OnStopped(ctx actor.Context) {
	log.Printf("%v OnStopped\n", s.PID())
}

func (s *promiseSender) OnReceiveMessage(ctx actor.Context) {
	log.Printf("%v recv: %v", s.PID(), ctx.Message())
}

func (s *promiseSender) OnRestarting(ctx actor.Context) {
	log.Printf("%v OnRestarting\n", s.PID())
}

func (s *promiseSender) OnRestarted(ctx actor.Context) {
	log.Printf("%v OnRestarted\n", s.PID())
}

func startPromiseSender() (s *promiseSender) {
	s = &promiseSender{}

	s.Actor = nactor.New(
		nactor.WithOnReceiveMessage(s.OnReceiveMessage),
		nactor.WithOnStarted(s.OnStarted),
		//nactor.WithOnStopping(s.OnStopping),
		nactor.WithOnStopped(s.OnStopped),
		nactor.WithOnRestarting(s.OnRestarting),
		nactor.WithOnRestarted(s.OnRestarted),
		nactor.WithStartedWaitGroup(&s.startedWg),
		nactor.WithStoppedWaitGroup(&s.stoppedWg),
		nactor.WithRPC(Protocol, nil, nil),
		nactor.WithPulse(3*time.Second, nil),
	)

	if err := s.Start(nil, "sender"); err != nil {
		log.Panic(err)
	}

	s.WaitForStarted()

	return s
}

func stopPromiseSender(s *promiseSender) {
	_ = s.StopAndWait()
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type promiseReceiver struct {
	*nactor.Actor

	startedWg sync.WaitGroup
	stoppedWg sync.WaitGroup
}

func (r *promiseReceiver) OnStarted(ctx actor.Context) {
	log.Printf("%v OnStarted\n", r.PID())
}

func (r *promiseReceiver) OnStopping(ctx actor.Context) {
	log.Printf("%v OnStopping\n", r.PID())
}

func (r *promiseReceiver) OnStopped(ctx actor.Context) {
	log.Printf("%v OnStopped\n", r.PID())
}

func (r *promiseReceiver) OnReceiveMessage(ctx actor.Context) {
	log.Printf("%v recv: %v", r.PID(), ctx.Message())
}

func (r *promiseReceiver) OnRestarting(ctx actor.Context) {
	log.Printf("%v OnRestarting\n", r.PID())
}

func (r *promiseReceiver) OnRestarted(ctx actor.Context) {
	log.Printf("%v OnRestarted\n", r.PID())
}

func startPromiseReceiver() *promiseReceiver {
	r := &promiseReceiver{}

	r.Actor = nactor.New(
		nactor.WithOnReceiveMessage(r.OnReceiveMessage),
		nactor.WithOnStarted(r.OnStarted),
		//nactor.WithOnStopping(r.OnStopping),
		nactor.WithOnStopped(r.OnStopped),
		nactor.WithOnRestarting(r.OnRestarting),
		nactor.WithOnRestarted(r.OnRestarted),
		nactor.WithStartedWaitGroup(&r.startedWg),
		nactor.WithStoppedWaitGroup(&r.stoppedWg),
		nactor.WithRPC(Protocol, func(sender *actor.PID, iRequestMessage nrpc.IMessage, ctx actor.Context) (args []interface{}, ok bool, err error) {
			log.Printf("before dispatch rpc")
			return nil, true, nil
		}, func(err error, sender *actor.PID, iRequestMessage nrpc.IMessage, iResponseMessage nrpc.IMessage, ctx actor.Context, args ...interface{}) {
			log.Printf("after dispatch rpc")
		}),
		nactor.WithPulse(3*time.Second, nil),
	)

	if err := r.RegisterRPCHandler(C2S_Ping_MessageID(), func(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
		recv := iRecv.(*C2S_Ping)
		send := iSend.(*S2C_Pong)
		defer func() {
			if sender != nil {
				ctx.Respond(send)
			}
		}()

		send.I = recv.I
		send.S = recv.S
	}); err != nil {
		log.Panic(err)
	}

	if err := r.Start(nil, "receiver"); err != nil {
		log.Panic(err)
	}

	r.WaitForStarted()

	return r
}

func stopPromiseReceiver(r *promiseReceiver) {
	_ = r.StopAndWait()
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func TestPromise_Basic(t *testing.T) {
	r := startPromiseReceiver()
	s := startPromiseSender()

	for i := 0; i < 3; i++ {
		var begin = 1 + int32(i)

		send := Get_C2S_Ping()
		send.I = begin
		s.NewRPCPromise(send, r.PID(), func(ctx *nactor.PromiseContext) {
			log.Printf("[%d] recv: %s\n", ctx.Seq(), ctx.ResponseMessage().String())
			recv := ctx.ResponseMessage().(*S2C_Pong)
			if recv.I != begin {
				ctx.SetError(fmt.Errorf("recv %d not %d", recv.I, begin))
				return
			}
			send.I += 1
			ctx.SetRequestMessage(send)
		}).Then(func(ctx *nactor.PromiseContext) {
			log.Printf("[%d] recv: %s\n", ctx.Seq(), ctx.ResponseMessage().String())
			recv := ctx.ResponseMessage().(*S2C_Pong)
			if recv.I != begin+1 {
				ctx.SetError(fmt.Errorf("recv %d not %d", recv.I, begin))
				return
			}
		}).Then(func(ctx *nactor.PromiseContext) {
			log.Printf("[%d] do nothing", ctx.Seq())
		}).Then(func(ctx *nactor.PromiseContext) {
			send.I += 1
			ctx.SetRequestMessage(send)
		}).Then(func(ctx *nactor.PromiseContext) {
			log.Printf("[%d] recv: %s\n", ctx.Seq(), ctx.ResponseMessage().String())
			recv := ctx.ResponseMessage().(*S2C_Pong)
			if recv.I != begin+2 {
				ctx.SetError(fmt.Errorf("recv %d not %d", recv.I, begin))
				return
			}
		}).OnComplete(func(err error, ctx *nactor.PromiseContext) {
			if err != nil {
				log.Printf("[%d] fail: %s", ctx.Seq(), err)
			} else {
				log.Printf("[%d] success", ctx.Seq())
			}
		}).Execute()
	}

	time.Sleep(1 * time.Second)

	if s.PromiseNum() > 0 {
		t.Errorf("Promise not release")
	}

	stopPromiseSender(s)
	stopPromiseReceiver(r)
}

func TestPromise_Timeout(t *testing.T) {
	r := startPromiseReceiver()
	s := startPromiseSender()

	s.NewRPCPromise(Get_C2S_NoResponse(), r.PID(), func(context *nactor.PromiseContext) {
		log.Printf("recv: %s\n", context.ResponseMessage().String())
	}).OnComplete(func(err error, ctx *nactor.PromiseContext) {
		if err != nil {
			log.Printf("fail: %s", err)
		} else {
			log.Printf("success\n")
		}
	}).Execute()

	time.Sleep(10 * time.Second)

	if s.PromiseNum() > 0 {
		t.Errorf("promise not release")
	}

	stopPromiseSender(s)
	stopPromiseReceiver(r)
}

func TestPromise_Add(t *testing.T) {
	r := startPromiseReceiver()
	s := startPromiseSender()

	var begin int32 = 1
	parent := s.NewPromise().OnComplete(func(err error, ctx *nactor.PromiseContext) {
		if err != nil {
			log.Printf("fail: %s", err)
		} else {
			log.Printf("success\n")
		}
	})
	for i := 0; i < 10; i++ {
		send := Get_C2S_Ping()
		send.I = begin + int32(i)
		subPromise := s.NewRPCPromise(send, r.PID(), func(context *nactor.PromiseContext) {
			log.Printf("recv: %s\n", context.ResponseMessage().String())
			send.I += 100
			context.SetRequestMessage(send)
		}).Then(func(context *nactor.PromiseContext) {
			log.Printf("recv: %s\n", context.ResponseMessage().String())
		})
		parent.Add(subPromise)
	}
	parent.Execute()

	time.Sleep(2 * time.Second)

	if s.PromiseNum() > 0 {
		t.Errorf("promise not release")
	}

	stopPromiseSender(s)
	stopPromiseReceiver(r)
}
