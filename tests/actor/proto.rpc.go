// Code generated by protoc-gen-rpc-go. DO NOT EDIT IT!!!
// source: proto.proto

package tests

import (
	fmt "fmt"
	math "math"
	sync "sync"

	rpc "nggs/rpc"

	proto "github.com/gogo/protobuf/proto"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// message [C2S_Ping] begin
func New_C2S_Ping() *C2S_Ping {
	m := &C2S_Ping{}
	return m
}

func (m *C2S_Ping) ResetEx() {

	m.I = 0

	m.S = ""

}

func (m *C2S_Ping) Clone() *C2S_Ping {
	if m == nil {
		return nil
	}
	n, ok := g_C2S_Ping_Pool.Get().(*C2S_Ping)
	if !ok || n == nil {
		n = &C2S_Ping{}
	}
	deriveDeepCopyC2S_Ping(n, m)
	return n
}

func (C2S_Ping) RPC() {
}

func (C2S_Ping) MessageID() rpc.MessageID {
	return 1
}

func C2S_Ping_MessageID() rpc.MessageID {
	return 1
}

func (C2S_Ping) MessageName() string {
	return "C2S_Ping"
}

func C2S_Ping_MessageName() string {
	return "C2S_Ping"
}

func (C2S_Ping) ResponseMessageID() rpc.MessageID {

	return S2C_Pong_MessageID()

}

func C2S_Ping_ResponseMessageID() rpc.MessageID {

	return S2C_Pong_MessageID()

}

/*
func Request_S2C_Pong(pid *PID, send *C2S_Ping) (*S2C_Pong, error) {
	return Request_S2C_Pong_T(pid, send, 10000 * time.Millisecond)
}

func Request_S2C_Pong_T(pid *PID, send *C2S_Ping, timeout time.Duration) (*S2C_Pong, error) {
	if pid == nil {
		return nil, fmt.Errorf("pid is nil")
	}
	f := actor.RootContext().RequestFuture(pid.ToReal(), send, timeout)
	iRecv, err := f.Result()
	if err != nil {
		return nil, err
	}
	recv, ok := iRecv.(*S2C_Pong)
	if !ok {
		return nil, fmt.Errorf("recv %#v is not S2C_Pong", recv)
	}
	return recv, nil
}
*/

var g_C2S_Ping_Pool = sync.Pool{}

func Get_C2S_Ping() *C2S_Ping {
	m, ok := g_C2S_Ping_Pool.Get().(*C2S_Ping)
	if !ok {
		m = New_C2S_Ping()
	} else {
		if m == nil {
			m = New_C2S_Ping()
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_C2S_Ping(i interface{}) {
	if m, ok := i.(*C2S_Ping); ok && m != nil {
		g_C2S_Ping_Pool.Put(i)
	}
}

func init() {
	Protocol.Register(
		&C2S_Ping{},
		func() rpc.IMessage { return Get_C2S_Ping() },
		func(msg rpc.IMessage) { Put_C2S_Ping(msg) },
	)
}

// message [C2S_Ping] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// message [S2C_Pong] begin
func New_S2C_Pong() *S2C_Pong {
	m := &S2C_Pong{}
	return m
}

func (m *S2C_Pong) ResetEx() {

	m.I = 0

	m.S = ""

}

func (m *S2C_Pong) Clone() *S2C_Pong {
	if m == nil {
		return nil
	}
	n, ok := g_S2C_Pong_Pool.Get().(*S2C_Pong)
	if !ok || n == nil {
		n = &S2C_Pong{}
	}
	deriveDeepCopyS2C_Pong(n, m)
	return n
}

func (S2C_Pong) RPC() {
}

func (S2C_Pong) MessageID() rpc.MessageID {
	return 2
}

func S2C_Pong_MessageID() rpc.MessageID {
	return 2
}

func (S2C_Pong) MessageName() string {
	return "S2C_Pong"
}

func S2C_Pong_MessageName() string {
	return "S2C_Pong"
}

func (S2C_Pong) ResponseMessageID() rpc.MessageID {

	return 0

}

func S2C_Pong_ResponseMessageID() rpc.MessageID {

	return 0

}

var g_S2C_Pong_Pool = sync.Pool{}

func Get_S2C_Pong() *S2C_Pong {
	m, ok := g_S2C_Pong_Pool.Get().(*S2C_Pong)
	if !ok {
		m = New_S2C_Pong()
	} else {
		if m == nil {
			m = New_S2C_Pong()
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_S2C_Pong(i interface{}) {
	if m, ok := i.(*S2C_Pong); ok && m != nil {
		g_S2C_Pong_Pool.Put(i)
	}
}

func init() {
	Protocol.Register(
		&S2C_Pong{},
		func() rpc.IMessage { return Get_S2C_Pong() },
		func(msg rpc.IMessage) { Put_S2C_Pong(msg) },
	)
}

// message [S2C_Pong] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// message [C2S_NoResponse] begin
func New_C2S_NoResponse() *C2S_NoResponse {
	m := &C2S_NoResponse{}
	return m
}

func (m *C2S_NoResponse) ResetEx() {

}

func (m *C2S_NoResponse) Clone() *C2S_NoResponse {
	if m == nil {
		return nil
	}
	n, ok := g_C2S_NoResponse_Pool.Get().(*C2S_NoResponse)
	if !ok || n == nil {
		n = &C2S_NoResponse{}
	}
	deriveDeepCopyC2S_NoResponse(n, m)
	return n
}

func (C2S_NoResponse) RPC() {
}

func (C2S_NoResponse) MessageID() rpc.MessageID {
	return 5
}

func C2S_NoResponse_MessageID() rpc.MessageID {
	return 5
}

func (C2S_NoResponse) MessageName() string {
	return "C2S_NoResponse"
}

func C2S_NoResponse_MessageName() string {
	return "C2S_NoResponse"
}

func (C2S_NoResponse) ResponseMessageID() rpc.MessageID {

	return 0

}

func C2S_NoResponse_ResponseMessageID() rpc.MessageID {

	return 0

}

var g_C2S_NoResponse_Pool = sync.Pool{}

func Get_C2S_NoResponse() *C2S_NoResponse {
	m, ok := g_C2S_NoResponse_Pool.Get().(*C2S_NoResponse)
	if !ok {
		m = New_C2S_NoResponse()
	} else {
		if m == nil {
			m = New_C2S_NoResponse()
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_C2S_NoResponse(i interface{}) {
	if m, ok := i.(*C2S_NoResponse); ok && m != nil {
		g_C2S_NoResponse_Pool.Put(i)
	}
}

func init() {
	Protocol.Register(
		&C2S_NoResponse{},
		func() rpc.IMessage { return Get_C2S_NoResponse() },
		func(msg rpc.IMessage) { Put_C2S_NoResponse(msg) },
	)
}

// message [C2S_NoResponse] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
