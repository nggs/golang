package event

import "errors"

var ErrNoHandler = errors.New("no handler")
