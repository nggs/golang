package etcd

import (
	"encoding/json"
	"io/ioutil"
)

type Config struct {
	Endpoint string
	Root     string
}

func (cfg *Config) LoadFromFile(path string) error {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, &cfg)
	if err != nil {
		return err
	}
	return nil
}

func (cfg Config) TidyAndCheck() error {
	if cfg.Endpoint == "" {
		return ErrEndpointMustNotEmpty
	}
	if cfg.Root == "" {
		return ErrRootMustNotEmpty
	}
	return nil
}
