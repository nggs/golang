package etcdv3

import (
	"context"
	"fmt"
	"os"
	"testing"
	"time"

	"go.etcd.io/etcd/client/v3"

	"nggs/random"
)

func getEndpoint() string {
	endpoint := os.Getenv("ETCD_ENDPOINT")
	if endpoint == "" {
		endpoint = "http://mydev.com:2379"
	}
	return endpoint
}

func Test_Base(t *testing.T) {
	endpoint := getEndpoint()

	t.Logf("endpoint=[%s]\n", endpoint)

	err := C.Init(endpoint)
	if err != nil {
		t.Error(err)
		return
	}
	defer C.Close()

	key := random.String(16)
	value := random.String(16)

	_, err = C.put(key, value)
	if err != nil {
		t.Error(err)
		return
	}

	t.Logf("put key=[%s], value=[%s]\n", key, value)

	resp, err := C.get(key)
	if err != nil {
		t.Error(err)
		return
	}
	for _, kv := range resp.Kvs {
		t.Logf("get key=[%s], value=[%s]\n", kv.Key, kv.Value)
	}

	if _, err := C.delete_(key); err != nil {
		t.Error(err)
	} else {
		t.Logf("delete key=[%s]\n", key)
	}

}

func Test_GetWithRev(t *testing.T) {
	endpoint := getEndpoint()

	t.Logf("endpoint=[%s]\n", endpoint)

	err := C.Init(endpoint)
	if err != nil {
		t.Error(err)
		return
	}
	defer C.Close()

	presp, err := C.Client.Put(context.TODO(), "foo", "bar1")
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(presp.Header.Revision)
	_, err = C.Client.Put(context.TODO(), "foo", "bar2")
	if err != nil {
		t.Error(err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	resp, err := C.Client.Get(ctx, "foo", clientv3.WithRev(presp.Header.Revision))
	cancel()
	if err != nil {
		t.Error(err)
		return
	}
	for _, ev := range resp.Kvs {
		t.Logf("%s : %s\n", ev.Key, ev.Value)
	}
}

func Test_PutGetDelete(t *testing.T) {
	endpoint := getEndpoint()

	t.Logf("endpoint=[%s]\n", endpoint)

	err := C.Init(endpoint)
	if err != nil {
		t.Error(err)
		return
	}
	defer C.Close()

	begin := time.Now()

	k := "test:key:1:2:3"

	for i := 0; i < 1000; i++ {
		v := random.String(8)

		err := C.Put(k, v)

		v, err = C.GetString(k)
		if err != nil {
			t.Error(err)
			return
		}

		//fmt.Println(v)

		_, err = C.Delete(k)
		if err != nil {
			t.Error(err)
			return
		}

		//t.Log(n)
	}

	t.Logf("consume=%v\n", time.Now().Sub(begin))
}

func Test_PutGetDeleteWithPrefix(t *testing.T) {
	endpoint := getEndpoint()

	t.Logf("endpoint=[%s]\n", endpoint)

	err := C.Init(endpoint)
	if err != nil {
		t.Error(err)
		return
	}
	defer C.Close()

	for i := range make([]int, 3) {
		err := C.Put(fmt.Sprintf("key_%d", i), fmt.Sprintf("value_%d", i))
		if err != nil {
			t.Error(err)
			return
		}
	}

	kvs, err := C.GetStrings("key_")
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Println(kvs)

	n, err := C.DeleteWithPrefix("key_")
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(n)
}

func Test_Watch(t *testing.T) {
	endpoint := getEndpoint()

	t.Logf("endpoint=[%s]\n", endpoint)

	err := C.Init(endpoint)
	if err != nil {
		t.Error(err)
		return
	}
	defer C.Close()

	k := "key"
	//v := "value"

	rch := C.WatchWithPrefix(k)
	go WatchLoop(rch, func(ev *clientv3.Event) {
		t.Logf("%v\n", ev)
	}, nil)

	time.Sleep(1 * time.Second)

	err = C.Put(k, random.String(8))
	if err != nil {
		t.Error(err)
		return
	}

	_, err = C.Delete(k)
	if err != nil {
		t.Error(err)
		return
	}

	time.Sleep(5 * time.Second)
}

func Test_PutWithLease(t *testing.T) {
	endpoint := getEndpoint()

	t.Logf("endpoint=[%s]\n", endpoint)

	err := C.Init(endpoint)
	if err != nil {
		t.Error(err)
		return
	}
	defer C.Close()

	leaseID, err := C.GrantLease(5)
	if err != nil {
		t.Error(err)
		return
	}

	k := "key"
	v := "value"

	err = C.PutWithLease(k, v, leaseID)
	if err != nil {
		t.Error(err)
		return
	}
}

func Test_KeepAlive(t *testing.T) {
	endpoint := getEndpoint()

	t.Logf("endpoint=[%s]\n", endpoint)

	err := C.Init(endpoint)
	if err != nil {
		t.Error(err)
		return
	}
	defer C.Close()

	k := "foo"
	v := "bar"

	leaseID, err := C.GrantLease(5)

	err = C.PutWithLease(k, v, leaseID)
	if err != nil {
		t.Error(err)
		return
	}

	// the key 'foo' will be kept forever
	ch, err := C.KeepAliveLease(leaseID)
	if err != nil {
		t.Error(err)
		return
	}

	ka := <-ch
	t.Log("ttl:", ka.TTL)

	time.Sleep(5 * time.Second)
}

//func Test_KeepAliveOnce(t *testing.T) {
//	endpoint := os.Getenv("ETCD_ENDPOINT")
//	if endpoint == "" {
//		t.Error("not found env ETCD_ENDPOINT or ETCD_ENDPOINT is empty string")
//		return
//	}
//
//	fmt.Printf("endpoint=[%s]\n", endpoint)
//
//	Init(endpoint)
//	defer Close()
//
//	k := "foo"
//	v := "bar"
//
//	leaseID, err := GrantLease(5)
//
//	err = PutWithLease(k, v, leaseID)
//	if err != nil {
//		t.Error(err)
//		return
//	}
//
//	time.Sleep(4 * time.Second)
//
//	// to renew the lease only once
//	resp, err := KeepAliveLeaseOnce(leaseID)
//	if err != nil {
//		t.Error(err)
//		return
//	}
//
//	fmt.Println("ttl:", resp.TTL)
//}

func Test_Txn(t *testing.T) {
	endpoint := getEndpoint()

	t.Logf("endpoint=[%s]\n", endpoint)

	err := C.Init(endpoint)
	if err != nil {
		t.Error(err)
		return
	}
	defer C.Close()

	kvc := clientv3.NewKV(C.Client)

	_, err = kvc.Put(context.TODO(), "key", "xyz")
	if err != nil {
		t.Error(err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	_, err = kvc.Txn(ctx).
		If(clientv3.Compare(clientv3.Value("key"), ">", "abc")). // txn value comparisons are lexical
		Then(clientv3.OpPut("key", "XYZ")).                      // this runs, since 'xyz' > 'abc'
		Else(clientv3.OpPut("key", "ABC")).
		Commit()
	cancel()
	if err != nil {
		t.Error(err)
		return
	}

	resp, err := kvc.Get(context.TODO(), "key")
	cancel()
	if err != nil {
		t.Error(err)
		return
	}
	for _, ev := range resp.Kvs {
		t.Logf("%s : %s\n", ev.Key, ev.Value)
	}
	// Output: key : XYZ
}
