package etcd

import (
	"errors"
)

var ErrKeyNotFound = errors.New("key not found ")

var ErrEndpointMustNotEmpty = errors.New("endpoint must not empty")

var ErrRootMustNotEmpty = errors.New("root must not empty")
