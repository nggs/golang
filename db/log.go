package db

import (
	nlog "nggs/log"
)

var gLogger = nlog.NewConsoleLogger()

func Logger() nlog.ILogger {
	return gLogger
}

func SetLogger(l nlog.ILogger) {
	gLogger = l
}
