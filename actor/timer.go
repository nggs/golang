package actor

import (
	"fmt"
	nexport "nggs/export"
	"sync/atomic"
	"time"

	"github.com/asynkron/protoactor-go/actor"
)

type TimerID = nexport.ActorTimerID
type TimerTag = nexport.ActorTimerTag

type TimerCallback func(id TimerID, tag TimerTag)

type timeout struct {
	id  TimerID
	tag TimerTag
}

var gTagGenerator uint64

func NextTimerTag() TimerTag {
	return atomic.AddUint64(&gTagGenerator, 1)
}

type timer struct {
	id     TimerID
	tag    TimerID
	cb     TimerCallback
	looped bool
	stopCh chan struct{}
}

func (t *timer) stop() {
	close(t.stopCh)
}

func (t *timer) trigger() {
	t.cb(t.id, t.tag)
}

type TimerManager struct {
	idGenerator uint64
	timers      map[TimerID]*timer
	pid         *actor.PID
}

func NewTimerManager(pid *actor.PID) *TimerManager {
	return &TimerManager{
		idGenerator: 0,
		timers:      make(map[TimerID]*timer),
		pid:         pid,
	}
}

func (m *TimerManager) nextID() TimerID {
	return atomic.AddUint64(&m.idGenerator, 1)
}

func (m *TimerManager) NewTimer(dur time.Duration, callback TimerCallback) TimerID {
	newTimer := &timer{
		id:     m.nextID(),
		tag:    0,
		cb:     callback,
		looped: false,
		stopCh: make(chan struct{}),
	}

	m.timers[newTimer.id] = newTimer

	go func() {
		select {
		case <-time.After(dur):
			RootContext().Send(m.pid, &timeout{id: newTimer.id, tag: newTimer.tag})

		case <-newTimer.stopCh:
			return
		}
	}()

	return newTimer.id
}

func (m *TimerManager) NewTimerWithTag(dur time.Duration, tag TimerTag, callback TimerCallback) TimerID {
	newTimer := &timer{
		id:     m.nextID(),
		tag:    tag,
		cb:     callback,
		looped: false,
		stopCh: make(chan struct{}),
	}

	m.timers[newTimer.id] = newTimer

	go func() {
		select {
		case <-time.After(dur):
			RootContext().Send(m.pid, &timeout{id: newTimer.id, tag: newTimer.tag})

		case <-newTimer.stopCh:
			return
		}
	}()

	return newTimer.id
}

func (m *TimerManager) NewLoopTimer(dur time.Duration, callback TimerCallback) TimerID {
	newTimer := &timer{
		id:     m.nextID(),
		tag:    0,
		cb:     callback,
		looped: true,
		stopCh: make(chan struct{}),
	}

	m.timers[newTimer.id] = newTimer

	go func() {
		ticker := time.NewTicker(dur)
		chTicker := ticker.C
		msg := &timeout{id: newTimer.id, tag: newTimer.tag}

		for {
			select {
			case <-chTicker:
				RootContext().Send(m.pid, msg)

			case <-newTimer.stopCh:
				ticker.Stop()
				return
			}
		}
	}()

	return newTimer.id
}

func (m *TimerManager) NewLoopTimerWithTag(dur time.Duration, tag TimerTag, callback TimerCallback) TimerID {
	newTimer := &timer{
		id:     m.nextID(),
		tag:    tag,
		cb:     callback,
		looped: true,
		stopCh: make(chan struct{}),
	}

	m.timers[newTimer.id] = newTimer

	go func() {
		ticker := time.NewTicker(dur)
		chTicker := ticker.C
		msg := &timeout{id: newTimer.id, tag: newTimer.tag}

		for {
			select {
			case <-chTicker:
				RootContext().Send(m.pid, msg)

			case <-newTimer.stopCh:
				ticker.Stop()
				return
			}
		}
	}()

	return newTimer.id
}

func (m *TimerManager) Trigger(id TimerID) error {
	t, ok := m.timers[id]
	if !ok {
		return fmt.Errorf("timer not found")
	}
	t.trigger()
	if !t.looped {
		delete(m.timers, id)
	}
	return nil
}

func (m *TimerManager) Stop(id TimerID) error {
	if t, ok := m.timers[id]; ok {
		t.stop()
		delete(m.timers, id)
		return nil
	}
	return fmt.Errorf("timer[%d] not found", id)
}

func (m *TimerManager) StopAll() {
	for _, t := range m.timers {
		t.stop()
	}
	// 清空map
	m.timers = make(map[TimerID]*timer)
}
