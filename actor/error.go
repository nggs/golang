package actor

import (
	"errors"
	"fmt"

	nrpc "nggs/rpc"
)

var ErrAlreadyStop = errors.New("already stop")

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var ErrHttpTaskTimeoutMustGreaterThanZero = errors.New("http task timeout must > 0")
var ErrHttpTaskRequestMustNotNil = errors.New("http task request must not nil")
var ErrHttpTaskOnCompletedMustNotNil = errors.New("http task on completed must not nil")
var ErrHttpTaskNotFound = errors.New("http task not found")

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type ErrPromiseNotExist struct {
	Seq PromiseSeq
}

func (e ErrPromiseNotExist) Error() string {
	return fmt.Sprintf("not exist, seq=%d", e.Seq)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type ErrPromiseTimeout struct {
	Seq              PromiseSeq
	RequestMessageID nrpc.MessageID
}

func (e ErrPromiseTimeout) Error() string {
	return fmt.Sprintf("timeout, seq=%d, request message id=%d", e.Seq, e.RequestMessageID)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type ErrPromiseProduceRequestMessageFail struct {
	Seq              PromiseSeq
	RequestMessageID nrpc.MessageID
	Err              error
}

func (e ErrPromiseProduceRequestMessageFail) Error() string {
	return fmt.Sprintf("produce request message fail, seq=%d, request message id=%d,  %s",
		e.Seq, e.RequestMessageID, e.Err)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type ErrPromiseUnmarshalRequestMessageFail struct {
	Seq              PromiseSeq
	RequestMessageID nrpc.MessageID
	Err              error
}

func (e ErrPromiseUnmarshalRequestMessageFail) Error() string {
	return fmt.Sprintf("unmarshal request message fail, seq=%d, request message id=%d, %s",
		e.Seq, e.RequestMessageID, e.Err)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type ErrPromiseProduceResponseMessageFail struct {
	Seq               PromiseSeq
	RequestMessageID  nrpc.MessageID
	ResponseMessageID nrpc.MessageID
	Err               error
}

func (e ErrPromiseProduceResponseMessageFail) Error() string {
	return fmt.Sprintf("produce resposne message fail, seq=%d, request message id=%d, response message id=%d, %s",
		e.Seq, e.RequestMessageID, e.ResponseMessageID, e.Err)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type ErrPromiseUnmarshalResponseMessageFail struct {
	Seq               PromiseSeq
	RequestMessageID  nrpc.MessageID
	ResponseMessageID nrpc.MessageID
	Err               error
}

func (e ErrPromiseUnmarshalResponseMessageFail) Error() string {
	return fmt.Sprintf("unmarshal resposne message fail, seq=%d, request message id=%d, response message id=%d, %s",
		e.Seq, e.RequestMessageID, e.ResponseMessageID, e.Err)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type ErrPromiseMarshalResponseMessageFail struct {
	Seq               PromiseSeq
	RequestMessageID  nrpc.MessageID
	ResponseMessageID nrpc.MessageID
	Err               error
}

func (e ErrPromiseMarshalResponseMessageFail) Error() string {
	return fmt.Sprintf("marshal resposne message fail, seq=%d, request message id=%d, response message id=%d, %s",
		e.Seq, e.RequestMessageID, e.ResponseMessageID, e.Err)
}
