package actor

import "github.com/asynkron/protoactor-go/actor"

func IsPIDValueEmpty(pid actor.PID) bool {
	if pid.Address == "" || pid.Id == "" {
		return true
	}
	return false
}

func IsPIDPtrEmpty(pid *actor.PID) bool {
	if pid == nil {
		return true
	}
	return IsPIDValueEmpty(*pid)
}
