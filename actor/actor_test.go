package actor

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
	"testing"
	"time"

	"github.com/gin-gonic/gin"

	"nggs/random"

	"github.com/asynkron/protoactor-go/actor"
	alog "github.com/asynkron/protoactor-go/log"
)

type example struct {
	*Actor
	Implement

	startedWg sync.WaitGroup
	stoppedWg sync.WaitGroup
}

func newExample() *example {
	e := &example{}

	e.Actor = New(
		WithImplement(e),
		WithStartedWaitGroup(&e.startedWg),
		WithStoppedWaitGroup(&e.stoppedWg),
		WithMailBoxSize(10),
		WithPulse(1*time.Second),
		WithHttpServer("localhost:8080", "", "", func(engine *gin.Engine) {
			log.Printf("before run http server")
		}, func(err error, engine *gin.Engine) {
			log.Printf("after run http server")
		}),
	)

	return e
}

func (e *example) OnReceiveMessage(ctx actor.Context) {
	iMsg := ctx.Message()
	switch msg := iMsg.(type) {
	case string:
		log.Printf("recv string: %s", msg)
		switch msg {
		case "restart test":
			log.Panic(msg)
		}
	default:
		log.Printf("recv %#v", msg)
	}
}

func (e *example) OnStarted(ctx actor.Context) {
	log.Println("OnStarted")
}

func (e *example) OnStopping(ctx actor.Context) {
	log.Println("OnStopping")
}

func (e *example) OnStopped(ctx actor.Context) {
	log.Println("OnStopped")
}

func (e *example) OnRestarting(ctx actor.Context) {
	log.Println("OnRestarting")
}

func (e *example) OnRestarted(ctx actor.Context) {
	log.Println("OnRestarted")
}

func (e *example) BeforeTriggerTimer(id TimerID, tag TimerTag, ctx actor.Context) (ok bool, err error) {
	log.Printf("BeforeTriggerTimer, id=%d, tag=%d", id, tag)
	return true, nil
}

func (e *example) AfterTriggerTimer(err error, id TimerID, tag TimerTag, ctx actor.Context) {
	log.Printf("AfterTriggerTimer, id=%d, tag=%d", id, tag)
}

func (e *example) BeforeDispatchHttpRequest(ctx *gin.Context) (ok bool, err error) {
	log.Printf("BeforeDispatchHttpRequest, ctx=%v", ctx)
	return true, nil
}

func (e *example) AfterDispatchHttpRequest(err error, ctx *gin.Context) {
	log.Printf("AfterDispatchHttpRequest, ctx=%v", ctx)
}

func (e *example) OnPulse(ctx actor.Context) {
	log.Println("OnPulse")
}

func TestSuper_StartStop(t *testing.T) {
	Init(alog.InfoLevel, nil)
	defer Close()

	e := newExample()

	err := e.Start(nil, "example")
	if err != nil {
		t.Errorf("e.Start fail, %s", err)
		return
	}

	e.WaitForStarted()

	e.NewLoopTimer(1*time.Second, 0, func(TimerID, TimerID) {
		RootContext().Send(e.PID(), random.String(16))
	})

	e.NewTimer(random.TimeDuration(1*time.Second, 5*time.Second), 0, func(TimerID, TimerID) {
		RootContext().Send(e.PID(), "restart test")
	})

	time.Sleep(5 * time.Second)

	err = e.PoisonAndWait()
	if err != nil {
		t.Errorf("e.StopAndWait fail, %s", err)
		return
	}

	RootContext().Poison(e.PID())

	time.Sleep(1 * time.Second)
}

func TestSuper_Http(t *testing.T) {
	Init(alog.InfoLevel, nil)
	defer Close()

	e := newExample()

	err := e.Start(nil, "example")
	if err != nil {
		t.Errorf("e.Start fail, %s", err)
		return
	}

	e.WaitForStarted()

	err = e.RegisterHttpRequestHandler("GET", "/echo", nil, func(ctx *gin.Context) {
		if recvData, e := ioutil.ReadAll(ctx.Request.Body); e != nil {
			t.Errorf("get request body data fail, %s", e)
		} else {
			if len(recvData) > 0 {
				_, e = ctx.Writer.Write(recvData)
				if e != nil {
					t.Errorf("write data fail, %s", e)
				}
			}
		}
	})
	if err != nil {
		t.Error(err)
		return
	}

	taskID, err := e.NewHttpTask(3*time.Second, 0, func() (request *http.Request, err error) {
		request, err = http.NewRequest("GET", "http://localhost:8080/echo", bytes.NewReader([]byte("echo")))
		if err != nil {
			return
		}
		return
	}, func(err error, task *HttpTask) {
		if err != nil {
			t.Errorf("NewHttpTask fail, %s", err)
			return
		}

		body, err := task.GetResponseBodyData()
		if err != nil {
			t.Errorf("GetResponseBodyData fail, %s", err)
			return
		}

		log.Printf("recv %s", body)

		if string(body) != "echo" {
			t.Errorf("recv data not match")
		}
	})
	if err != nil {
		t.Errorf("NewHttpTask fail, %s", err)
		return
	}

	log.Printf("NewHttpTask success, id=%d", taskID)

	time.Sleep(1 * time.Second)

	err = e.PoisonAndWait()
	if err != nil {
		t.Errorf("e.StopAndWait fail, %s", err)
		return
	}

	time.Sleep(1 * time.Second)
}
