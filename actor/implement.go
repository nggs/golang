package actor

import (
	"github.com/asynkron/protoactor-go/actor"
	"github.com/gin-gonic/gin"

	nevent "nggs/event"
	npb "nggs/network/protocol/protobuf/v3"
	nrpc "nggs/rpc"
)

type Implement struct {
}

func NewImplement() *Implement {
	return &Implement{}
}

func (i Implement) OnStarted(ctx actor.Context) {

}

func (i Implement) OnStopping(ctx actor.Context) {

}

func (i Implement) OnStopped(ctx actor.Context) {

}

func (i Implement) OnReceiveMessage(ctx actor.Context) {

}

func (i Implement) OnActorTerminated(who *actor.PID, ctx actor.Context) {

}

func (i Implement) OnRestarting(actor.Context) {

}

func (i Implement) OnRestarted(ctx actor.Context) {

}

func (i Implement) BeforeTriggerTimer(id TimerID, tag TimerTag, ctx actor.Context) (ok bool, err error) {
	ok = true
	return
}

func (i Implement) AfterTriggerTimer(err error, id TimerID, tag TimerTag, ctx actor.Context) {

}

func (i Implement) OnPulse(ctx actor.Context) {

}

func (i Implement) BeforeDispatchEvent(inEvent nevent.IEvent, ctx actor.Context) (args []interface{}, ok bool, err error) {
	ok = true
	return
}

func (i Implement) AfterDispatchEvent(err error, iEvent nevent.IEvent, ctx actor.Context, args ...interface{}) {

}

func (i Implement) BeforeDispatchRPC(sender *actor.PID, iRequestMessage nrpc.IMessage, ctx actor.Context) (args []interface{}, ok bool, err error) {
	ok = true
	return
}

func (i Implement) AfterDispatchRPC(err error, sender *actor.PID, iRequestMessage nrpc.IMessage, iResponseMessage nrpc.IMessage, ctx actor.Context, args ...interface{}) {

}

func (i Implement) BeforeCompleteHttpTask(id HttpTaskID, tag HttpTaskTag, ctx actor.Context) (ok bool, err error) {
	ok = true
	return
}

func (i Implement) AfterCompleteHttpTask(err error, id HttpTaskID, tag HttpTaskTag, ctx actor.Context) {

}

func (i Implement) BeforeDispatchHttpRequest(ctx *gin.Context) (ok bool, err error) {
	ok = true
	return
}

func (i Implement) AfterDispatchHttpRequest(err error, ctx *gin.Context) {

}

func (i Implement) BeforeDispatchMessage(iProtocol npb.IProtocol, iRecv npb.IMessage, ctx actor.Context) (iSend npb.IMessage, args []interface{}, ok bool, err error) {
	ok = true
	return
}

func (i Implement) AfterDispatchMessage(err error, iRecv npb.IMessage, iSend npb.IMessage, ctx actor.Context, args ...interface{}) {

}
