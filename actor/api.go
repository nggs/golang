package actor

import (
	"fmt"
	"time"

	"github.com/asynkron/protoactor-go/actor"
	alog "github.com/asynkron/protoactor-go/log"
	"github.com/asynkron/protoactor-go/remote"

	ndebug "nggs/debug"
	nlog "nggs/log"
	nrpc "nggs/rpc"
)

var gSystem *actor.ActorSystem

func System() *actor.ActorSystem {
	return gSystem
}

func RootContext() *actor.RootContext {
	return gSystem.Root
}

func DefaultDecider(reason interface{}) actor.Directive {
	gLogger.Error("crashed, reason: %#v, stack: %s", reason, ndebug.Stack())
	return actor.StopDirective
}

func Init(logLevel alog.Level, logger nlog.ILogger, options ...actor.ConfigOption) {
	actor.SetLogLevel(logLevel)
	remote.SetLogLevel(logLevel)

	if logger != nil {
		gLogger = logger
	}

	gSystem = actor.NewActorSystem(options...)
}

func Close() {
	gLogger.Close()
}

func RequestFuture(pid *actor.PID, send nrpc.IMessage, timeout time.Duration) (nrpc.IMessage, error) {
	f := gSystem.Root.RequestFuture(pid, send, timeout)
	iRecv, err := f.Result()
	if err != nil {
		return nil, err
	}
	recv, ok := iRecv.(nrpc.IMessage)
	if !ok {
		return nil, fmt.Errorf("recv msg %#v is not a IMessage", recv)
	}
	return recv, nil
}

func ForwardToChildren(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	for _, pid := range ctx.Children() {
		// 转发给所有子actor
		gSystem.Root.Send(pid, iRecv)
	}
}
